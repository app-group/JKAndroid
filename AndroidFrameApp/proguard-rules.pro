#项目
-dontwarn com.cjj.androidframe.**
-keep class com.cjj.androidframe.data.** { *; }
-keep class com.cjj.androidframe.net.** { *; }
-keep class com.cjj.androidframe.mvp.** { *; }
-keep class com.cjj.androidframe.test.AFReflect** { *; }
-keep class com.cjj.androidframe.view.holder** { *; }