package com.cjj.androidframe.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.jkframework.control.JKImageView;

import java.util.ArrayList;
import java.util.List;


public class AFList1Adapter extends BaseQuickAdapter<AFPlayingBean, BaseViewHolder> {


    public AFList1Adapter(List<AFPlayingBean> data) {
        super(R.layout.androidframe_listholder,data);
    }

    /**
     * 更新数据
     * @param a_afpdList 列表数组
     */
    public void Update(ArrayList<AFPlayingBean> a_afpdList)
    {
        setNewInstance(a_afpdList);
    }

    @Override
    protected void convert(BaseViewHolder helper, AFPlayingBean item) {
        helper.setText(R.id.jktvContent,item.getIntro());
        JKImageView jkivImage = helper.getView(R.id.jkivImage);
        jkivImage.SetLoadingImage(R.drawable.androidframe_defaultimageh_all_0);
        jkivImage.SetFailImage(R.drawable.androidframe_defaultimageh_all_0);
        jkivImage.SetImageAsync(item.getImg());
    }
}
