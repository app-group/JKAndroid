package com.cjj.androidframe.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.view.AFListView;

import java.util.ArrayList;


public class AFList2Adapter extends BaseAdapter {

    /**列表数据*/
    private ArrayList<AFPlayingBean> a_afpdList;

    /**
     * 初始化数据
     * @param a_afpdList 列表数组
     */
    public AFList2Adapter(ArrayList<AFPlayingBean> a_afpdList)
    {
        this.a_afpdList = a_afpdList;
    }

    /**
     * 更新数据
     * @param a_afpdList 列表数组
     */
    public void Update(ArrayList<AFPlayingBean> a_afpdList)
    {
        this.a_afpdList = a_afpdList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return a_afpdList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AFListView aflvView;
        if (convertView == null) {
            aflvView = new AFListView(parent.getContext());
        } else {
            aflvView = (AFListView) convertView;
        }
        aflvView.Update(a_afpdList.get(position));
        return aflvView;
    }

}
