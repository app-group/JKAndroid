package com.cjj.androidframe.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cjj.androidframe.R;

import java.util.ArrayList;
import java.util.List;

public class AFPage2Adapter extends BaseQuickAdapter<Integer, BaseViewHolder> {


	public AFPage2Adapter(List<Integer> data) {
		super(R.layout.androidframe_pageholder,data);
	}

	/**
	 * 更新数据
	 * @param a_afpdList 列表数组
	 */
	public void Update(ArrayList<Integer> a_afpdList)
	{
		setNewInstance(a_afpdList);
	}

	@Override
	protected void convert(BaseViewHolder helper, Integer item) {
		helper.setImageResource(R.id.jkivImage,item);
	}
}