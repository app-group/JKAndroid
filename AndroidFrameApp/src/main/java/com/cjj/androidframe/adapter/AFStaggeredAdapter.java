package com.cjj.androidframe.adapter;

import android.widget.FrameLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFHairListBean;
import com.jkframework.control.JKImageView;

import java.util.ArrayList;
import java.util.List;


public class AFStaggeredAdapter extends BaseQuickAdapter<AFHairListBean, BaseViewHolder> {


    public AFStaggeredAdapter(List<AFHairListBean> data) {
        super(R.layout.androidframe_staggeredholder,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AFHairListBean item) {
        //rllRoot.setRatio(RatioDatumMode.DATUM_WIDTH,item.getWidth(),item.getHeight());
        FrameLayout vfRoot = helper.getView(R.id.vfRoot);
        JKImageView jkivImage = helper.getView(R.id.jkivImage);
        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) vfRoot.getLayoutParams();
        lp.dimensionRatio = item.getWidth() + ":" + item.getHeight();
        vfRoot.setLayoutParams(lp);
        jkivImage.SetLoadingImage(R.drawable.hairmanager_defaultimageb_all_0);
        jkivImage.SetFailImage(R.drawable.hairmanager_defaultimageb_all_0);
        jkivImage.SetImageHttp(item.getThumb());
    }

    /**
     * 更新数据
     * @param a_afhldList 列表数组
     */
    public void Update(ArrayList<AFHairListBean> a_afhldList)
    {
        setNewInstance(a_afhldList);
    }

//    @Override
//    public int getItemViewType(int position)
//    {
//        return position;
//    }

}
