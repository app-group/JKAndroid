package com.cjj.androidframe.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFCraneBean;

import java.util.ArrayList;
import java.util.List;


public class AFTestAdapter extends BaseQuickAdapter<AFCraneBean, BaseViewHolder> {


    public AFTestAdapter(List<AFCraneBean> data) {
        super(R.layout.androidframe_listholder,data);
    }

    /**
     * 更新数据
     * @param a_afpdList 列表数组
     */
    public void Update(ArrayList<AFCraneBean> a_afpdList)
    {
        setNewInstance(a_afpdList);
    }

    @Override
    protected void convert(BaseViewHolder helper, AFCraneBean item) {
        helper.setText(R.id.jktvContent,item.getCranename());
    }
}