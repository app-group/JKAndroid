/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.databinding.AdapterListItemBindingImpl;
import com.jkframework.adapter.JKBaseDataBindingAdapter;


/**
 * Create by KunMinX at 20/4/19
 */
public class ListAdapter extends JKBaseDataBindingAdapter<AFPlayingBean, AdapterListItemBindingImpl> {

    public ListAdapter(Context context) {
        super(context, R.layout.adapter_list_item, DiffUtils.getInstance().getListCallback());
//        setOnItemClickListener(((item, position) -> {
//            PlayerManager.getInstance().playAudio(position);
//        }));
    }

    @Override
    protected void onBindItem(AdapterListItemBindingImpl binding, AFPlayingBean item, RecyclerView.ViewHolder holder) {
        binding.setItem(item);
        binding.executePendingBindings();
////        int currentIndex = PlayerManager.getInstance().getAlbumIndex();
//        binding.ivPlayStatus.setColor(currentIndex == holder.getAdapterPosition()
//                ? binding.getRoot().getContext().getResources().getColor(R.color.gray) : Color.TRANSPARENT);
    }
}
