/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.adapter;

import androidx.databinding.DataBindingUtil;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.databinding.AdapterListItemBindingImpl;

import org.jetbrains.annotations.NotNull;


/**
 * Create by KunMinX at 20/4/19
 */
public class ListAdapter2 extends BaseQuickAdapter<AFPlayingBean, BaseDataBindingHolder<AdapterListItemBindingImpl>> {


    public ListAdapter2() {
        super(R.layout.adapter_list_item);
        setDiffCallback(DiffUtils.getInstance().getListCallback());
    }

    @Override
    protected void convert(@NotNull BaseDataBindingHolder<AdapterListItemBindingImpl> holder, AFPlayingBean item) {
        AdapterListItemBindingImpl binding = holder.getDataBinding();
        if (binding != null) {
            binding.setItem(item);
//            binding.setPresenter(mPresenter);
            binding.executePendingBindings();
        }
    }
}
