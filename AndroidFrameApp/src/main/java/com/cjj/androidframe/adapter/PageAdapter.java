/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFPageBean;
import com.jkframework.control.JKImageView;
import com.youth.banner.adapter.BannerAdapter;

import java.util.List;


/**
 * Create by KunMinX at 20/4/19
 */
public class PageAdapter extends BannerAdapter<AFPageBean, PageAdapter.ViewHolder> {

//    public PageAdapter(Context context) {
//        super(context, R.layout.adapter_page_item, DiffUtils.getInstance().getPageCallback());
////        setOnItemClickListener(((item, position) -> {
////            PlayerManager.getInstance().playAudio(position);
////        }));
//    }
//
//    @Override
//    protected void onBindItem(AdapterPageItemBindingImpl binding, AFPageBean item, RecyclerView.ViewHolder holder) {
//        binding.setItem(item);
//////        int currentIndex = PlayerManager.getInstance().getAlbumIndex();
////        binding.ivPlayStatus.setColor(currentIndex == holder.getAdapterPosition()
////                ? binding.getRoot().getContext().getResources().getColor(R.color.gray) : Color.TRANSPARENT);
//    }

    public PageAdapter(List<AFPageBean> mDatas) {
        //设置数据，也可以调用banner提供的方法,或者自己在adapter中实现
        super(mDatas);
    }

    //创建ViewHolder，可以用viewType这个字段来区分不同的ViewHolder
    @Override
    public ViewHolder onCreateHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_page_item, parent, false));
    }

    @Override
    public void onBindView(ViewHolder holder, AFPageBean data, int position, int size) {
        holder.imageView.setImageResource(data.getResource());
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        JKImageView imageView;

        public ViewHolder(@NonNull View view) {
            super(view);
            imageView = view.findViewById(R.id.jkivImage);
        }
    }
}
