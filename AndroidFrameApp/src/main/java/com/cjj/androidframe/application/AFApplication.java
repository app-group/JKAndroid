package com.cjj.androidframe.application;

import android.content.Context;

import androidx.multidex.MultiDex;

import com.cjj.androidframe.BuildConfig;
import com.cjj.androidframe.config.AFConfig;
import com.cjj.androidframe.config.AFVersion;
import com.jkframework.algorithm.JKFile;
import com.jkframework.application.JKApplication;
import com.jkframework.config.JKVersion;
import com.jkframework.debug.JKDebug;

public class AFApplication extends JKApplication {

	@Override
	public void onCreate()
	{
		super.onCreate();
        JKDebug.Init(BuildConfig.DEBUG ,AFConfig.MESSAGE_CENTER);

        /*初始化*/
		boolean bInit = JKVersion.CheckVersion();
		if (bInit)
		{
			AFVersion.UpdateOldVersion();
			JKVersion.SaveVersion();
		}

//        /*腾讯bugly*/
//        if (!BuildConfig.DEBUG)
//            QQBugly.GetInstance();
	}

    @Override
    protected void attachBaseContext(final Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
