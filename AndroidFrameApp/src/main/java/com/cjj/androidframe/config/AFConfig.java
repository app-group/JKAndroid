package com.cjj.androidframe.config;


import com.cjj.androidframe.BuildConfig;

public class AFConfig {
	
	/**消息平台反射类*/
	public final static String MESSAGE_CENTER = "com.cjj.androidframe.test.AFReflect";
    /**默认请求每页内容个数*/
    public final static int NORMAL_PAGESIZE = 20;

    /**是否使用测试数据*/
    public static final boolean DEBUG_DATA = BuildConfig.DEBUG ? true : false;
}
