package com.cjj.androidframe.config;

import com.jkframework.config.JKSystem;
import com.jkframework.config.JKVersion;


public class AFVersion {
	
	/**
	 * 更新老版本
	 */
	public static void UpdateOldVersion()
	{
		int nCode = JKVersion.GetLastVersion();
		int nCurrentCode = JKSystem.GetVersionCode();
		if (nCode == 0)
		{
			Update_1_0_0_1();
		}
		else{
			if (nCode < 10000001 && nCurrentCode >= 10000001)
			{
				Update_1_0_0_1();
			}
		}
	}

	/**
	 * 更新1.0.0(1)的内容
	 */
	private static void Update_1_0_0_1()
	{

	}
}
