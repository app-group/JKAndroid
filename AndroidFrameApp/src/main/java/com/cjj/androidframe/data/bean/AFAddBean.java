package com.cjj.androidframe.data.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Objects;

/**
 * Created by 2015/5/25.
 */
public class AFAddBean {

    private int id;
    private String text;
    private int count = 0;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AFAddBean() {
    }

    public AFAddBean(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AFAddBean)) return false;
        AFAddBean afAddBean = (AFAddBean) o;
        return id == afAddBean.id &&
                count == afAddBean.count &&
                Objects.equals(text, afAddBean.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, count);
    }
}

