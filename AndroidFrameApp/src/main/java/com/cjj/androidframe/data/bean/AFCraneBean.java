package com.cjj.androidframe.data.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2015/5/25.
 */
public class AFCraneBean implements Parcelable {

    private int resType;
    private int cranegroup;
    private int craneid;
    private int craneitemid = -1;
    private String cranename;
    private int cranetypeid;
    private int status;

    public int getResType() {
        return resType;
    }

    public void setResType(int resType) {
        this.resType = resType;
    }

    public int getCranegroup() {
        return cranegroup;
    }

    public void setCranegroup(int cranegroup) {
        this.cranegroup = cranegroup;
    }

    public int getCraneid() {
        return craneid;
    }

    public void setCraneid(int craneid) {
        this.craneid = craneid;
    }

    public int getCraneitemid() {
        return craneitemid;
    }

    public void setCraneitemid(int craneitemid) {
        this.craneitemid = craneitemid;
    }

    public String getCranename() {
        return cranename;
    }

    public void setCranename(String cranename) {
        this.cranename = cranename;
    }

    public int getCranetypeid() {
        return cranetypeid;
    }

    public void setCranetypeid(int cranetypeid) {
        this.cranetypeid = cranetypeid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public AFCraneBean() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.resType);
        dest.writeInt(this.cranegroup);
        dest.writeInt(this.craneid);
        dest.writeInt(this.craneitemid);
        dest.writeString(this.cranename);
        dest.writeInt(this.cranetypeid);
        dest.writeInt(this.status);
    }

    protected AFCraneBean(Parcel in) {
        this.resType = in.readInt();
        this.cranegroup = in.readInt();
        this.craneid = in.readInt();
        this.craneitemid = in.readInt();
        this.cranename = in.readString();
        this.cranetypeid = in.readInt();
        this.status = in.readInt();
    }

    public static final Creator<AFCraneBean> CREATOR = new Creator<AFCraneBean>() {
        @Override
        public AFCraneBean createFromParcel(Parcel source) {
            return new AFCraneBean(source);
        }

        @Override
        public AFCraneBean[] newArray(int size) {
            return new AFCraneBean[size];
        }
    };
}
