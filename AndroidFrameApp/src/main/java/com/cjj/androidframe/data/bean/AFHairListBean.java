package com.cjj.androidframe.data.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2015/5/25.
 */
public class AFHairListBean implements Parcelable {

    private String thumb;
    private String avatar;
    private String create_time;
    private String PhotoID;
    private String PhotoIntro = "";
    private String PhotoUrl;
    private int width;
    private int height;

    public AFHairListBean() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumb);
        dest.writeString(this.avatar);
        dest.writeString(this.create_time);
        dest.writeString(this.PhotoID);
        dest.writeString(this.PhotoIntro);
        dest.writeString(this.PhotoUrl);
        dest.writeInt(this.width);
        dest.writeInt(this.height);
    }

    protected AFHairListBean(Parcel in) {
        this.thumb = in.readString();
        this.avatar = in.readString();
        this.create_time = in.readString();
        this.PhotoID = in.readString();
        this.PhotoIntro = in.readString();
        this.PhotoUrl = in.readString();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    public static final Creator<AFHairListBean> CREATOR = new Creator<AFHairListBean>() {
        @Override
        public AFHairListBean createFromParcel(Parcel source) {
            return new AFHairListBean(source);
        }

        @Override
        public AFHairListBean[] newArray(int size) {
            return new AFHairListBean[size];
        }
    };

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getPhotoID() {
        return PhotoID;
    }

    public void setPhotoID(String photoID) {
        PhotoID = photoID;
    }

    public String getPhotoIntro() {
        return PhotoIntro;
    }

    public void setPhotoIntro(String photoIntro) {
        PhotoIntro = photoIntro;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
