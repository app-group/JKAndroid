package com.cjj.androidframe.data.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

/**
 * Created by 2015/5/25.
 */
public class AFPageBean {

    private int id;
    private int resource;

    public AFPageBean(int id, int resource) {
        this.id = id;
        this.resource = resource;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AFPageBean)) return false;
        AFPageBean that = (AFPageBean) o;
        return id == that.id &&
                resource == that.resource;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, resource);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }
}
