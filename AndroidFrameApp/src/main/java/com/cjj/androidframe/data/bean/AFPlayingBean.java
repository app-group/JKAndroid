package com.cjj.androidframe.data.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

/**
 * Created by 2015/5/25.
 */
public class AFPlayingBean implements Parcelable {

    private int id;
    private String title;
    private String img;
    private String intro;
    private String addtime;
    private String url;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.img);
        dest.writeString(this.intro);
        dest.writeString(this.addtime);
        dest.writeString(this.url);
    }

    public AFPlayingBean() {
    }

    public AFPlayingBean(int id, String title, String img, String intro, String addtime, String url) {
        this.id = id;
        this.title = title;
        this.img = img;
        this.intro = intro;
        this.addtime = addtime;
        this.url = url;
    }

    protected AFPlayingBean(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.img = in.readString();
        this.intro = in.readString();
        this.addtime = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<AFPlayingBean> CREATOR = new Parcelable.Creator<AFPlayingBean>() {
        @Override
        public AFPlayingBean createFromParcel(Parcel source) {
            return new AFPlayingBean(source);
        }

        @Override
        public AFPlayingBean[] newArray(int size) {
            return new AFPlayingBean[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AFPlayingBean)) return false;
        AFPlayingBean that = (AFPlayingBean) o;
        return id == that.id &&
                Objects.equals(title, that.title) &&
                Objects.equals(img, that.img) &&
                Objects.equals(intro, that.intro) &&
                Objects.equals(addtime, that.addtime) &&
                Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, img, intro, addtime, url);
    }
}
