package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.cjj.androidframe.data.bean.AFAddBean;

import java.util.ArrayList;

/**
 * Created by 2016/7/5.
 */
public class AFAddModel {

    private ArrayList<AFAddBean> dataList = new ArrayList<>();

    public ArrayList<AFAddBean> getDataList() {
        return dataList;
    }

    public void setDataList(ArrayList<AFAddBean> dataList) {
        this.dataList = dataList;
    }

    public AFAddModel() {
    }
}
