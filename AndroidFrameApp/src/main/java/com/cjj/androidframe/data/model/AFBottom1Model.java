package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2016/7/5.
 */
public class AFBottom1Model implements Parcelable {

    public AFBottom1Model() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected AFBottom1Model(Parcel in) {
    }

    public static final Creator<AFBottom1Model> CREATOR = new Creator<AFBottom1Model>() {
        @Override
        public AFBottom1Model createFromParcel(Parcel source) {
            return new AFBottom1Model(source);
        }

        @Override
        public AFBottom1Model[] newArray(int size) {
            return new AFBottom1Model[size];
        }
    };
}
