package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2016/7/5.
 */
public class AFBottom2Model implements Parcelable {

    public AFBottom2Model() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected AFBottom2Model(Parcel in) {
    }

    public static final Creator<AFBottom2Model> CREATOR = new Creator<AFBottom2Model>() {
        @Override
        public AFBottom2Model createFromParcel(Parcel source) {
            return new AFBottom2Model(source);
        }

        @Override
        public AFBottom2Model[] newArray(int size) {
            return new AFBottom2Model[size];
        }
    };
}
