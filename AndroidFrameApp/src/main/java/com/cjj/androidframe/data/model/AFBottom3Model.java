package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2016/7/5.
 */
public class AFBottom3Model implements Parcelable {

    public AFBottom3Model() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected AFBottom3Model(Parcel in) {
    }

    public static final Creator<AFBottom3Model> CREATOR = new Creator<AFBottom3Model>() {
        @Override
        public AFBottom3Model createFromParcel(Parcel source) {
            return new AFBottom3Model(source);
        }

        @Override
        public AFBottom3Model[] newArray(int size) {
            return new AFBottom3Model[size];
        }
    };
}
