package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2016/7/5.
 */
public class AFBottom4Model implements Parcelable {

    public AFBottom4Model() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected AFBottom4Model(Parcel in) {
    }

    public static final Creator<AFBottom4Model> CREATOR = new Creator<AFBottom4Model>() {
        @Override
        public AFBottom4Model createFromParcel(Parcel source) {
            return new AFBottom4Model(source);
        }

        @Override
        public AFBottom4Model[] newArray(int size) {
            return new AFBottom4Model[size];
        }
    };
}
