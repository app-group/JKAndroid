package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.cjj.androidframe.data.bean.AFPlayingBean;

import java.util.ArrayList;

/**
 * Created by 2016/7/5.
 */
public class AFListModel implements Parcelable {

    private int page1 = 1;
    private int page2 = 1;
    private ArrayList<AFPlayingBean> list1 = new ArrayList<>();
    private ArrayList<AFPlayingBean> list2 = new ArrayList<>();

    public int getPage1() {
        return page1;
    }

    public void setPage1(int page1) {
        this.page1 = page1;
    }

    public int getPage2() {
        return page2;
    }

    public void setPage2(int page2) {
        this.page2 = page2;
    }

    public ArrayList<AFPlayingBean> getList1() {
        return list1;
    }

    public void setList1(ArrayList<AFPlayingBean> list1) {
        this.list1 = list1;
    }

    public ArrayList<AFPlayingBean> getList2() {
        return list2;
    }

    public void setList2(ArrayList<AFPlayingBean> list2) {
        this.list2 = list2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.page1);
        dest.writeInt(this.page2);
        dest.writeTypedList(this.list1);
        dest.writeTypedList(this.list2);
    }

    public AFListModel() {
    }

    protected AFListModel(Parcel in) {
        this.page1 = in.readInt();
        this.page2 = in.readInt();
        this.list1 = in.createTypedArrayList(AFPlayingBean.CREATOR);
        this.list2 = in.createTypedArrayList(AFPlayingBean.CREATOR);
    }

    public static final Creator<AFListModel> CREATOR = new Creator<AFListModel>() {
        @Override
        public AFListModel createFromParcel(Parcel source) {
            return new AFListModel(source);
        }

        @Override
        public AFListModel[] newArray(int size) {
            return new AFListModel[size];
        }
    };
}
