package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2016/7/5.
 */
public class AFLoadingModel implements Parcelable {

    //等待Logo判断
    private boolean Wait = false;
    //加载判断
    private boolean Loading = false;

    public boolean isWait() {
        return Wait;
    }
    public void setWait(boolean wait) {
        Wait = wait;
    }

    public boolean isLoading() {
        return Loading;
    }

    public void setLoading(boolean loading) {
        Loading = loading;
    }

    public AFLoadingModel() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.Wait ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Loading ? (byte) 1 : (byte) 0);
    }

    protected AFLoadingModel(Parcel in) {
        this.Wait = in.readByte() != 0;
        this.Loading = in.readByte() != 0;
    }

    public static final Creator<AFLoadingModel> CREATOR = new Creator<AFLoadingModel>() {
        @Override
        public AFLoadingModel createFromParcel(Parcel source) {
            return new AFLoadingModel(source);
        }

        @Override
        public AFLoadingModel[] newArray(int size) {
            return new AFLoadingModel[size];
        }
    };
}
