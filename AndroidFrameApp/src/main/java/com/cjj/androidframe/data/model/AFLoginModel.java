package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 2016/7/5.
 */
public class AFLoginModel implements Parcelable {

    private String Account;
    private String Password;
    private String Result;

    public AFLoginModel() {
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String account) {
        this.Account = account;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        this.Result = result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Account);
        dest.writeString(this.Password);
        dest.writeString(this.Result);
    }

    protected AFLoginModel(Parcel in) {
        this.Account = in.readString();
        this.Password = in.readString();
        this.Result = in.readString();
    }

    public static final Creator<AFLoginModel> CREATOR = new Creator<AFLoginModel>() {
        @Override
        public AFLoginModel createFromParcel(Parcel source) {
            return new AFLoginModel(source);
        }

        @Override
        public AFLoginModel[] newArray(int size) {
            return new AFLoginModel[size];
        }
    };
}
