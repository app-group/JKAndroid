package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 2016/7/5.
 */
public class AFMainModel implements Parcelable {


    public AFMainModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected AFMainModel(Parcel in) {
    }

    public static final Creator<AFMainModel> CREATOR = new Creator<AFMainModel>() {
        @Override
        public AFMainModel createFromParcel(Parcel source) {
            return new AFMainModel(source);
        }

        @Override
        public AFMainModel[] newArray(int size) {
            return new AFMainModel[size];
        }
    };
}
