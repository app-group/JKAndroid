package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by 2016/7/5.
 */
public class AFPageModel implements Parcelable {

    private ArrayList<String> banner = new ArrayList<>();

    public ArrayList<String> getBanner() {
        return banner;
    }

    public void setBanner(ArrayList<String> banner) {
        this.banner = banner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.banner);
    }

    public AFPageModel() {
    }

    protected AFPageModel(Parcel in) {
        this.banner = in.createStringArrayList();
    }

    public static final Creator<AFPageModel> CREATOR = new Creator<AFPageModel>() {
        @Override
        public AFPageModel createFromParcel(Parcel source) {
            return new AFPageModel(source);
        }

        @Override
        public AFPageModel[] newArray(int size) {
            return new AFPageModel[size];
        }
    };
}
