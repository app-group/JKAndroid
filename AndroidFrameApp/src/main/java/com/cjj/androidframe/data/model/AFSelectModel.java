package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2016/7/5.
 */
public class AFSelectModel implements Parcelable {

    private String ImagePath;
    private String FilePath;
    private ArrayList<LocalMedia> PictureList = new ArrayList<>();

    public ArrayList<LocalMedia> getPictureList() {
        return PictureList;
    }

    public void setPictureList(ArrayList<LocalMedia> pictureList) {
        PictureList = pictureList;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }

    public AFSelectModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ImagePath);
        dest.writeString(this.FilePath);
        dest.writeTypedList(this.PictureList);
    }

    protected AFSelectModel(Parcel in) {
        this.ImagePath = in.readString();
        this.FilePath = in.readString();
        this.PictureList = in.createTypedArrayList(LocalMedia.CREATOR);
    }

    public static final Creator<AFSelectModel> CREATOR = new Creator<AFSelectModel>() {
        @Override
        public AFSelectModel createFromParcel(Parcel source) {
            return new AFSelectModel(source);
        }

        @Override
        public AFSelectModel[] newArray(int size) {
            return new AFSelectModel[size];
        }
    };
}
