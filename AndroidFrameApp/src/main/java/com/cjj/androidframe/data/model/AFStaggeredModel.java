package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.cjj.androidframe.data.bean.AFHairListBean;

import java.util.ArrayList;

/**
 * Created by 2016/7/5.
 */
public class AFStaggeredModel implements Parcelable {

    private int page = 1;
    private ArrayList<AFHairListBean> list = new ArrayList<>();

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public ArrayList<AFHairListBean> getList() {
        return list;
    }

    public void setList(ArrayList<AFHairListBean> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.page);
        dest.writeTypedList(this.list);
    }

    public AFStaggeredModel() {
    }

    protected AFStaggeredModel(Parcel in) {
        this.page = in.readInt();
        this.list = in.createTypedArrayList(AFHairListBean.CREATOR);
    }

    public static final Creator<AFStaggeredModel> CREATOR = new Creator<AFStaggeredModel>() {
        @Override
        public AFStaggeredModel createFromParcel(Parcel source) {
            return new AFStaggeredModel(source);
        }

        @Override
        public AFStaggeredModel[] newArray(int size) {
            return new AFStaggeredModel[size];
        }
    };
}
