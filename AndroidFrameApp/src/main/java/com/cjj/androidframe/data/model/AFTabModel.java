package com.cjj.androidframe.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 2016/7/5.
 */
public class AFTabModel implements Parcelable {

    private int Index;

    public int getIndex() {
        return Index;
    }

    public void setIndex(int index) {
        Index = index;
    }

    public AFTabModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Index);
    }

    protected AFTabModel(Parcel in) {
        this.Index = in.readInt();
    }

    public static final Creator<AFTabModel> CREATOR = new Creator<AFTabModel>() {
        @Override
        public AFTabModel createFromParcel(Parcel source) {
            return new AFTabModel(source);
        }

        @Override
        public AFTabModel[] newArray(int size) {
            return new AFTabModel[size];
        }
    };
}
