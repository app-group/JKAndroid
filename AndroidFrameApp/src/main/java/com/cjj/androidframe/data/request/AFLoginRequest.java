package com.cjj.androidframe.data.request;

import com.cjj.androidframe.data.bean.AFHairListBean;

import java.util.ArrayList;

/**
 * Created by 2015/5/25.
 */
public class AFLoginRequest {

    private String account_name;
    private String account_password;
    private String mac_address;
    private Integer language = 0;
    private Integer mobileType = 0;
    private Integer systemType = 0;

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }

    public Integer getMobileType() {
        return mobileType;
    }

    public void setMobileType(Integer mobileType) {
        this.mobileType = mobileType;
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_password() {
        return account_password;
    }

    public void setAccount_password(String account_password) {
        this.account_password = account_password;
    }

    public String getMac_address() {
        return mac_address;
    }

    public void setMac_address(String mac_address) {
        this.mac_address = mac_address;
    }
}
