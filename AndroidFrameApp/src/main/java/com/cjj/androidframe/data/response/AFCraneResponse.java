package com.cjj.androidframe.data.response;

import com.cjj.androidframe.data.bean.AFCraneBean;

import java.util.ArrayList;

/**
 * Created by 2015/5/25.
 */
public class AFCraneResponse {

    private String rcode = "";
    private String rmsg = "未知错误";
    private ArrayList<AFCraneBean> list = new ArrayList<>();

    public String getRcode() {
        return rcode;
    }

    public void setRcode(String rcode) {
        this.rcode = rcode;
    }

    public String getRmsg() {
        return rmsg;
    }

    public void setRmsg(String rmsg) {
        this.rmsg = rmsg;
    }

    public ArrayList<AFCraneBean> getList() {
        return list;
    }

    public void setList(ArrayList<AFCraneBean> list) {
        this.list = list;
    }
}
