package com.cjj.androidframe.data.response;

import com.cjj.androidframe.data.bean.AFHairListBean;

import java.util.ArrayList;

/**
 * Created by 2015/5/25.
 */
public class AFHairListResponse {

    private String status = "-1";
    private String message = "未知错误";
    private ArrayList<AFHairListBean> result = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AFHairListBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<AFHairListBean> result) {
        this.result = result;
    }
}
