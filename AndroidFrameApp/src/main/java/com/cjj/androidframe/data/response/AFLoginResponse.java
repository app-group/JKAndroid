package com.cjj.androidframe.data.response;

/**
 * Created by 2015/5/25.
 */
public class AFLoginResponse {

    private String rcode;
    private String rmsg = "未知错误";
    private Integer version;

    public String getRcode() {
        return rcode;
    }

    public void setRcode(String rcode) {
        this.rcode = rcode;
    }

    public String getRmsg() { return rmsg;
    }

    public void setRmsg(String rmsg) {
        this.rmsg = rmsg;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
