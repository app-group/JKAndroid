package com.cjj.androidframe.data.response;

import com.cjj.androidframe.data.bean.AFPlayingBean;

import java.util.ArrayList;

/**
 * Created by 2015/5/25.
 */
public class AFPlayingListResponse {

    private String status = "-1";
    private String message = "未知错误";
    private ArrayList<AFPlayingBean> result = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AFPlayingBean> getResult() {
        return result;
    }

    public void setResult(ArrayList<AFPlayingBean> result) {
        this.result = result;
    }
}
