package com.cjj.androidframe.mvp;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.databinding.ViewDataBinding;

import com.cjj.androidframe.BuildConfig;
import com.cjj.androidframe.config.AFConfig;
import com.jkframework.activity.JKBaseActivity;
import com.jkframework.control.JKMessagebox;
import com.jkframework.debug.JKException;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.zackratos.ultimatebarx.ultimatebarx.UltimateBarX;

import java.util.Locale;

public abstract class AFBaseActivity<E extends ViewDataBinding> extends JKBaseActivity<E> {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		UltimateBarX.with(this)
				.transparent()
				.light(true)
				.applyStatusBar();

		UltimateBarX.with(this)
				.transparent()
				.applyNavigationBar();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case Menu.FIRST: {
				JKMessagebox.Inputbox(this, "消息平台调试", "确定",
						new OnInputConfirmListener() {
							@Override
							public void onConfirm(String text) {
								String tFunction = text.toLowerCase(Locale.getDefault());
								JKException.DoingReflect(AFBaseActivity.this, AFConfig.MESSAGE_CENTER, tFunction);
							}
						});
				break;
			}
			case Menu.FIRST + 1: {
				throw  new NullPointerException();
			}
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (BuildConfig.DEBUG) {
			menu.add(Menu.NONE, Menu.FIRST, Menu.FIRST, "消息平台调试");
			menu.add(Menu.NONE, Menu.FIRST + 1, Menu.FIRST + 1, "崩溃测试按钮");
		}
		return true;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}

	
	@Override
	protected void onResume() {
		super.onResume();
	}

}
