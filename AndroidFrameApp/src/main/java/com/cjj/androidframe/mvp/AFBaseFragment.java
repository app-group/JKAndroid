package com.cjj.androidframe.mvp;

import androidx.databinding.ViewDataBinding;

import com.jkframework.fragment.JKBaseFragment;

public abstract class AFBaseFragment<E extends ViewDataBinding> extends JKBaseFragment<E> {

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
