/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvp;


import androidx.lifecycle.Lifecycle;

public interface BaseView {

    /**
     * 锁屏加载
     */
    void LockScreen(String tMsg);

    /**
     * 解除锁屏
     */
    void UnlockScreen();

    /**
     * 界面是否活跃
     */
    boolean IsActive();

    /**
     * 获取生命周期对象
     * @return 生命周期对象
     */
    Lifecycle GetLifecycle();

}
