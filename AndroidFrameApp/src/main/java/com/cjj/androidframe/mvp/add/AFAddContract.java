/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvp.add;


import com.cjj.androidframe.data.model.AFAddModel;
import com.cjj.androidframe.mvp.BasePresenter;
import com.cjj.androidframe.mvp.BaseView;

public interface AFAddContract {

    interface View extends BaseView{
        ///设置界面
        void CleanChildView();
        void AddChildView();    //添加一个空界面
        void SetChildEdit(int nIndex,String tText);
        void SetChildCount(int nIndex,int nCount);
        ///从界面上读取属性
        String GetChildEdit(int nIndex);
        int GetChildCount(int nIndex);
    }

    interface Presenter extends BasePresenter<AFAddModel> {

        /*添加一项*/
        void AddPart();
    }
}
