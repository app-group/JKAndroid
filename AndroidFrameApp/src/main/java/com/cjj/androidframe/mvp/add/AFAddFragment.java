package com.cjj.androidframe.mvp.add;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFAddModel;
import com.cjj.androidframe.databinding.AndroidframeAddfragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.cjj.androidframe.view.AFAddView;
import com.jakewharton.rxbinding4.view.RxView;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.functions.Consumer;

import static autodispose2.AutoDispose.autoDisposable;

public class AFAddFragment extends AFBaseFragment<AndroidframeAddfragmentBindingImpl> implements AFAddContract.View {

    /**
     * Presenter对象
     */
    private AFAddPresenter mPresenter;

    public static AFAddFragment newInstance() {
        return new AFAddFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.androidframe_addfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFAddPresenter(this, new AFAddModel());

        /*设置登录点击事件*/
        RxView.clicks(getBinding().cbAdd)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        mPresenter.AddPart();
                    }
                });

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public void AddChildView() {
        AFAddView vPart = new AFAddView(getContext());
        getBinding().vlParts.addView(vPart);
    }

    @Override
    public void CleanChildView() {
        getBinding().vlParts.removeAllViews();
    }

    @Override
    public void SetChildEdit(int nIndex, String tText) {
        AFAddView vPart = (AFAddView) getBinding().vlParts.getChildAt(nIndex);
        vPart.SetEdit(tText);
    }

    @Override
    public void SetChildCount(int nIndex, int nCount) {
        AFAddView vPart = (AFAddView) getBinding().vlParts.getChildAt(nIndex);
        vPart.SetCount(nCount);
    }

    @Override
    public String GetChildEdit(int nIndex) {
        AFAddView vPart = (AFAddView) getBinding().vlParts.getChildAt(nIndex);
        return vPart.GetEdit();
    }

    @Override
    public int GetChildCount(int nIndex) {
        AFAddView vPart = (AFAddView) getBinding().vlParts.getChildAt(nIndex);
        return vPart.GetCount();
    }
}
