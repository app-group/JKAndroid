package com.cjj.androidframe.mvp.add;

import com.cjj.androidframe.data.bean.AFAddBean;
import com.cjj.androidframe.data.model.AFAddModel;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFAddPresenter implements AFAddContract.Presenter{

    private AFAddContract.View mView;
    private AFAddModel mModel;

    public AFAddPresenter(AFAddContract.View view, AFAddModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFAddModel SaveData() {
        for (int i=0; i<mModel.getDataList().size(); ++i)
        {
            mModel.getDataList().get(i).setText(mView.GetChildEdit(i));
            mModel.getDataList().get(i).setCount(mView.GetChildCount(i));
        }
        return mModel;
    }

    @Override
    public void LoadData(AFAddModel model) {
        mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void AddPart()
    {
        mModel.getDataList().add(new AFAddBean());
        mView.AddChildView();
    }

    @Override
    public void Start() {
        mView.CleanChildView();
        for (int i=0; i<mModel.getDataList().size(); ++i)
        {
            mView.AddChildView();
            mView.SetChildEdit(i,mModel.getDataList().get(i).getText());
            mView.SetChildCount(i,mModel.getDataList().get(i).getCount());
        }
    }

    @Override
    public void Init() {
        mModel.getDataList().add(new AFAddBean());
        Start();
    }
}