/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvp.list;


import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.data.model.AFAddModel;
import com.cjj.androidframe.data.model.AFListModel;
import com.cjj.androidframe.mvp.BasePresenter;
import com.cjj.androidframe.mvp.BaseView;

import java.util.ArrayList;

public interface AFListContract {

    interface View extends BaseView{
        void AutoRefresh1();
        void AutoRefresh2();
        void RefreshComplete1();
        void RefreshComplete2();
        void LoadMoreComplete1(boolean bHasMore);
        void LoadMoreComplete2(boolean bHasMore);
        void LoadMoreError1(String tErrorMsg);
        void LoadMoreError2(String tErrorMsg);

        ///设置界面
        void UpdateList1();
        void UpdateList2();
        void SetList1(ArrayList<AFPlayingBean> a_afpdData);
        void SetList2(ArrayList<AFPlayingBean> a_afpdData);

        ///从界面上读取属性
    }

    interface Presenter extends BasePresenter<AFListModel> {

        /*加载列表1*/
        void LoadList1();
        /*加载列表2*/
        void LoadList2();
        void LoadNextPage1();
        void LoadNextPage2();
        ArrayList<AFPlayingBean> GetList1();
        ArrayList<AFPlayingBean> GetList2();
    }
}
