package com.cjj.androidframe.mvp.list;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cjj.androidframe.R;
import com.cjj.androidframe.adapter.AFList1Adapter;
import com.cjj.androidframe.adapter.AFList2Adapter;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.data.model.AFListModel;
import com.cjj.androidframe.databinding.AndroidframeListfragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.jkframework.control.JKToast;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AFListFragment extends AFBaseFragment<AndroidframeListfragmentBindingImpl> implements AFListContract.View {

    /**
     * Presenter对象
     */
    private AFListPresenter mPresenter;
    /**
     * 列表1适配器
     */
    private AFList1Adapter aflaAdapter1;
    /**
     * 列表2适配器
     */
    private AFList2Adapter aflaAdapter2;


    @Override
    protected int getLayout() {
        return R.layout.androidframe_listfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFListPresenter(this, new AFListModel());

        aflaAdapter1 = new AFList1Adapter(mPresenter.GetList1());
        getBinding().jkrvList.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
//        getBinding().jkrvList.addItemDecoration(
//                new HorizontalDividerItemDecoration.Builder(mActivity)
//                        .color(0xffcccccc)
//                        .size(2)
//                        .build());
        getBinding().jkrvList.setAdapter(aflaAdapter1);


//        AFRefreshHeader2 afrhHeader = AFRefreshHeader2_.build(getContext());
//        jkrRefresh1.SetRefreshListener(new JKRefresh.JKRefreshListener() {
//            @Override
//            public void onRefreshBegin(JKRefresh container) {
//                mPresenter.LoadList1();
//            }
//        });
//        jkrRefresh1.SetView(afrhHeader);
//        jkrRefresh1.AddRefreshUIHandler(afrhHeader);

//        AFLoadMoreFooter aflmfFooter = AFLoadMoreFooter_.build(getContext());
        getBinding().jkrRefresh1.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NotNull RefreshLayout refreshLayout) {
                mPresenter.LoadNextPage1();
            }

            @Override
            public void onRefresh(@NotNull RefreshLayout refreshlayout) {
                mPresenter.LoadList1();
            }
        });
//        jkrvList.SetLoadMoreListener(new JKRecyclerView.JKLoadMoreListener() {
//            @Override
//            public void onLoadMoreBegin(final JKRecyclerView container) {
//                mPresenter.LoadNextPage1();
//            }
//        });
//        jkrvList.SetLoadMoreView(aflmfFooter);
//        jkrvList.AddLoadMoreUIHandler(aflmfFooter);


        /*列表2*/
        aflaAdapter2 = new AFList2Adapter(mPresenter.GetList2());
        getBinding().jkrvList2.AddItemDecoration(0xffcccccc, 2);
        getBinding().jkrvList2.setAdapter(aflaAdapter2);
        getBinding().jkrRefresh2.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NotNull RefreshLayout refreshlayout) {
                mPresenter.LoadNextPage2();
            }

            @Override
            public void onRefresh(@NotNull RefreshLayout refreshlayout) {
                mPresenter.LoadList2();
            }
        });

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public void AutoRefresh1() {
        getBinding().jkrvList.scrollToPosition(0);
        getBinding().jkrRefresh1.autoRefresh();
    }

    @Override
    public void AutoRefresh2() {
        getBinding().jkrvList2.smoothScrollToPosition(0);
        getBinding().jkrRefresh2.autoRefresh();
    }

    @Override
    public void RefreshComplete1() {
        getBinding().jkrRefresh1.finishRefresh();
    }

    @Override
    public void RefreshComplete2() {
        getBinding().jkrRefresh2.finishRefresh();
    }

    @Override
    public void LoadMoreComplete1(boolean bHasMore) {
        getBinding().jkrRefresh1.finishLoadMore(true);
        getBinding().jkrRefresh1.setNoMoreData(!bHasMore);
    }

    @Override
    public void LoadMoreComplete2(boolean bHasMore) {
        getBinding().jkrRefresh2.finishLoadMore(true);
        getBinding().jkrRefresh2.setNoMoreData(!bHasMore);
    }

    @Override
    public void LoadMoreError1(String tErrorMsg) {
        getBinding().jkrRefresh1.finishLoadMore(false);
        JKToast.ShowLongToast(tErrorMsg);
    }

    @Override
    public void LoadMoreError2(String tErrorMsg) {
        getBinding().jkrRefresh2.finishLoadMore(false);
        JKToast.ShowLongToast(tErrorMsg);
    }

    @Override
    public void UpdateList1() {
        aflaAdapter1.notifyDataSetChanged();
    }

    @Override
    public void UpdateList2() {
        aflaAdapter2.notifyDataSetChanged();
    }

    @Override
    public void SetList1(ArrayList<AFPlayingBean> a_afpdData) {
        aflaAdapter1.Update(a_afpdData);
    }

    @Override
    public void SetList2(ArrayList<AFPlayingBean> a_afpdData) {
        aflaAdapter2.Update(a_afpdData);
    }
}
