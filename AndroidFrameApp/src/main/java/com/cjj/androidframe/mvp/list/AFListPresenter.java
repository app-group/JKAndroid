package com.cjj.androidframe.mvp.list;

import static com.google.common.base.Preconditions.checkNotNull;
import static autodispose2.AutoDispose.autoDisposable;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.data.model.AFListModel;
import com.cjj.androidframe.data.response.AFPlayingListResponse;
import com.cjj.androidframe.net.AFNetSend;
import com.jkframework.control.JKToast;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.HttpException;


/**
 * Created by 2016/7/5.
 */
public class AFListPresenter implements AFListContract.Presenter{

    private AFListContract.View mView;
    private AFListModel mModel;

    public AFListPresenter(AFListContract.View view, AFListModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public void LoadList1() {
        mModel.setPage1(1);

        AFNetSend.RequestPlaying(mModel.getPage1())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFPlayingListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFPlayingListResponse afPlayingListResponse) {
                        mView.RefreshComplete1();
                        if (afPlayingListResponse.getStatus().equals("1")) {
                            mModel.getList1().clear();
                            mModel.getList1().addAll(afPlayingListResponse.getResult());
                            mView.UpdateList1();
                            mModel.setPage1(mModel.getPage1() + 1);
                            if (afPlayingListResponse.getResult().size() < 5)
                                mView.LoadMoreComplete1(false);
                            else {
                                mView.LoadMoreComplete1(true);
                            }
                        } else {
                            JKToast.ShowLongToast(afPlayingListResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.RefreshComplete1();
                        if (e instanceof HttpException || e instanceof ConnectException) {
                            JKToast.ShowLongToast("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            JKToast.ShowLongToast("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            JKToast.ShowLongToast("连接超时");
                        } else if (!(e instanceof CancellationException)) {
                            JKToast.ShowLongToast("数据异常");
                        }
                    }
                });
    }

    @Override
    public void LoadNextPage1() {
        AFNetSend.RequestPlaying(mModel.getPage1())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFPlayingListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFPlayingListResponse afPlayingListResponse) {
                        if (afPlayingListResponse.getStatus().equals("1")) {
                            mModel.getList1().addAll(afPlayingListResponse.getResult());
                            mView.UpdateList1();
                            mModel.setPage1(mModel.getPage1() + 1);
                            if (afPlayingListResponse.getResult().size() < 5)
                                mView.LoadMoreComplete1(false);
                            else {
                                mView.LoadMoreComplete1(true);
                            }
                        } else {
                            mView.LoadMoreError1(afPlayingListResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (e instanceof HttpException || e instanceof ConnectException) {
                            mView.LoadMoreError2("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            mView.LoadMoreError2("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            mView.LoadMoreError2("连接超时");
                        } else if (!(e instanceof CancellationException)){
                            mView.LoadMoreError2("数据异常");
                        }
                    }
                });
    }

    @Override
    public void LoadList2() {
        mModel.setPage2(1);
        AFNetSend.RequestPlaying(mModel.getPage2())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFPlayingListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFPlayingListResponse afPlayingListResponse) {
                        mView.RefreshComplete2();
                        if (afPlayingListResponse.getStatus().equals("1")) {
                            mModel.getList2().clear();
                            mModel.getList2().addAll(afPlayingListResponse.getResult());
                            mView.UpdateList2();
                            mModel.setPage2(mModel.getPage2() + 1);
                            if (afPlayingListResponse.getResult().size() < 5)
                                mView.LoadMoreComplete2(false);
                            else {
                                mView.LoadMoreComplete2(true);
                            }
                        } else {
                            JKToast.ShowLongToast(afPlayingListResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.RefreshComplete2();
                        if (e instanceof HttpException) {
                            JKToast.ShowLongToast("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            JKToast.ShowLongToast("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            JKToast.ShowLongToast("连接超时");
                        } else if (!(e instanceof CancellationException)) {
                            JKToast.ShowLongToast("数据异常");
                        }
                    }
                });
    }

    @Override
    public void LoadNextPage2() {
        AFNetSend.RequestPlaying(mModel.getPage2())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFPlayingListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFPlayingListResponse afPlayingListResponse) {
                        if (afPlayingListResponse.getStatus().equals("1")) {
                            mModel.getList2().addAll(afPlayingListResponse.getResult());
                            mView.UpdateList2();
                            mModel.setPage2(mModel.getPage2() + 1);
                            if (afPlayingListResponse.getResult().size() < 5)
                                mView.LoadMoreComplete2(false);
                            else {
                                mView.LoadMoreComplete2(true);
                            }
                        } else {
                            mView.LoadMoreError2(afPlayingListResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (e instanceof HttpException) {
                            mView.LoadMoreError2("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            mView.LoadMoreError2("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            mView.LoadMoreError2("连接超时");
                        } else if (!(e instanceof CancellationException)){
                            mView.LoadMoreError2("数据异常");
                        }
                    }
                });
    }

    @Override
    public ArrayList<AFPlayingBean> GetList1() {
        return mModel.getList1();
    }

    @Override
    public ArrayList<AFPlayingBean> GetList2() {
        return mModel.getList2();
    }

    @Override
    public AFListModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFListModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
        this.mView.SetList1(mModel.getList1());
        this.mView.SetList2(mModel.getList2());
    }

    @Override
    public void Init() {
        mView.AutoRefresh1();
        mView.AutoRefresh2();
        Start();
    }
}