package com.cjj.androidframe.mvp.loading;


import android.os.Bundle;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.AndroidframeLoadingactivityBindingImpl;
import com.cjj.androidframe.mvp.AFBaseActivity;
import com.jkframework.debug.JKDebug;
import com.jkframework.manager.JKActivityManager;

public class AFLoadingActivity extends AFBaseActivity<AndroidframeLoadingactivityBindingImpl> {

    @Override
    protected int getLayout() {
        return R.layout.androidframe_loadingactivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        JKDebug.Activation();
        super.onCreate(savedInstanceState);
    }

        @Override
        public void onBackPressedSupport() {
            NavController nav = Navigation.findNavController(this, R.id.main_fragment_host);
            if (nav.getCurrentDestination() != null && nav.getCurrentDestination().getId() != R.id.mainFragment) {
                nav.navigateUp();
            } else {
                JKActivityManager.Exit();
            }
        }

    @Override
    public void finish() {
        super.finish();
    }

}
