package com.cjj.androidframe.mvp.loading;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.fragment.NavHostFragment;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFLoadingModel;
import com.cjj.androidframe.databinding.AndroidframeLoadingfragmentBinding;
import com.cjj.androidframe.mvp.AFBaseFragment;

import org.jetbrains.annotations.NotNull;


public class AFLoadingFragment extends AFBaseFragment<AndroidframeLoadingfragmentBinding> implements AFLoadingContract.View {

    /**
     * persent
     */
    private AFLoadingPresenter mPresenter;
    private AFLoadingActivity activity;


    @Override
    protected int getLayout() {
        return R.layout.androidframe_loadingfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        activity = (AFLoadingActivity) context;//保存Context引用
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.CheckFinish();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFLoadingPresenter(this, new AFLoadingModel());

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public boolean IsBackground() {
        return activity.IsBackground();
    }

    @Override
    public void GoToHomeActivity() {
//        ReplaceActivity(AFMainActivity.class);
        NavHostFragment.findNavController(this).navigate(R.id.action_loadingFragment_mainFragment);
//        ReplaceActivity(AFTestActivity.class);
    }
}
