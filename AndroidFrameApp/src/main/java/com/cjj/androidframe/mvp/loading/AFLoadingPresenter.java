package com.cjj.androidframe.mvp.loading;

import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.data.model.AFLoadingModel;

import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;

import static autodispose2.AutoDispose.autoDisposable;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Created by 2016/7/5.
 */
public class AFLoadingPresenter implements AFLoadingContract.Presenter {

    private AFLoadingContract.View mView;
    private AFLoadingModel mModel;

    public AFLoadingPresenter(AFLoadingContract.View view, AFLoadingModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFLoadingModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFLoadingModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
    }

    @Override
    public void Init() {
        Start();
        CheckUpdate();
        WaitLogo();
    }

    @Override
    public void CheckUpdate() {
        mModel.setLoading(true);
        CheckFinish();
    }

    @Override
    public void WaitLogo() {
        Flowable.timer(2, TimeUnit.SECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) {
                        mModel.setWait(true);
                        CheckFinish();
                    }
                });
    }

    @Override
    public void CheckFinish() {
        if (mView.IsBackground())
            return;
        if (!mModel.isLoading() || !mModel.isWait()) {
            return;
        }

        mView.GoToHomeActivity();
    }
}