package com.cjj.androidframe.mvp.login;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFLoginModel;
import com.cjj.androidframe.databinding.AndroidframeLoginfragmentBinding;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.jakewharton.rxbinding4.view.RxView;
import com.jakewharton.rxbinding4.widget.RxTextView;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;

import static autodispose2.AutoDispose.autoDisposable;

public class AFLoginFragment extends AFBaseFragment<AndroidframeLoginfragmentBinding> implements AFLoginContract.View {

    /**
     * Presenter对象
     */
    private AFLoginPresenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_loginfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFLoginPresenter(this, new AFLoginModel());

        /*设置登录点击事件*/
        RxView.clicks(getBinding().jktvLogin)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        mPresenter.Login();
                    }
                });

        /*设置按钮状态*/
        Observable<Boolean> bInputAccount = RxTextView.textChanges(getBinding().jketAccount)
                .map(new Function<CharSequence, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull CharSequence charSequence) {
                        return !TextUtils.isEmpty(charSequence);
                    }

                });
        Observable<Boolean> bInputPassword = RxTextView.textChanges(getBinding().jketPassword)
                .map(new Function<CharSequence, Boolean>() {
                    @Override
                    public Boolean apply(@NonNull CharSequence charSequence) {
                        return !TextUtils.isEmpty(charSequence);
                    }
                });

        Observable.combineLatest(bInputAccount, bInputPassword, new BiFunction<Boolean, Boolean, Boolean>() {
            @Override
            public Boolean apply(@NonNull Boolean aBoolean, @NonNull Boolean aBoolean2) {
                return aBoolean && aBoolean2;
            }
        }).to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@NonNull Boolean aBoolean) {
                        getBinding().jktvLogin.setEnabled(aBoolean);
                    }
                });

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public void SetAccount(String tText) {
        getBinding().jketAccount.setText(tText);
    }

    @Override
    public void SetPassword(String tText) {
        getBinding().jketPassword.setText(tText);
    }

    @Override
    public void SetResult(String tText) {
        getBinding().jktvReslt.setText(tText);
    }

    @Override
    public String GetAccount() {
        return getBinding().jketAccount.getText().toString();
    }

    @Override
    public String GetPassword() {
        return getBinding().jketPassword.getText().toString();
    }

    @Override
    public String GetResult() {
        return getBinding().jktvReslt.getText().toString();
    }
}
