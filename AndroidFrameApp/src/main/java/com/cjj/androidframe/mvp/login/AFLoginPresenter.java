package com.cjj.androidframe.mvp.login;

import static com.google.common.base.Preconditions.checkNotNull;
import static autodispose2.AutoDispose.autoDisposable;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.data.model.AFLoginModel;
import com.cjj.androidframe.data.response.AFLoginResponse;
import com.cjj.androidframe.net.AFNetSend;
import com.jkframework.control.JKToast;
import com.jkframework.serialization.JKJsonSerialization;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.CancellationException;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Created by 2016/7/5.
 */
public class AFLoginPresenter implements AFLoginContract.Presenter {

    private AFLoginContract.View mView;
    private AFLoginModel mModel;

    public AFLoginPresenter(AFLoginContract.View view, AFLoginModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFLoginModel SaveData() {
        mModel.setAccount(mView.GetAccount());
        mModel.setPassword(mView.GetPassword());
        mModel.setResult(mView.GetResult());
        return mModel;
    }

    @Override
    public void LoadData(AFLoginModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
        mView.SetAccount(mModel.getAccount());
        mView.SetPassword(mModel.getPassword());
        mView.SetResult(mModel.getResult());
    }

    @Override
    public void Init() {
        Start();
    }

    @Override
    public void Login() {
        mView.LockScreen("正在登录...");
        AFNetSend.Login(mView.GetAccount(), mView.GetPassword())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFLoginResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFLoginResponse afLoginResponse) {
                        mView.UnlockScreen();
                        mView.SetResult(JKJsonSerialization.GetString(afLoginResponse));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.UnlockScreen();
                        if (e instanceof HttpException || e instanceof ConnectException) {
                            JKToast.ShowLongToast("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            JKToast.ShowLongToast("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            JKToast.ShowLongToast("连接超时");
                        } else if (!(e instanceof CancellationException)) {
                            JKToast.ShowLongToast("数据异常");
                        }
                    }
                });
    }
}