/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvp.main;


import com.cjj.androidframe.data.model.AFMainModel;
import com.cjj.androidframe.data.model.AFPageModel;
import com.cjj.androidframe.mvp.BasePresenter;
import com.cjj.androidframe.mvp.BaseView;

public interface AFMainContract {

    interface View extends BaseView{
        ///设置界面

        ///从界面上读取属性
    }

    interface Presenter extends BasePresenter<AFMainModel> {


    }
}
