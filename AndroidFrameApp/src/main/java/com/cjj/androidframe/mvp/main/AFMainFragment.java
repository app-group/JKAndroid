package com.cjj.androidframe.mvp.main;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.fragment.NavHostFragment;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFMainModel;
import com.cjj.androidframe.databinding.AndroidframeMainfragmentBinding;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.jakewharton.rxbinding4.view.RxView;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.functions.Consumer;

import static autodispose2.AutoDispose.autoDisposable;

public class AFMainFragment extends AFBaseFragment<AndroidframeMainfragmentBinding> implements AFMainContract.View {

    private AFMainPresenter mPresenter;

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.androidframe_mainfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFMainPresenter(this, new AFMainModel());

        /*设置登录demo点击事件*/
        RxView.clicks(getBinding().jktvLoginDemo)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_loginFragment);
                        }
                    }
                });
        /*设置列表demo点击事件*/
        RxView.clicks(getBinding().jktvListDemo)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_listFragment);
                        }
                    }
                });
        /*设置广告demo点击事件*/
        RxView.clicks(getBinding().jktvPagerDemo)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_pageFragment);
                        }
                    }
                });
        /*设置动态添加界面demo点击事件*/
        RxView.clicks(getBinding().jktvAddDemo)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_addFragment);
                        }
                    }
                });
        /*设置瀑布流界面demo点击事件*/
        RxView.clicks(getBinding().jktvStaggeredDemo)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_staggeredFragment);
                        }
                    }
                });
        /*设置tab切换demo点击事件*/
        RxView.clicks(getBinding().jktvTabDemo)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_tabFragment);
                        }
                    }
                });
        /*设置选择界面点击事件*/
        RxView.clicks(getBinding().jktvSelectDemo)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) {
                        if (NavHostFragment.findNavController(AFMainFragment.this).getCurrentDestination().getId() == R.id.mainFragment) {
                            NavHostFragment.findNavController(AFMainFragment.this).navigate(R.id.action_mainFragment_to_selectFragment);
                        }
                    }
                });


        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

}
