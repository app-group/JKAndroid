package com.cjj.androidframe.mvp.main;

import com.cjj.androidframe.data.model.AFMainModel;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFMainPresenter implements AFMainContract.Presenter{

    private AFMainContract.View mView;
    private AFMainModel mModel;

    public AFMainPresenter(AFMainContract.View view, AFMainModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFMainModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFMainModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
    }

    @Override
    public void Init() {
        Start();
    }
}