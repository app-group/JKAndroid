package com.cjj.androidframe.mvp.page;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.R;
import com.cjj.androidframe.adapter.AFPage2Adapter;
import com.cjj.androidframe.data.model.AFPageModel;
import com.cjj.androidframe.databinding.AndroidframePagefragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.jakewharton.rxbinding4.view.RxView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.functions.Consumer;

import static autodispose2.AutoDispose.autoDisposable;


public class AFPageFragment extends AFBaseFragment<AndroidframePagefragmentBindingImpl> implements AFPageContract.View {


    /**
     * Presenter对象
     */
    private AFPagePresenter mPresenter;
    private AFPage2Adapter hmwaAdapter2;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_pagefragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFPagePresenter(this, new AFPageModel());

        RxView.clicks(getBinding().cbMove)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        mPresenter.MoveToCurrentItem2();
                    }

                });

        RxView.clicks(getBinding().cbUpdate)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        mPresenter.UpdateViewPager();
                    }

                });

        ArrayList<Integer> a_tList2 = new ArrayList<>();
        a_tList2.add(R.drawable.hairmanager_weizhushoubanner1_all_0);
        a_tList2.add(R.drawable.hairmanager_weizhushoubanner2_all_0);
        a_tList2.add(R.drawable.hairmanager_weizhushoubanner3_all_0);
        hmwaAdapter2 = new AFPage2Adapter(a_tList2);
        getBinding().jkvpBanner2.setAdapter(hmwaAdapter2);

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public void UpdateViewPager() {
        hmwaAdapter2.notifyDataSetChanged();
    }

    @Override
    public void MoveTo2() {
        getBinding().jkvpBanner2.setCurrentItem(1, true);
    }
}
