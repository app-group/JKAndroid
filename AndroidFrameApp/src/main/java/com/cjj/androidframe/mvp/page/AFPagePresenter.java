package com.cjj.androidframe.mvp.page;

import com.cjj.androidframe.data.model.AFPageModel;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFPagePresenter implements AFPageContract.Presenter{

    private AFPageContract.View mView;
    private AFPageModel mModel;

    public AFPagePresenter(AFPageContract.View view, AFPageModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public void UpdateViewPager()
    {
        mView.UpdateViewPager();
    }

    @Override
    public void MoveToCurrentItem2()
    {
        mView.MoveTo2();
    }

    @Override
    public AFPageModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFPageModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {

    }

    @Override
    public void Init() {
        Start();
    }
}