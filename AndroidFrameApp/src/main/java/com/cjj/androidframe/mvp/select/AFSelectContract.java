/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvp.select;


import androidx.fragment.app.Fragment;

import com.cjj.androidframe.data.model.AFSelectModel;
import com.cjj.androidframe.mvp.BasePresenter;
import com.cjj.androidframe.mvp.BaseView;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;

public interface AFSelectContract {

    interface View extends BaseView{
        ///设置界面
        void SetImage(String tPath);
        void SetFile(String tPath);

        ///从界面上读取属性
    }

    interface Presenter extends BasePresenter<AFSelectModel> {

        void SelectImage(ArrayList<LocalMedia> a_tmPictureList);
        void SelectFile(String tPath);
        void PreviewImage(Fragment fragment);
    }
}
