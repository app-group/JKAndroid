package com.cjj.androidframe.mvp.select;

import static autodispose2.AutoDispose.autoDisposable;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFSelectModel;
import com.cjj.androidframe.databinding.AndroidframeSelectfragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.jakewharton.rxbinding4.view.RxView;
import com.jkframework.algorithm.JKConvert;
import com.jkframework.algorithm.JKFile;
import com.jkframework.config.JKPictureSelector;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.engine.CropEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.functions.Consumer;

public class AFSelectFragment extends AFBaseFragment<AndroidframeSelectfragmentBindingImpl> implements AFSelectContract.View {

    /**
     * Presenter对象
     */
    private AFSelectPresenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_selectfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFSelectPresenter(this, new AFSelectModel());

        /*设置登录点击事件*/
        RxView.clicks(getBinding().jktvAlbum)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        PictureSelector.create(AFSelectFragment.this)
                                .openGallery(SelectMimeType.ofImage())
                                .setImageEngine(JKPictureSelector.createGlideEngine())
                                .setCropEngine(new CropEngine() {
                                    @Override
                                    public void onStartCrop(Fragment fragment, LocalMedia currentLocalMedia, ArrayList<LocalMedia> dataSource, int requestCode) {

                                    }
                                })
                                .forResult(new OnResultCallbackListener<LocalMedia>() {
                                    @Override
                                    public void onResult(ArrayList<LocalMedia> result) {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                    }
                });

        RxView.clicks(getBinding().jktvShot)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        PictureSelector.create(AFSelectFragment.this)
                                .openCamera(SelectMimeType.ofImage())
                                .setCropEngine(new CropEngine() {
                                    @Override
                                    public void onStartCrop(Fragment fragment, LocalMedia currentLocalMedia, ArrayList<LocalMedia> dataSource, int requestCode) {

                                    }
                                })
                                .forResult(new OnResultCallbackListener<LocalMedia>() {
                                    @Override
                                    public void onResult(ArrayList<LocalMedia> result) {

                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                });
                    }
                });

        RxView.clicks(getBinding().jkivImage)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        mPresenter.PreviewImage(AFSelectFragment.this);
                    }
                });

        RxView.clicks(getBinding().jktvChoiceFile)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Object>() {
                    @Override
                    public void accept(@NonNull Object o) throws Exception {
                        JKFile.ChoiceFilePath(new JKFile.JKChoiceListener() {
                            @Override
                            public void FinishChoice(ArrayList<Uri> pathList) {
                                if (pathList.size() > 0) {
                                    mPresenter.SelectFile(JKConvert.UriToPath(pathList.get(0)));
                                }
                            }
                        });
                    }
                });

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//                case PictureConfig.CHOOSE_REQUEST:
//                    // 图片选择结果回调
//                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
//                    // 例如 LocalMedia 里面返回三种path
//                    // 1.media.getPath(); 为原图path
//                    // 2.media.getCutPath();为裁剪后path，需判断media.isCut();是否为true
//                    // 3.media.getCompressPath();为压缩后path，需判断media.isCompressed();是否为true
//                    // 如果裁剪并压缩了，以取压缩路径为准，因为是先裁剪后压缩的
//                    mPresenter.SelectImage(selectList);
//                    break;
//            }
//        }
//    }

    @Override
    public void SetImage(String tPath) {
        getBinding().jkivImage.SetImageAsync(tPath);
    }

    @Override
    public void SetFile(String tPath) {
        getBinding().jktvPath.setText(tPath);
    }
}
