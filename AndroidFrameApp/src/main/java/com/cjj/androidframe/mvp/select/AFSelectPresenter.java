package com.cjj.androidframe.mvp.select;


import static com.google.common.base.Preconditions.checkNotNull;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.cjj.androidframe.data.model.AFSelectModel;
import com.jkframework.config.JKPictureSelector;
import com.jkframework.control.JKToast;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnExternalPreviewEventListener;

import java.util.ArrayList;

/**
 * Created by 2016/7/5.
 */
public class AFSelectPresenter implements AFSelectContract.Presenter {

    private AFSelectContract.View mView;
    private AFSelectModel mModel;

    public AFSelectPresenter(AFSelectContract.View view, AFSelectModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public void SelectImage(ArrayList<LocalMedia> a_tmPictureList) {
        mModel.setPictureList(a_tmPictureList);
        if (a_tmPictureList.size() > 0) {
            LocalMedia lmData = a_tmPictureList.get(0);
            if (lmData.isCut()) {
                mModel.setImagePath(lmData.getCutPath());
            } else
                mModel.setImagePath(lmData.getPath());
            mView.SetImage(mModel.getImagePath());
        }
    }

    @Override
    public void SelectFile(String tPath) {
        mModel.setFilePath(tPath);
        mView.SetFile(tPath);
    }

    @Override
    public void PreviewImage(Fragment fragment) {
        if (mModel.getPictureList().size() > 0)
            PictureSelector.create(fragment).openPreview()
                    .setImageEngine(JKPictureSelector.createGlideEngine())
                    .setExternalPreviewEventListener(new OnExternalPreviewEventListener() {
                        @Override
                        public void onPreviewDelete(int position) {

                        }

                        @Override
                        public boolean onLongPressDownload(Context context, LocalMedia media) {
                            return false;
                        }
                    }).startActivityPreview(0, true,mModel.getPictureList());
        else
            JKToast.ShowLongToast("请先加载图片");
    }

    @Override
    public AFSelectModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFSelectModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
        mView.SetImage(mModel.getImagePath());
        mView.SetFile(mModel.getFilePath());
    }

    @Override
    public void Init() {
        Start();
    }
}