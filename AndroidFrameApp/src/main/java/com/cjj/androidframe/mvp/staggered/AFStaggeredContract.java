/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvp.staggered;


import com.cjj.androidframe.data.bean.AFHairListBean;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.data.model.AFLoginModel;
import com.cjj.androidframe.data.model.AFStaggeredModel;
import com.cjj.androidframe.mvp.BasePresenter;
import com.cjj.androidframe.mvp.BaseView;

import java.util.ArrayList;

public interface AFStaggeredContract {

    interface View extends BaseView{
        void AutoRefresh();
        void RefreshComplete();
        void LoadMoreComplete(boolean bHasMore);
        void LoadMoreError(String tErrorMsg);

        ///设置界面
        void UpdateList();
        void UpdateRangeList(int nStart, int nCount);
        void SetList(ArrayList<AFHairListBean> a_afhbData);

        ///从界面上读取属性
    }

    interface Presenter extends BasePresenter<AFStaggeredModel> {

        void LoadList();
        void LoadNextPage();
        ArrayList<AFHairListBean> GetList();
    }
}
