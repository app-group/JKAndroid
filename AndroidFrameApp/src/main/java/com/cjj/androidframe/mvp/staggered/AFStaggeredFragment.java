package com.cjj.androidframe.mvp.staggered;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.cjj.androidframe.R;
import com.cjj.androidframe.adapter.AFStaggeredAdapter;
import com.cjj.androidframe.data.bean.AFHairListBean;
import com.cjj.androidframe.data.model.AFStaggeredModel;
import com.cjj.androidframe.databinding.AndroidframeStaggeredfragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.jkframework.control.JKToast;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshLoadMoreListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AFStaggeredFragment extends AFBaseFragment<AndroidframeStaggeredfragmentBindingImpl> implements AFStaggeredContract.View {

    /**
     * Presenter对象
     */
    private AFStaggeredPresenter mPresenter;
    /**
     * 列表1适配器
     */
    private AFStaggeredAdapter afsaAdapter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_staggeredfragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFStaggeredPresenter(this, new AFStaggeredModel());

        afsaAdapter = new AFStaggeredAdapter(mPresenter.GetList());
        final StaggeredGridLayoutManager sglmManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        sglmManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        getBinding().jkrvList.setLayoutManager(sglmManager);
//        jkrvList.addItemDecoration(
//                new HorizontalDividerItemDecoration.Builder(getContext())
//                        .color(0xffcccccc)
//                        .size(2)
//                        .build());
        getBinding().jkrvList.setAdapter(afsaAdapter);
//        jkrvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                sglmManager.invalidateSpanAssignments();
//            }
//        });
//        AFLoadMoreFooter aflmfFooter = AFLoadMoreFooter_.build(getContext());
//        jkrvList.SetHeaderView(aflmfFooter);

//        AFRefreshHeader afrhHeader = AFRefreshHeader_.build(getContext());
//        jkrRefresh.SetView(afrhHeader);
//        jkrRefresh.AddRefreshUIHandler(afrhHeader);

//        AFLoadMoreFooter aflmfFooter = AFLoadMoreFooter_.build(getContext());
//        jkrvList.SetLoadMoreListener(new JKRecyclerView.JKLoadMoreListener() {
//            @Override
//            public void onLoadMoreBegin(final JKRecyclerView container) {
//                mPresenter.LoadNextPage();
//            }
//        });
//        jkrvList.SetLoadMoreView(aflmfFooter);
//        jkrvList.AddLoadMoreUIHandler(aflmfFooter);
        getBinding().jkrRefresh.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NotNull RefreshLayout refreshlayout) {
                mPresenter.LoadNextPage();
            }

            @Override
            public void onRefresh(@NotNull RefreshLayout refreshlayout) {
                mPresenter.LoadList();
            }
        });

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public void AutoRefresh() {
        getBinding().jkrvList.scrollToPosition(0);
        getBinding().jkrRefresh.autoRefresh();
    }

    @Override
    public void RefreshComplete() {
        getBinding().jkrRefresh.finishRefresh();
    }

    @Override
    public void LoadMoreComplete(boolean bHasMore) {
        getBinding().jkrRefresh.finishLoadMore(true);
        getBinding().jkrRefresh.setNoMoreData(!bHasMore);
    }

    @Override
    public void LoadMoreError(String tErrorMsg) {
        getBinding().jkrRefresh.finishLoadMore(false);
        JKToast.ShowLongToast(tErrorMsg);
    }

    @Override
    public void UpdateList() {
        afsaAdapter.notifyDataSetChanged();
    }

    @Override
    public void UpdateRangeList(int nStart, int nCount) {
        afsaAdapter.notifyItemRangeChanged(nStart, nCount);
    }

    @Override
    public void SetList(ArrayList<AFHairListBean> a_afpdData) {
        afsaAdapter.Update(a_afpdData);
    }

}
