package com.cjj.androidframe.mvp.staggered;

import static com.google.common.base.Preconditions.checkNotNull;
import static autodispose2.AutoDispose.autoDisposable;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;

import com.cjj.androidframe.data.bean.AFHairListBean;
import com.cjj.androidframe.data.model.AFStaggeredModel;
import com.cjj.androidframe.data.response.AFHairListResponse;
import com.cjj.androidframe.net.AFNetSend;
import com.jkframework.control.JKToast;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.HttpException;


/**
 * Created by 2016/7/5.
 */
public class AFStaggeredPresenter implements AFStaggeredContract.Presenter {

    private AFStaggeredContract.View mView;
    private AFStaggeredModel mModel;

    public AFStaggeredPresenter(AFStaggeredContract.View view, AFStaggeredModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public void LoadList() {
        mModel.setPage(1);

        AFNetSend.RequestHairList(mModel.getPage())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFHairListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFHairListResponse afHairListResponse) {
                        mView.RefreshComplete();
                        if (afHairListResponse.getStatus().equals("1")) {
                            mModel.getList().clear();
                            mModel.getList().addAll(afHairListResponse.getResult());
                            mView.UpdateList();
                            mModel.setPage(mModel.getPage() + 1);
                            if (afHairListResponse.getResult().size() < 5)
                                mView.LoadMoreComplete(false);
                            else {
                                mView.LoadMoreComplete(true);
                            }
                        }
                        else {
                            JKToast.ShowLongToast(afHairListResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mView.RefreshComplete();
                        if (e instanceof HttpException || e instanceof ConnectException) {
                            JKToast.ShowLongToast("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            JKToast.ShowLongToast("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            JKToast.ShowLongToast("连接超时");
                        } else if (!(e instanceof CancellationException)) {
                            JKToast.ShowLongToast("数据异常");
                        }
                    }
                });
    }

    @Override
    public void LoadNextPage() {
        AFNetSend.RequestHairList(mModel.getPage())
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(mView.GetLifecycle(), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFHairListResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AFHairListResponse afHairListResponse) {
                        if (afHairListResponse.getStatus().equals("1")) {
                            int nStart = mModel.getList().size();
                            mModel.getList().addAll(afHairListResponse.getResult());
                            mView.UpdateRangeList(nStart,afHairListResponse.getResult().size());
                            mModel.setPage(mModel.getPage() + 1);
                            if (afHairListResponse.getResult().size() < 5)
                                mView.LoadMoreComplete(false);
                            else {
                                mView.LoadMoreComplete(true);
                            }
                        }
                        else {
                            mView.LoadMoreError(afHairListResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (e instanceof HttpException || e instanceof ConnectException) {
                            mView.LoadMoreError("网络异常");
                        } else if (e instanceof UnknownHostException) {
                            mView.LoadMoreError("地址不存在");
                        } else if (e instanceof SocketTimeoutException) {
                            mView.LoadMoreError("连接超时");
                        } else if (!(e instanceof CancellationException)){
                            mView.LoadMoreError("数据异常");
                        }
                    }
                });
    }

    @Override
    public ArrayList<AFHairListBean> GetList()
    {
        return mModel.getList();
    }

    @Override
    public AFStaggeredModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFStaggeredModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
        mView.SetList(mModel.getList());
    }

    @Override
    public void Init() {
        Start();
        mView.AutoRefresh();
    }
}