package com.cjj.androidframe.mvp.tab;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFTabModel;
import com.cjj.androidframe.databinding.AndroidframeTabfragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;
import com.cjj.androidframe.mvp.tab.bottom1.AFBottom1Fragment;
import com.cjj.androidframe.mvp.tab.bottom2.AFBottom2Fragment;
import com.cjj.androidframe.mvp.tab.bottom3.AFBottom3Fragment;
import com.cjj.androidframe.mvp.tab.bottom4.AFBottom4Fragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.jkframework.config.JKSystem;

import org.jetbrains.annotations.NotNull;

import static com.google.android.material.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;

public class AFTabFragment extends AFBaseFragment<AndroidframeTabfragmentBindingImpl> implements AFTabContract.View {

    /**
     * Presenter对象
     */
    private AFTabPresenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_tabfragment;
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFTabPresenter(this, new AFTabModel());

        getBinding().jkbnvBottom.setItemHorizontalTranslationEnabled(false);
        getBinding().jkbnvBottom.setLabelVisibilityMode(NavigationBarView.LABEL_VISIBILITY_LABELED);
        getBinding().jkbnvBottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.miBottom1)
                    mPresenter.SelectBottom(0);
                else if (item.getItemId() == R.id.miBottom2)
                    mPresenter.SelectBottom(1);
                else if (item.getItemId() == R.id.miBottom3)
                    mPresenter.SelectBottom(2);
                else if (item.getItemId() == R.id.miBottom4)
                    mPresenter.SelectBottom(3);
                return true;
            }
        });

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

    @Override
    public void SelectBottom(int nIndex) {
        FragmentTransaction ftTransaction = getChildFragmentManager().beginTransaction();
        getChildFragmentManager().getFragments();
        for (int i = 0; i < getChildFragmentManager().getFragments().size(); ++i) {
            ftTransaction.hide(getChildFragmentManager().getFragments().get(i));
        }
        Fragment fgFragment;
        String tTag;
        switch (nIndex) {
            default:
            case 0:
                tTag = "AFTabFragment_0";
                fgFragment = getChildFragmentManager().findFragmentByTag(tTag);
                if (fgFragment == null)
                    fgFragment = new AFBottom1Fragment();
                break;
            case 1:
                tTag = "AFTabFragment_1";
                fgFragment = getChildFragmentManager().findFragmentByTag(tTag);
                if (fgFragment == null)
                    fgFragment = new AFBottom2Fragment();
                break;
            case 2:
                tTag = "AFTabFragment_2";
                fgFragment = getChildFragmentManager().findFragmentByTag(tTag);
                if (fgFragment == null)
                    fgFragment = new AFBottom3Fragment();
                break;
            case 3:
                tTag = "AFTabFragment_3";
                fgFragment = getChildFragmentManager().findFragmentByTag(tTag);
                if (fgFragment == null)
                    fgFragment = new AFBottom4Fragment();
                break;
        }
        if (!fgFragment.isAdded()) {
            ftTransaction.add(R.id.vfMain, fgFragment, tTag);
            ftTransaction.show(fgFragment);
            ftTransaction.commitAllowingStateLoss();
            JKSystem.CloseKeyboard();
        } else {
            ftTransaction.show(fgFragment);
            ftTransaction.commitAllowingStateLoss();
            JKSystem.CloseKeyboard();
        }
    }
}
