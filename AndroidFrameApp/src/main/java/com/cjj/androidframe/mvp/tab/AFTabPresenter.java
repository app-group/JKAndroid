package com.cjj.androidframe.mvp.tab;


import com.cjj.androidframe.data.model.AFTabModel;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFTabPresenter implements AFTabContract.Presenter{

    private AFTabContract.View mView;
    private AFTabModel mModel;

    public AFTabPresenter(AFTabContract.View view, AFTabModel model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }


    @Override
    public AFTabModel SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFTabModel model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
        SelectBottom(0);
    }

    @Override
    public void Init() {
        mModel.setIndex(0);
        Start();
    }

    @Override
    public void SelectBottom(int nIndex) {
        mView.SelectBottom(nIndex);
    }
}