package com.cjj.androidframe.mvp.tab.bottom1;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFBottom1Model;
import com.cjj.androidframe.databinding.AndroidframeBottom1fragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;

import org.jetbrains.annotations.NotNull;

public class AFBottom1Fragment extends AFBaseFragment<AndroidframeBottom1fragmentBindingImpl> implements AFBottom1Contract.View {

    private AFBottom1Presenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_bottom1fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFBottom1Presenter(this, new AFBottom1Model());

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

}
