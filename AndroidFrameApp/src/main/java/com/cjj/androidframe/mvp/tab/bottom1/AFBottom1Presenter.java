package com.cjj.androidframe.mvp.tab.bottom1;

import com.cjj.androidframe.data.model.AFBottom1Model;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFBottom1Presenter implements AFBottom1Contract.Presenter{

    private AFBottom1Contract.View mView;
    private AFBottom1Model mModel;

    public AFBottom1Presenter(AFBottom1Contract.View view, AFBottom1Model model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFBottom1Model SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFBottom1Model model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
    }

    @Override
    public void Init() {
        Start();
    }
}