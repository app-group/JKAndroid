package com.cjj.androidframe.mvp.tab.bottom2;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFBottom2Model;
import com.cjj.androidframe.databinding.AndroidframeBottom2fragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;

import org.jetbrains.annotations.NotNull;

public class AFBottom2Fragment extends AFBaseFragment<AndroidframeBottom2fragmentBindingImpl> implements AFBottom2Contract.View {

    private AFBottom2Presenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_bottom2fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFBottom2Presenter(this,new AFBottom2Model());

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

}
