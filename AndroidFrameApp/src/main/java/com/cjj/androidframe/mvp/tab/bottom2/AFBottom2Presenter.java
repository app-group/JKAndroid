package com.cjj.androidframe.mvp.tab.bottom2;

import com.cjj.androidframe.data.model.AFBottom2Model;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFBottom2Presenter implements AFBottom2Contract.Presenter{

    private AFBottom2Contract.View mView;
    private AFBottom2Model mModel;

    public AFBottom2Presenter(AFBottom2Contract.View view, AFBottom2Model model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFBottom2Model SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFBottom2Model model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
    }

    @Override
    public void Init() {
        Start();
    }
}