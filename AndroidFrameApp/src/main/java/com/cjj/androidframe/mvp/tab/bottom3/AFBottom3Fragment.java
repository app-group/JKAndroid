package com.cjj.androidframe.mvp.tab.bottom3;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFBottom3Model;
import com.cjj.androidframe.databinding.AndroidframeBottom3fragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;

import org.jetbrains.annotations.NotNull;



public class AFBottom3Fragment extends AFBaseFragment<AndroidframeBottom3fragmentBindingImpl> implements AFBottom3Contract.View {

    private AFBottom3Presenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_bottom3fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFBottom3Presenter(this,new AFBottom3Model());

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

}
