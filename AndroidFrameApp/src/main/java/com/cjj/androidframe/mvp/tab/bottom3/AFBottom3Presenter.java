package com.cjj.androidframe.mvp.tab.bottom3;

import com.cjj.androidframe.data.model.AFBottom1Model;
import com.cjj.androidframe.data.model.AFBottom3Model;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFBottom3Presenter implements AFBottom3Contract.Presenter{

    private AFBottom3Contract.View mView;
    private AFBottom3Model mModel;

    public AFBottom3Presenter(AFBottom3Contract.View view, AFBottom3Model model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFBottom3Model SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFBottom3Model model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
    }

    @Override
    public void Init() {
        Start();
    }
}