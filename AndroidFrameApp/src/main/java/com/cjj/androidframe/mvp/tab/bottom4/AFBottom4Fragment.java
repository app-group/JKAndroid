package com.cjj.androidframe.mvp.tab.bottom4;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.model.AFBottom4Model;
import com.cjj.androidframe.databinding.AndroidframeBottom4fragmentBindingImpl;
import com.cjj.androidframe.mvp.AFBaseFragment;

import org.jetbrains.annotations.NotNull;

public class AFBottom4Fragment extends AFBaseFragment<AndroidframeBottom4fragmentBindingImpl> implements AFBottom4Contract.View {

    private AFBottom4Presenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.androidframe_bottom4fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new AFBottom4Presenter(this,new AFBottom4Model());

        if (!bInit) {
            bInit = true;
            mPresenter.Init();
        } else {
            bRecycle = true;
        }
    }

}
