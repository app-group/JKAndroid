package com.cjj.androidframe.mvp.tab.bottom4;

import com.cjj.androidframe.data.model.AFBottom3Model;
import com.cjj.androidframe.data.model.AFBottom4Model;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by 2016/7/5.
 */
public class AFBottom4Presenter implements AFBottom4Contract.Presenter{

    private AFBottom4Contract.View mView;
    private AFBottom4Model mModel;

    public AFBottom4Presenter(AFBottom4Contract.View view, AFBottom4Model model) {
        mView = checkNotNull(view);
        mModel = checkNotNull(model);
    }

    @Override
    public AFBottom4Model SaveData() {
        return mModel;
    }

    @Override
    public void LoadData(AFBottom4Model model) {
        this.mModel = checkNotNull(model);
        Start();
    }

    @Override
    public void Start() {
    }

    @Override
    public void Init() {
        Start();
    }
}