package com.cjj.androidframe.mvp.test;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cjj.androidframe.R;
import com.cjj.androidframe.adapter.AFTestAdapter;
import com.cjj.androidframe.data.bean.AFCraneBean;
import com.cjj.androidframe.data.response.AFCraneResponse;
import com.cjj.androidframe.net.AFNetSend;
import com.jkframework.manager.JKActivityManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

import static autodispose2.AutoDispose.autoDisposable;


public class AFTestActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.androidframe_testactivity);

        RecyclerView jkrvList = findViewById(R.id.jkrvList);

        ArrayList<AFCraneBean> list = new ArrayList<>();
        AFTestAdapter cmflaAdapter = new AFTestAdapter(list);
        jkrvList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        jkrvList.setAdapter(cmflaAdapter);

        AFNetSend.RequestCraneList()
                .subscribeOn(Schedulers.io())       //指定上个subscribe所在线程
                .observeOn(AndroidSchedulers.mainThread())  //指定subscribe回调时线程
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
                .subscribe(new SingleObserver<AFCraneResponse>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NotNull AFCraneResponse response) {
                        if (response.getRcode().equals("Y")) {
                            list.addAll(response.getList());
                            cmflaAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });

    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);

    }
}
