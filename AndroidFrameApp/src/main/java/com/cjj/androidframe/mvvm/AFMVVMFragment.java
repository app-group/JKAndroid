package com.cjj.androidframe.mvvm;

import androidx.databinding.ViewDataBinding;

import com.jkframework.fragment.JKMVVMFragment;

public abstract class AFMVVMFragment<E extends ViewDataBinding> extends JKMVVMFragment<E>{

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
