package com.cjj.androidframe.mvvm.add;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cjj.androidframe.data.bean.AFAddBean;

import java.util.ArrayList;
import java.util.List;

public class AddViewModel extends ViewModel {

    public final MutableLiveData<List<AFAddBean>> list = new MutableLiveData<>();
    public final MutableLiveData<Boolean> notifyCurrentListChanged = new MutableLiveData<>();

    {

    }
}