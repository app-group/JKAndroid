package com.cjj.androidframe.mvvm.list;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.net.request.WSMaintenanceRequest;

import java.util.List;

public class ListViewModel extends ViewModel {


    public final MutableLiveData<List<AFPlayingBean>> list = new MutableLiveData<>();

    public final MutableLiveData<Boolean> refresh = new MutableLiveData<>();

    public final WSMaintenanceRequest listRequest = new WSMaintenanceRequest();

}