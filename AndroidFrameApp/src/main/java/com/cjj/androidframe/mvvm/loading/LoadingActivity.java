package com.cjj.androidframe.mvvm.loading;


import android.os.Bundle;

import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.ActivityLoadingBindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMActivity;
import com.jkframework.config.JKDataBindingConfig;
import com.jkframework.debug.JKDebug;

public class LoadingActivity extends AFMVVMActivity<ActivityLoadingBindingImpl> {

    private LoadingActivityViewModel mLoadingActivityViewModel;
    private boolean mIsListened = false;

    @Override
    protected void initViewModel() {
        mLoadingActivityViewModel = getActivityViewModel(LoadingActivityViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {
        return new JKDataBindingConfig(R.layout.activity_loading, BR.vm, mLoadingActivityViewModel);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        JKDebug.Activation();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!mIsListened) {

            // TODO tip 2：此处演示通过 UnPeekLiveData 来发送 生命周期安全的、事件源可追溯的 通知。

            // 如果这么说还不理解的话，详见 https://xiaozhuanlan.com/topic/0168753249
            // --------
            // 与此同时，此处传达的另一个思想是 最少知道原则，
            // fragment 内部的事情在 fragment 内部消化，不要试图在 Activity 中调用和操纵 Fragment 内部的东西。
            // 因为 fragment 端的处理后续可能会改变，并且可受用于更多的 Activity，而不单单是本 Activity。

            mIsListened = true;
        }
    }
}
