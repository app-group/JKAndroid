/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvvm.login;

import static autodispose2.AutoDispose.autoDisposable;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.provider.ProviderProperties;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;

import com.blankj.utilcode.util.ActivityUtils;
import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.FragmentLoginBindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMFragment;
import com.cjj.androidframe.mvvm.SharedViewModel;
import com.jkframework.config.JKDataBindingConfig;
import com.jkframework.config.JKSystem;
import com.jkframework.control.JKToast;
import com.tbruyelle.rxpermissions3.RxPermissions;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;

/**
 * Create by KunMinX at 19/10/29
 */
public class LoginFragment extends AFMVVMFragment<FragmentLoginBindingImpl> {

    private LoginViewModel mLoginViewModel;
    private SharedViewModel mSharedViewModel;
    private LocationManager locationManager;

    @Override
    protected void initViewModel() {
        mLoginViewModel = getFragmentViewModel(LoginViewModel.class);
        mSharedViewModel = getActivityViewModel(SharedViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {

        //TODO tip: DataBinding 严格模式：
        // 将 DataBinding 实例限制于 base 页面中，默认不向子类暴露，
        // 通过这样的方式，来彻底解决 视图调用的一致性问题，
        // 如此，视图刷新的安全性将和基于函数式编程的 Jetpack Compose 持平。
        // 而 DataBindingConfig 就是在这样的背景下，用于为 base 页面中的 DataBinding 提供绑定项。

        // 如果这样说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350 和 https://xiaozhuanlan.com/topic/2356748910
        return new JKDataBindingConfig(R.layout.fragment_login, BR.vm, mLoginViewModel)
                .addBindingParam(BR.click, new ClickProxy());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        //        new RxPermissions(this)
//                .request(Manifest.permission.READ_PHONE_STATE)
//                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
//                .subscribe(granted -> {
//                    if (granted) { // Always true pre-M
//                        mMainViewModel.permissions.setValue(true);
//                    } else {
//                        showLongToast("请开启读取手机状态权限");
//                        ActivityUtils.finishAllActivities();
//                    }
//                });
//
//        Observable.just("").delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
//                .subscribe(item -> {
//                    mMainViewModel.wait.setValue(true);
//                });
//
//
//        mMainViewModel.permissions.observe(getViewLifecycleOwner(), s -> {
//            loading();
//        });
//
//        mMainViewModel.wait.observe(getViewLifecycleOwner(), s -> {
//            loading();
//        });
    }

    // TODO tip 2：此处通过 DataBinding 来规避 在 setOnClickListener 时存在的 视图调用的一致性问题，

    // 也即，有绑定就有绑定，没绑定也没什么大不了的，总之 不会因一致性问题造成 视图调用的空指针。
    // 如果这么说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350
    private final Handler locationUpdateHandler = new Handler();
    private final Runnable locationUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            // 创建一个 Location 对象并设置属性
            Location mockLocation = new Location(LocationManager.NETWORK_PROVIDER);
            mockLocation.setLatitude(30.45976);  // 纬度
            mockLocation.setLongitude(114.40583);  // 经度
            mockLocation.setAltitude(0);  // 海拔
            mockLocation.setTime(System.currentTimeMillis());
            mockLocation.setAccuracy(220);  // 精度
            mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());

            // 设置模拟位置
            locationManager.setTestProviderEnabled(LocationManager.NETWORK_PROVIDER, true);
            locationManager.setTestProviderLocation(LocationManager.NETWORK_PROVIDER, mockLocation);

            // 重新调度 Runnable，以便在 1 秒后再次运行
            locationUpdateHandler.postDelayed(this, 500);
        }
    };

    public class ClickProxy {

        public void Login() {
            if (mLoginViewModel.result.getValue() != null && mLoginViewModel.result.getValue().equals("开启"))
            {
                // 禁用虚拟定位的代码
                locationUpdateHandler.removeCallbacks(locationUpdateRunnable);
                locationManager.removeTestProvider(LocationManager.NETWORK_PROVIDER);
                mLoginViewModel.result.setValue("关闭");
                return;
            }

//            LocationListener locationListener = new LocationListener() {
//                public void onLocationChanged(Location location) {
//                    // 当新位置变化时调用
//                    double latitude = location.getLatitude();
//                    double longitude = location.getLongitude();
//                    double altitude = location.getAltitude();
//                    double accuracy = location.getAccuracy();
//                }
//
//                public void onStatusChanged(String provider, int status, Bundle extras) {
//                }
//
//                public void onProviderEnabled(String provider) {
//                }
//
//                public void onProviderDisabled(String provider) {
//                }
//            };


            new RxPermissions(mActivity)
                    .request(Manifest.permission.ACCESS_FINE_LOCATION)
                    .to(autoDisposable(AndroidLifecycleScopeProvider.from(mActivity, Lifecycle.Event.ON_DESTROY)))
                    .subscribe(granted -> {
                        if (granted) {
                            // 添加测试提供者
                            locationManager.addTestProvider(
                                    LocationManager.NETWORK_PROVIDER,
                                    false,
                                    false,
                                    false,
                                    false,
                                    true,
                                    true,
                                    true,
                                    ProviderProperties.POWER_USAGE_LOW,
                                    ProviderProperties.ACCURACY_FINE
                            );
                            // 开始周期性更新
                            locationUpdateHandler.post(locationUpdateRunnable);
                            mLoginViewModel.result.setValue("开启");
                        } else {
                            mLoginViewModel.result.setValue("没有权限");
                        }
                    }, throwable -> {
                        // handle error here
                        mLoginViewModel.result.setValue("Error: " + throwable.getMessage());
                    });
        }
    }
}
