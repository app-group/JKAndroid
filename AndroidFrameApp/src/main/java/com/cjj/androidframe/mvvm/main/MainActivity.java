package com.cjj.androidframe.mvvm.main;


import android.os.Bundle;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.blankj.utilcode.util.ActivityUtils;
import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.ActivityLoadingBindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMActivity;
import com.cjj.androidframe.mvvm.SharedViewModel;
import com.cjj.androidframe.mvvm.loading.LoadingActivityViewModel;
import com.jkframework.config.JKDataBindingConfig;
import com.jkframework.control.JKToast;
import com.jkframework.debug.JKDebug;

import java.util.concurrent.TimeUnit;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.functions.Function;

import static autodispose2.AutoDispose.autoDisposable;

public class MainActivity extends AFMVVMActivity<ActivityLoadingBindingImpl> {

    private LoadingActivityViewModel mLoadingActivityViewModel;
    private SharedViewModel mSharedViewModel;
    private boolean mIsListened = false;

    @Override
    protected void initViewModel() {
        mLoadingActivityViewModel = getActivityViewModel(LoadingActivityViewModel.class);
        mSharedViewModel = getActivityViewModel(SharedViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {
        return new JKDataBindingConfig(R.layout.activity_main, BR.vm, mLoadingActivityViewModel);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedViewModel.activityCanBeClosedDirectly.observe(this, aBoolean -> {
            NavController nav = Navigation.findNavController(this, R.id.main_host);
            if (nav.getCurrentDestination() != null && nav.getCurrentDestination().getId() != R.id.fragment_main) {
                nav.navigateUp();
            } else {
                mLoadingActivityViewModel.exit.onNext(1);
            }
        });

        mLoadingActivityViewModel.exit//.debounce(100, TimeUnit.MILLISECONDS)
                .mergeWith(mLoadingActivityViewModel.exit.debounce(2, TimeUnit.SECONDS).map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer i) throws Exception {
                        // 两次点击返回间隔大于2s的事件, 用0表示, 区分正常的点击
                        return 0;
                    }
                }))
                // 这一步转化是关键
                // 使用一个scan转换符去做处理, 如何当前事件是0, 则返回0
                // 否则则是上一个事件加一(这个就可以将所有前后间隔小于2s的事件标上序号, 方便后序订阅处理,
                // 拿到序号1的事件弹出提示, 拿到序号2的序列做返回, 甚至于可以处理3号, 4号),
                // 这样也就可以毫不费力地解决上诉说的要求2秒内点击三次才退出
                // 变成 (1 -> 0 -> 1 -> 0 -> 1 -> 2 -> 3 -> ...)
                .scan((prev, cur) -> {
                    if (cur == 0)
                        return 0;
                    return prev + 1;
                })
                // 过滤掉0, 后面我们只关心拿到的是几号的时间
                .filter(v -> v > 0)
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this)))
                .subscribe(v -> {
                    if (v == 1) {
                        // 弹出提示
                        JKToast.ShowLongToast("再按一次退出程序");
                    } else if (v == 2) {
                        // 返回
                        ActivityUtils.finishAllActivities(true);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        mSharedViewModel.activityCanBeClosedDirectly.setValue(true);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!mIsListened) {

            // TODO tip 2：此处演示通过 UnPeekLiveData 来发送 生命周期安全的、事件源可追溯的 通知。

            // 如果这么说还不理解的话，详见 https://xiaozhuanlan.com/topic/0168753249
            // --------
            // 与此同时，此处传达的另一个思想是 最少知道原则，
            // fragment 内部的事情在 fragment 内部消化，不要试图在 Activity 中调用和操纵 Fragment 内部的东西。
            // 因为 fragment 端的处理后续可能会改变，并且可受用于更多的 Activity，而不单单是本 Activity。

            mIsListened = true;
        }
    }
}
