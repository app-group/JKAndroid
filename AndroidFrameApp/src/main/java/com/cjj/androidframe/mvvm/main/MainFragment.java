/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvvm.main;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.FragmentMainBindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMFragment;
import com.cjj.androidframe.mvvm.SharedViewModel;
import com.jkframework.config.JKDataBindingConfig;
import com.jkframework.control.JKProgressDialog;

/**
 * Create by KunMinX at 19/10/29
 */
public class MainFragment extends AFMVVMFragment<FragmentMainBindingImpl> {

    private MainViewModel mMainViewModel;
    private SharedViewModel mSharedViewModel;

    @Override
    protected void initViewModel() {
        mMainViewModel = getFragmentViewModel(MainViewModel.class);
        mSharedViewModel = getActivityViewModel(SharedViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {

        //TODO tip: DataBinding 严格模式：
        // 将 DataBinding 实例限制于 base 页面中，默认不向子类暴露，
        // 通过这样的方式，来彻底解决 视图调用的一致性问题，
        // 如此，视图刷新的安全性将和基于函数式编程的 Jetpack Compose 持平。
        // 而 DataBindingConfig 就是在这样的背景下，用于为 base 页面中的 DataBinding 提供绑定项。

        // 如果这样说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350 和 https://xiaozhuanlan.com/topic/2356748910
        return new JKDataBindingConfig(R.layout.fragment_main, BR.vm, mMainViewModel)
                .addBindingParam(BR.click, new ClickProxy());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //        new RxPermissions(this)
//                .request(Manifest.permission.READ_PHONE_STATE)
//                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
//                .subscribe(granted -> {
//                    if (granted) { // Always true pre-M
//                        mMainViewModel.permissions.setValue(true);
//                    } else {
//                        showLongToast("请开启读取手机状态权限");
//                        ActivityUtils.finishAllActivities();
//                    }
//                });
//
//        Observable.just("").delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .to(autoDisposable(AndroidLifecycleScopeProvider.from(this, Lifecycle.Event.ON_DESTROY)))
//                .subscribe(item -> {
//                    mMainViewModel.wait.setValue(true);
//                });
//
//
//        mMainViewModel.permissions.observe(getViewLifecycleOwner(), s -> {
//            loading();
//        });
//
//        mMainViewModel.wait.observe(getViewLifecycleOwner(), s -> {
//            loading();
//        });
    }

    private void loading() {
//        Boolean bPermissions = mMainViewModel.permissions.getValue();
//        Boolean bWait = mMainViewModel.wait.getValue();
//        if (bPermissions != null && bWait != null) {
//            if (bPermissions && bWait) {
//                nav().navigate(R.id.action_mainFragment_to_loginFragment);
//            }
//        }
    }


    // TODO tip 2：此处通过 DataBinding 来规避 在 setOnClickListener 时存在的 视图调用的一致性问题，

    // 也即，有绑定就有绑定，没绑定也没什么大不了的，总之 不会因一致性问题造成 视图调用的空指针。
    // 如果这么说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350

    public class ClickProxy {

        public void gotoLogin() {
            if (nav().getCurrentDestination().getId() == R.id.fragment_main)
            {
                nav().navigate(R.id.action_main_to_login);
            }
        }

        public void gotoList()
        {
            if (nav().getCurrentDestination().getId() == R.id.fragment_main)
            {
                nav().navigate(R.id.action_main_to_list);
            }
        }

        public void gotoPage()
        {
            if (nav().getCurrentDestination().getId() == R.id.fragment_main)
            {
                nav().navigate(R.id.action_main_to_page);
            }
        }

        public void gotoSelect()
        {
            if (nav().getCurrentDestination().getId() == R.id.fragment_main)
            {
                nav().navigate(R.id.action_main_to_select);
            }
        }

        public void gotoTabbar()
        {
            if (nav().getCurrentDestination().getId() == R.id.fragment_main)
            {
                nav().navigate(R.id.action_main_to_tab);
            }
        }

        public void gotoAdd()
        {
            if (nav().getCurrentDestination().getId() == R.id.fragment_main)
            {
                nav().navigate(R.id.action_main_to_add);
            }
        }
    }

}
