/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvvm.page;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.adapter.PageAdapter;
import com.cjj.androidframe.data.bean.AFPageBean;
import com.cjj.androidframe.databinding.FragmentPageBindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMFragment;
import com.cjj.androidframe.mvvm.SharedViewModel;
import com.jkframework.config.JKDataBindingConfig;
import com.youth.banner.indicator.CircleIndicator;

import java.util.ArrayList;

/**
 * Create by KunMinX at 19/10/29
 */
public class PageFragment extends AFMVVMFragment<FragmentPageBindingImpl> {

    private PageViewModel mPageViewModel;
    private SharedViewModel mSharedViewModel;

    @Override
    protected void initViewModel() {
        mPageViewModel = getFragmentViewModel(PageViewModel.class);
        mSharedViewModel = getActivityViewModel(SharedViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {

        //TODO tip: DataBinding 严格模式：
        // 将 DataBinding 实例限制于 base 页面中，默认不向子类暴露，
        // 通过这样的方式，来彻底解决 视图调用的一致性问题，
        // 如此，视图刷新的安全性将和基于函数式编程的 Jetpack Compose 持平。
        // 而 DataBindingConfig 就是在这样的背景下，用于为 base 页面中的 DataBinding 提供绑定项。

        // 如果这样说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350 和 https://xiaozhuanlan.com/topic/2356748910
        return new JKDataBindingConfig(R.layout.fragment_page, BR.vm, mPageViewModel)
                .addBindingParam(BR.click, new ClickProxy());
//                .addBindingParam(BR.adapter, new PageAdapter(getContext()));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ArrayList<AFPageBean> a_tList = new ArrayList<>();
        a_tList.add(new AFPageBean(1, R.drawable.hairmanager_weizhushoubanner1_all_0));
        a_tList.add(new AFPageBean(2, R.drawable.hairmanager_weizhushoubanner2_all_0));
        a_tList.add(new AFPageBean(3, R.drawable.hairmanager_weizhushoubanner3_all_0));
        mPageViewModel.list.setValue(a_tList);

        getBinding().jkvpBanner.addBannerLifecycleObserver(this).setAdapter(new PageAdapter(a_tList)).setIndicator(new CircleIndicator(getContext()));

//        getBinding().jkvpBanner.SetMode(true);
//        getBinding().jkvpBanner.StartAutoScroll(5000);
//        getBinding().jkvpBanner.initIndicator();
//        getBinding().jkvpBanner.getIndicator()
//                .setOrientation(JKViewPager.Orientation.HORIZONTAL)
//                .setFocusColor(Color.GREEN)
//                .setNormalColor(Color.WHITE)
//                .setMargin(0, 0, 0, JKConvert.DipToPx(5))
//                .setIndicatorPadding(JKConvert.DipToPx(5))
//                .setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)
//                .setRadius(JKConvert.DipToPx(3)).build();
    }

    // TODO tip 2：此处通过 DataBinding 来规避 在 setOnClickListener 时存在的 视图调用的一致性问题，

    // 也即，有绑定就有绑定，没绑定也没什么大不了的，总之 不会因一致性问题造成 视图调用的空指针。
    // 如果这么说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350

    public class ClickProxy {

        public void MoveTo2() {
            getBinding().jkvpBanner.setCurrentItem(2);
        }

        public void UpdateViewPager()
        {
            getBinding().jkvpBanner.getAdapter().notifyDataSetChanged();
        }
    }

}
