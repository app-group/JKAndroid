package com.cjj.androidframe.mvvm.page;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cjj.androidframe.data.bean.AFPageBean;

import java.util.List;

public class PageViewModel extends ViewModel {

    public final MutableLiveData<List<AFPageBean>> list = new MutableLiveData<>();

}