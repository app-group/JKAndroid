/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.mvvm.select;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.StringUtils;
import com.bumptech.glide.Glide;
import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.FragmentSelectBindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMFragment;
import com.cjj.androidframe.mvvm.SharedViewModel;
import com.jkframework.algorithm.JKConvert;
import com.jkframework.algorithm.JKFile;
import com.jkframework.config.JKDataBindingConfig;
import com.jkframework.config.JKPictureSelector;
import com.jkframework.control.JKToast;
import com.luck.picture.lib.basic.PictureSelector;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.config.SelectMimeType;
import com.luck.picture.lib.engine.CropEngine;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.interfaces.OnExternalPreviewEventListener;
import com.luck.picture.lib.interfaces.OnResultCallbackListener;
import com.luck.picture.lib.utils.DateUtils;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropImageEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by KunMinX at 19/10/29
 */
public class SelectFragment extends AFMVVMFragment<FragmentSelectBindingImpl> {

    private SelectViewModel mSelectViewModel;
    private SharedViewModel mSharedViewModel;

    @Override
    protected void initViewModel() {
        mSelectViewModel = getFragmentViewModel(SelectViewModel.class);
        mSharedViewModel = getActivityViewModel(SharedViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {

        //TODO tip: DataBinding 严格模式：
        // 将 DataBinding 实例限制于 base 页面中，默认不向子类暴露，
        // 通过这样的方式，来彻底解决 视图调用的一致性问题，
        // 如此，视图刷新的安全性将和基于函数式编程的 Jetpack Compose 持平。
        // 而 DataBindingConfig 就是在这样的背景下，用于为 base 页面中的 DataBinding 提供绑定项。

        // 如果这样说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350 和 https://xiaozhuanlan.com/topic/2356748910
        return new JKDataBindingConfig(R.layout.fragment_select, BR.vm, mSelectViewModel)
                .addBindingParam(BR.click, new ClickProxy());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    // TODO tip 2：此处通过 DataBinding 来规避 在 setOnClickListener 时存在的 视图调用的一致性问题，

    // 也即，有绑定就有绑定，没绑定也没什么大不了的，总之 不会因一致性问题造成 视图调用的空指针。
    // 如果这么说还不理解的话，详见 https://xiaozhuanlan.com/topic/9816742350

    public class ClickProxy {
        public void PreviewImage() {
            if (mSelectViewModel.imageList != null)
            {
                ArrayList<LocalMedia> list = new ArrayList<>();
                PictureSelector.create(SelectFragment.this).openPreview()
                        .setImageEngine(JKPictureSelector.createGlideEngine())
                        .setExternalPreviewEventListener(new OnExternalPreviewEventListener() {
                            @Override
                            public void onPreviewDelete(int position) {

                            }

                            @Override
                            public boolean onLongPressDownload(Context context, LocalMedia media) {
                                return false;
                            }
                        }).startActivityPreview(0, true, mSelectViewModel.imageList);
            }
            else
                JKToast.ShowLongToast("请先加载图片");
        }

        public void LoadAlbum() {
            PictureSelector.create(SelectFragment.this)
                    .openGallery(SelectMimeType.ofImage())
                    .setImageEngine(JKPictureSelector.createGlideEngine())
                    .setCropEngine(new CropEngine() {
                        @Override
                        public void onStartCrop(Fragment fragment, LocalMedia currentLocalMedia, ArrayList<LocalMedia> dataSource, int requestCode) {
                            String currentCropPath = currentLocalMedia.getAvailablePath();
                            Uri inputUri;
                            if (PictureMimeType.isContent(currentCropPath) || PictureMimeType.isHasHttp(currentCropPath)) {
                                inputUri = Uri.parse(currentCropPath);
                            } else {
                                inputUri = Uri.fromFile(new File(currentCropPath));
                            }
                            String fileName = DateUtils.getCreateFileName("CROP_") + ".jpg";
                            Uri destinationUri = Uri.fromFile(new File(JKFile.GetPublicPath(), fileName));
                            ArrayList<String> dataCropSource = new ArrayList<>();
                            for (int i = 0; i < dataSource.size(); i++) {
                                LocalMedia media = dataSource.get(i);
                                dataCropSource.add(media.getAvailablePath());
                            }
                            UCrop uCrop = UCrop.of(inputUri, destinationUri, dataCropSource);
                            uCrop.setImageEngine(new UCropImageEngine() {
                                @Override
                                public void loadImage(Context context, String url, ImageView imageView) {
                                    Glide.with(context).load(url).into(imageView);
                                }

                                @Override
                                public void loadImage(Context context, Uri url, int maxWidth, int maxHeight, OnCallbackListener<Bitmap> call) {
                                }
                            });
                            uCrop.start(fragment.getActivity(), fragment, requestCode);
                        }
                    })
                    .forResult(new OnResultCallbackListener<LocalMedia>() {
                        @Override
                        public void onResult(ArrayList<LocalMedia> result) {
                            if (result.size() > 0) {
                                mSelectViewModel.imageList = result;
                                LocalMedia lmData = (LocalMedia) result.get(0);
                                if (lmData.isCut()) {
                                    mSelectViewModel.picturePath.setValue(lmData.getCutPath());
                                } else
                                    mSelectViewModel.picturePath.setValue(lmData.getPath());
                            }
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }

        public void Shot() {
            PictureSelector.create(SelectFragment.this)
                    .openCamera(SelectMimeType.ofImage())
//                    .setCropEngine(new CropEngine() {
//                        @Override
//                        public void onStartCrop(Fragment fragment, LocalMedia currentLocalMedia, ArrayList<LocalMedia> dataSource, int requestCode) {
//
//                        }
//                    })
                    .forResultActivity(new OnResultCallbackListener<LocalMedia>() {
                        @Override
                        public void onResult(ArrayList<LocalMedia> result) {
                            if (result.size() > 0) {
                                mSelectViewModel.imageList = result;
                                LocalMedia lmData = (LocalMedia) result.get(0);
                                if (lmData.isCut()) {
                                    mSelectViewModel.picturePath.setValue(lmData.getCutPath());
                                } else
                                    mSelectViewModel.picturePath.setValue(lmData.getPath());
                            }
                        }

                        @Override
                        public void onCancel() {

                        }
                    });
        }

        public void ChoiceFile() {
            JKFile.ChoiceFilePath(pathList -> {
                if (pathList.size() > 0) {
                    mSelectViewModel.filePath.setValue(JKConvert.UriToPath(pathList.get(0)));
                }
            });
        }
    }

}
