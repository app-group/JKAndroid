package com.cjj.androidframe.mvvm.select;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cjj.androidframe.data.bean.AFPageBean;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

public class SelectViewModel extends ViewModel {

    public final MutableLiveData<String> picturePath = new MutableLiveData<>();
    public final MutableLiveData<String> filePath = new MutableLiveData<>();
    public ArrayList<LocalMedia> imageList;

    {
    }
}