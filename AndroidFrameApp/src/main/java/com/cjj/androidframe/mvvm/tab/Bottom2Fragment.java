package com.cjj.androidframe.mvvm.tab;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.cjj.androidframe.BR;
import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.FragmentBottom1BindingImpl;
import com.cjj.androidframe.databinding.FragmentBottom2BindingImpl;
import com.cjj.androidframe.mvvm.AFMVVMFragment;
import com.cjj.androidframe.mvvm.SharedViewModel;
import com.jkframework.config.JKDataBindingConfig;

public class Bottom2Fragment extends AFMVVMFragment<FragmentBottom2BindingImpl> {

    private BottomModel mBottomModel;
    private SharedViewModel mSharedViewModel;

    @Override
    protected void initViewModel() {
        mBottomModel = getFragmentViewModel(BottomModel.class);
        mSharedViewModel = getActivityViewModel(SharedViewModel.class);
    }

    @Override
    protected JKDataBindingConfig getDataBindingConfig() {
        return new JKDataBindingConfig(R.layout.fragment_bottom2, BR.vm, mBottomModel);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
