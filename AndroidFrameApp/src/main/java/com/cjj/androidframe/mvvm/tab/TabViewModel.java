package com.cjj.androidframe.mvvm.tab;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TabViewModel extends ViewModel {

    public final MutableLiveData<String> picturePath = new MutableLiveData<>();
    public final MutableLiveData<String> filePath = new MutableLiveData<>();

    {
    }
}