package com.cjj.androidframe.net;

import com.cjj.androidframe.data.request.AFLoginRequest;
import com.cjj.androidframe.data.response.AFCraneResponse;
import com.cjj.androidframe.data.response.AFHairListResponse;
import com.cjj.androidframe.data.response.AFLoginResponse;
import com.cjj.androidframe.data.response.AFPlayingListResponse;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by 2016/7/5.
 */
interface AFNetInterface {

    @POST("CheckLogin")
    Single<AFLoginResponse> RequestLogin(@Body AFLoginRequest request);

    @FormUrlEncoded
    @POST("api/api.php")
    Single<AFPlayingListResponse> RequestPlaying(@Field("version") String tVersion, @Field("vars") String tJson);

    @FormUrlEncoded
    @POST("api/api.php")
    Single<AFHairListResponse> RequestHairList(@Field("version") String tVersion, @Field("vars") String tJson);

    @GET("GetAllCraneInfoList")
    Single<AFCraneResponse> RequestAllCraneInfoList();
}
