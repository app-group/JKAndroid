package com.cjj.androidframe.net;

import com.cjj.androidframe.data.request.AFLoginRequest;
import com.cjj.androidframe.data.response.AFCraneResponse;
import com.cjj.androidframe.data.response.AFHairListResponse;
import com.cjj.androidframe.data.response.AFLoginResponse;
import com.cjj.androidframe.data.response.AFPlayingListResponse;
import com.jkframework.algorithm.JKEncryption;
import com.jkframework.net.JKHttpRetrofit;
import com.jkframework.serialization.JKJson;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Retrofit;

/**
 * Created by 2016/7/5.
 */
public class AFNetSend {

    public static Single<AFLoginResponse> Login(String tAccount, String tPassword)
    {
        AFLoginRequest request = new AFLoginRequest();
        request.setAccount_name(tAccount);
        request.setAccount_password(JKEncryption.MD5_32(tPassword).toLowerCase());

        Retrofit retrofit = JKHttpRetrofit.GetRetrofitBuilder(null)
                .baseUrl("http://api.chuanfeng.com.cn:9090/CMSMobile/")
                .build();

        AFNetInterface service = retrofit.create(AFNetInterface.class);
        return service.RequestLogin(request);
    }

    public static Single<AFPlayingListResponse> RequestPlaying(int nPage)
    {
        JKJson jkjJson = new JKJson();
        jkjJson.CreateNode("action", "user_plaything_get");
        jkjJson.CreateNode("page", nPage);

        Retrofit retrofit = JKHttpRetrofit.GetRetrofitBuilder(null)
                .baseUrl("http://alli.ishowtu.com/")
                .build();
        AFNetInterface service = retrofit.create(AFNetInterface.class);

        return service.RequestPlaying("v2", jkjJson.GetString());
//        return  JKHttpRetrofit.GetRetrofitCache().using(AFNetCache.class)
//                .RequestPlaying(service.RequestPlaying("v2", jkjJson.GetString()).toObservable(),new DynamicKey(jkjJson.GetString()),new EvictProvider(true)).singleOrError();
    }

    public static Single<AFHairListResponse> RequestHairList(int nPage)
    {
        JKJson jkjJson = new JKJson();
        jkjJson.CreateNode("action", "zhuanye_list_get");
        jkjJson.CreateNode("page", nPage);
        jkjJson.CreateNode("keyword", "");
        jkjJson.CreateNode("search", "");

        Retrofit retrofit = JKHttpRetrofit.GetRetrofitBuilder(null)
                .baseUrl("http://alli.ishowtu.com/")
                .build();
        AFNetInterface service = retrofit.create(AFNetInterface.class);

        return service.RequestHairList("v2", jkjJson.GetString());
//        return  JKHttpRetrofit.GetRetrofitCache().using(AFNetCache.class)
//                .RequestHairList(service.RequestHairList("v2", jkjJson.GetString()).toObservable(),new DynamicKey(jkjJson.GetString()),new EvictProvider(true)).singleOrError();
    }

    public static Single<AFCraneResponse> RequestCraneList()
    {
        Retrofit retrofit = JKHttpRetrofit.GetRetrofitBuilder(null).baseUrl("http://192.168.2.186/CMSMobile/").build();
        AFNetInterface service = retrofit.create(AFNetInterface.class);

        return service.RequestAllCraneInfoList();
    }
}
