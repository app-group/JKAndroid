/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cjj.androidframe.net.request;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.cjj.androidframe.config.AFConfig;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.jkframework.net.JKBaseRequest;
import com.jkframework.net.JKNetState;

import java.util.ArrayList;
import java.util.List;

/**
 * 信息列表 Request
 * <p>
 * TODO tip：Request 通常按业务划分
 * 一个项目中通常存在多个 Request
 * <p>
 * request 的职责仅限于 数据请求，不建议在此处理 UI 逻辑，
 * UI 逻辑只适合在 Activity/Fragment 等视图控制器中完成，是 “数据驱动” 的一部分，
 * 将来升级到 Jetpack Compose 更是如此。
 * <p>
 * 如果这样说还不理解的话，详见 https://xiaozhuanlan.com/topic/6257931840
 * <p>
 * Create by KunMinX at 20/04/26
 */
public class WSMaintenanceRequest extends JKBaseRequest {

    private MutableLiveData<List<AFPlayingBean>> listData;

    //TODO tip 向 ui 层提供的 request LiveData，使用抽象的 LiveData 而不是 MutableLiveData
    // 如此是为了来自数据层的数据，在 ui 层中只读，以避免团队新手不可预期的误用

    public LiveData<List<AFPlayingBean>> getListData() {
        if (listData == null) {
            listData = new MutableLiveData<>();
        }
        return listData;
    }

    public void requestList() {
        if (AFConfig.DEBUG_DATA) {
            JKNetState testNetState = new JKNetState();
            netStateEvent.postValue(testNetState);
            /*构造列表*/
            List<AFPlayingBean> list = new ArrayList<>();
            list.add(new AFPlayingBean(1,"标题1","3","4","5","http://pic5.photophoto.cn/20071217/0008020241208713_b.jpg"));
            list.add(new AFPlayingBean(11,"标题2","33","44","55","https://img.pconline.com.cn/images/upload/upc/tx/itbbs/1312/01/c3/29176669_1385857341852_mthumb.jpg"));
            listData.postValue(list);
        } else {
//            DataRepository.getInstance().requestMaintenance(new DataResult<>((response, netState) -> {
//                netStateEvent.postValue(netState);
//                if (netState.isSuccess())
//                {
//                    listData.postValue(response.getList());
//                }
//                else {
//                    listData.postValue(new ArrayList<>());
//                }
//            }));
        }
    }
}
