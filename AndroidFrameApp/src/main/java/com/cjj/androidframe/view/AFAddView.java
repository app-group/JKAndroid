package com.cjj.androidframe.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;

import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.AndroidframeAddholderBindingImpl;
import com.jkframework.algorithm.JKConvert;

public class AFAddView extends LinearLayout {

    private final AndroidframeAddholderBindingImpl mBinding;

    public AFAddView(Context context) {
        this(context, null);
    }

    public AFAddView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AFAddView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = inflate(context, R.layout.androidframe_addholder, this);
        view.setTag("layout/androidframe_addholder_0");
        mBinding = DataBindingUtil.bind(view);

        mBinding.cbButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int nCount = JKConvert.toInt(mBinding.jktvCount.getText().toString());
                mBinding.jktvCount.setText(JKConvert.toString(nCount + 1));
            }
        });
    }

    public void SetEdit(String tText) {
        mBinding.jketEdit.setText(tText);
    }

    public void SetCount(int nCount) {
        mBinding.jktvCount.setText(JKConvert.toString(nCount));
    }

    public String GetEdit() {
        return mBinding.jketEdit.getText().toString();
    }

    public int GetCount() {
        return JKConvert.toInt(mBinding.jktvCount.getText().toString());
    }

}