package com.cjj.androidframe.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;

import com.cjj.androidframe.R;
import com.cjj.androidframe.data.bean.AFPlayingBean;
import com.cjj.androidframe.databinding.AndroidframeListholderBindingImpl;


public class AFListView extends LinearLayout {

    private final AndroidframeListholderBindingImpl mBinding;

    public AFListView(Context context) {
        this(context, null);
    }

    public AFListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AFListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(context).inflate(R.layout.androidframe_listholder, this);
        view.setTag("layout/androidframe_listholder_0");
        mBinding = DataBindingUtil.bind(view);

        mBinding.jkivImage.SetLoadingImage(R.drawable.androidframe_defaultimageh_all_0);
        mBinding.jkivImage.SetFailImage(R.drawable.androidframe_defaultimageh_all_0);
    }

    public void Update(AFPlayingBean afpdData) {
        mBinding.jkivImage.SetImageHttp(afpdData.getImg());
        mBinding.jktvContent.setText(afpdData.getIntro());
    }
}