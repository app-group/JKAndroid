package com.cjj.androidframe.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.AndroidframeLoadmoreBindingImpl;
import com.scwang.smart.refresh.layout.api.RefreshFooter;
import com.scwang.smart.refresh.layout.api.RefreshKernel;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.constant.SpinnerStyle;

import org.jetbrains.annotations.NotNull;


@SuppressWarnings("RestrictedApi")
public class AFLoadMoreFooterView extends LinearLayout implements RefreshFooter {

    private final AndroidframeLoadmoreBindingImpl mBinding;
    /**
     * 旋转动画
     */
    private ObjectAnimator raAnimation = new ObjectAnimator();
    /**
     * 加载完成
     */
    private boolean bLoadFinish = false;


    public AFLoadMoreFooterView(Context context) {
        this(context, null);
    }

    public AFLoadMoreFooterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AFLoadMoreFooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.androidframe_loadmore, this);
        view.setTag("layout/androidframe_loadmore_0");
        mBinding = DataBindingUtil.bind(view);
    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;
    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int maxDragHeight) {

    }

    @Override
    public void onReleased(@NotNull RefreshLayout refreshLayout, int height, int extendHeight) {

    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public void onStartAnimator(@NonNull RefreshLayout layout, int height, int extendHeight) {

    }

    @Override
    public int onFinish(@NonNull RefreshLayout layout, boolean success) {
        if (success) {
            mBinding.jkivArrow.setVisibility(View.INVISIBLE);
            mBinding.jkivUpdate.setVisibility(View.VISIBLE);
            mBinding.tvTips.setText("加载完成");
            mBinding.jkivUpdate.clearAnimation();
        } else {
            mBinding.jkivArrow.setVisibility(View.VISIBLE);
            mBinding.jkivUpdate.setVisibility(View.INVISIBLE);
            mBinding.tvTips.setText("加载失败,请重试");
            mBinding.jkivUpdate.clearAnimation();
        }
        return 0;
    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public boolean autoOpen(int duration, float dragRate, boolean animationOnly) {
        return false;
    }

    @Override
    public void onStateChanged(@NotNull RefreshLayout refreshLayout, @NotNull RefreshState oldState, RefreshState newState) {
        switch (newState) {
            case PullUpToLoad:
                if (!bLoadFinish) {
                    mBinding.jkivArrow.setVisibility(View.VISIBLE);
                    mBinding.jkivUpdate.setVisibility(View.INVISIBLE);
                    mBinding.tvTips.setText("上拉加载更多");

                    mBinding.jkivArrow.animate().rotation(0).setDuration(250).start();
                }
                break;
            case Loading:
                mBinding.jkivArrow.setVisibility(View.INVISIBLE);
                mBinding.jkivUpdate.setVisibility(View.VISIBLE);
                mBinding.tvTips.setText("正在努力加载中...");

                raAnimation = ObjectAnimator.ofFloat(mBinding.jkivUpdate, "rotation", 0, 360);
                raAnimation.setDuration(1000);
                raAnimation.setRepeatCount(ValueAnimator.INFINITE);
                raAnimation.setRepeatMode(ValueAnimator.RESTART);
                raAnimation.setInterpolator(new LinearInterpolator());
                raAnimation.start();
                break;
            case ReleaseToLoad:
                if (!bLoadFinish) {
                    mBinding.jkivArrow.setVisibility(View.VISIBLE);
                    mBinding.jkivUpdate.setVisibility(View.INVISIBLE);
                    mBinding.tvTips.setText("松开即可加载");

                    mBinding.jkivArrow.animate().rotation(-180).setDuration(250).start();
                }
                break;
        }
    }

    @Override
    public boolean setNoMoreData(boolean noMoreData) {
        bLoadFinish = noMoreData;
        if (noMoreData) {
            mBinding.jkivArrow.setVisibility(View.INVISIBLE);
            mBinding.jkivUpdate.setVisibility(View.VISIBLE);
            mBinding.tvTips.setText("加载完成");
            mBinding.jkivUpdate.clearAnimation();

            raAnimation = ObjectAnimator.ofFloat(mBinding.jkivUpdate, "rotation", 0, 360);
            raAnimation.setDuration(1000);
            raAnimation.setRepeatCount(ValueAnimator.INFINITE);
            raAnimation.setRepeatMode(ValueAnimator.RESTART);
            raAnimation.setInterpolator(new LinearInterpolator());
            raAnimation.start();
        } else {
            mBinding.jkivArrow.setVisibility(View.VISIBLE);
            mBinding.jkivUpdate.setVisibility(View.INVISIBLE);
            mBinding.tvTips.setText("上拉加载更多");

            raAnimation.cancel();
            mBinding.jkivArrow.animate().rotation(0).setDuration(250).start();
        }
        return false;
    }
}



