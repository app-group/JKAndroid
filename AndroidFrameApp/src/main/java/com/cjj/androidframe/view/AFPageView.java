package com.cjj.androidframe.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;

import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.AndroidframePageholderBindingImpl;


public class AFPageView extends LinearLayout {

    private final AndroidframePageholderBindingImpl mBinding;

    public AFPageView(Context context) {
        this(context, null);
    }

    public AFPageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AFPageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.androidframe_pageholder, this);
        view.setTag("layout/androidframe_pageholder_0");
        mBinding = DataBindingUtil.bind(view);
    }

    public void Update(int nResources) {
        mBinding.jkivImage.setImageResource(nResources);
    }
}