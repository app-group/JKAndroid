package com.cjj.androidframe.view;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.cjj.androidframe.R;
import com.cjj.androidframe.databinding.AndroidframeRefreshBindingImpl;
import com.scwang.smart.refresh.layout.api.RefreshHeader;
import com.scwang.smart.refresh.layout.api.RefreshKernel;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.constant.RefreshState;
import com.scwang.smart.refresh.layout.constant.SpinnerStyle;

import org.jetbrains.annotations.NotNull;


@SuppressWarnings("RestrictedApi")
public class AFRefreshHeaderView extends LinearLayout implements RefreshHeader {

    private final AndroidframeRefreshBindingImpl mBinding;
    /**
     * 旋转动画
     */
    private ObjectAnimator raAnimation = new ObjectAnimator();


    public AFRefreshHeaderView(Context context) {
        this(context, null);
    }

    public AFRefreshHeaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AFRefreshHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.androidframe_refresh, this);
        view.setTag("layout/androidframe_refresh_0");
        mBinding = DataBindingUtil.bind(view);
    }


    @Override
    public void onReleased(@NotNull RefreshLayout refreshLayout, int height, int extendHeight) {

    }

    @NonNull
    @Override
    public View getView() {
        return this;
    }

    @NonNull
    @Override
    public SpinnerStyle getSpinnerStyle() {
        return SpinnerStyle.Translate;
    }

    @Override
    public void setPrimaryColors(int... colors) {

    }

    @Override
    public void onInitialized(@NonNull RefreshKernel kernel, int height, int extendHeight) {

    }

    @Override
    public void onMoving(boolean isDragging, float percent, int offset, int height, int maxDragHeight) {

    }

    @Override
    public void onHorizontalDrag(float percentX, int offsetX, int offsetMax) {

    }

    @Override
    public void onStartAnimator(@NonNull RefreshLayout layout, int height, int extendHeight) {

    }

    @Override
    public int onFinish(@NonNull RefreshLayout layout, boolean success) {
        mBinding.jkivArrow.setVisibility(View.INVISIBLE);
        mBinding.jkivUpdate.setVisibility(View.VISIBLE);
        mBinding.tvTips.setText("刷新完成");
        return 0;
    }

    @Override
    public boolean isSupportHorizontalDrag() {
        return false;
    }

    @Override
    public boolean autoOpen(int duration, float dragRate, boolean animationOnly) {
        return false;
    }

    @Override
    public void onStateChanged(@NotNull RefreshLayout refreshLayout, @NotNull RefreshState oldState, RefreshState newState) {
        switch (newState) {
            case None:
            case PullDownToRefresh:
                mBinding.jkivArrow.setVisibility(View.VISIBLE);
                mBinding.jkivUpdate.setVisibility(View.INVISIBLE);
                mBinding.tvTips.setText("下拉可以刷新");

                mBinding.jkivArrow.animate().rotation(0).setDuration(250).start();
                break;
            case Refreshing:
                mBinding.jkivArrow.setVisibility(View.INVISIBLE);
                mBinding.jkivUpdate.setVisibility(View.VISIBLE);
                mBinding.tvTips.setText("正在努力加载中...");
                mBinding.jkivArrow.clearAnimation();

                raAnimation = ObjectAnimator.ofFloat(mBinding.jkivUpdate, "rotation", 0, 360);
                raAnimation.setDuration(1000);
                raAnimation.setRepeatCount(ValueAnimator.INFINITE);
                raAnimation.setRepeatMode(ValueAnimator.RESTART);
                raAnimation.setInterpolator(new LinearInterpolator());
                raAnimation.start();
                break;
            case ReleaseToRefresh:
                mBinding.jkivArrow.setVisibility(View.VISIBLE);
                mBinding.jkivUpdate.setVisibility(View.INVISIBLE);
                mBinding.tvTips.setText("松开即可刷新");

                mBinding.jkivArrow.animate().rotation(-180).setDuration(250).start();
                break;
        }
    }
}
