package com.jkframework.qqbugly;

import com.jkframework.application.JKApplication;
import com.tencent.bugly.crashreport.CrashReport;

public class QQBugly {
	
	/**腾讯crash单例对象*/
	private static QQBugly qqMain = null;

	/**
	 * 获取腾讯单例对象
	 * @return  腾讯统计对象
	 */
	public static QQBugly GetInstance()
	{
		QQBugly tmp = qqMain;
		if (tmp == null) {
			synchronized (QQBugly.class) {
				tmp = qqMain;
				if (tmp == null) {
					qqMain = tmp = new QQBugly();
				}
			}
		}
		return tmp;
	}

	private QQBugly()
	{
		try {
			CrashReport.initCrashReport(JKApplication.getInstance().getApplicationContext());
		}
		catch (Exception ignored)
		{

		}
	}

}
