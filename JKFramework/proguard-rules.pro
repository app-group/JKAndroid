-keepattributes EnclosingMethod
-keepattributes SourceFile,LineNumberTable
-keepattributes StackMapTable

-dontwarn com.jkframework.**
-keep class com.jkframework.algorithm.JKAnalysis{ *; }
-keep class com.jkframework.algorithm.JKConvert{ *; }
-keep class com.jkframework.algorithm.JKFile{ *; }
-keep class com.jkframework.algorithm.JKPicture { *; }
-keep class com.jkframework.config.JKSystem{ *; }
-keep class com.jkframework.debug.JKDebug{ *; }

#androidannotations
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#pinyin4j
-dontwarn com.hp.hpl.sparta.**
-keep class com.hp.hpl.sparta** {*;}
-dontwarn net.sourceforge.pinyin4j.**
-keep class net.sourceforge.pinyin4j** {*;}

#EventBus
-keepattributes *Annotation*
-keepclassmembers class * {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# And if you use AsyncExecutor:
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

#org.conscrypt
-dontwarn org.conscrypt.**
-keep class org.conscrypt** { *; }
-keep interface org.conscrypt** { *; }

#RxJava
-dontwarn java.util.concurrent.Flow*
-keep class io.reactivex.rxjava3.** { *; } #额外添加

#PersistentCookieJar
-dontwarn com.franmontiel.persistentcookiejar.**
-keep class com.franmontiel.persistentcookiejar.*
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

#joda-time
-dontwarn org.joda.time.**

#AgentWeb
-keep class com.just.agentweb** {
    *;
}
-dontwarn com.just.agentweb.**

#PictureSelector
-keep class com.luck.picture.lib** { *; }
-keep class com.luck.picture.lib.** { *; }
-keep class com.luck.lib.camerax.** { *; }
-dontwarn com.yalantis.ucrop**
-keep class com.yalantis.ucrop** { *; }
-keep interface com.yalantis.ucrop** { *; }

#checker-compat-qual
-dontwarn afu.org.checkerframework.**
-dontwarn org.checkerframework.**

#commons-compress
-dontwarn org.tukaani.xz.**
-dontwarn org.brotli.dec.**
-dontwarn com.github.luben.zstd.**

#BottomNavigationViewEx
-keep public class com.google.android.material.bottomnavigation.BottomNavigationView { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationMenuView { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationPresenter.** { *; }
-keep public class com.google.android.material.bottomnavigation.BottomNavigationItemView { *; }

#rxhttp
-dontwarn com.download.library.DownloadTask$DownloadTaskStatus**

#XPopup
-dontwarn com.lxj.xpopup.widget.**
-keep class com.lxj.xpopup.widget**{*;}

#UltimateBarX
-keep class com.zackratos.ultimatebarx.ultimatebarx.** { *; }
-keep public class * extends androidx.fragment.app.Fragment { *; }

#MPAndroidChart
-dontwarn com.github.mikephil.**
-keep class com.github.mikephil.**{ *; }


