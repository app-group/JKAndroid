package com.jkframework.activity;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.jkframework.config.JKSystem;
import com.jkframework.control.JKProgressDialog;
import com.jkframework.debug.JKDebug;
import com.jkframework.debug.JKException;

import javax.annotation.Nullable;

import me.yokeyword.fragmentation.ExtraTransaction;
import me.yokeyword.fragmentation.ISupportActivity;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.SupportActivityDelegate;
import me.yokeyword.fragmentation.SupportHelper;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public abstract class JKBaseActivity<E extends ViewDataBinding> extends AppCompatActivity implements ISupportActivity
{
	/**锁屏对话框对象*/
	private AlertDialog adDialog = null;
	/**锁屏对话框文字*/
	private String tMessage = "";
	/**Activity是否运行中*/
	private  boolean bRun = false;
	/**Activity是否锁屏中*/
	private boolean bLock = false;
    /**Activity是否允许手动解除锁屏*/
    private boolean bCancel = false;
    /**Activity是否暂停*/
    private boolean bPause = false;

	final SupportActivityDelegate mDelegate = new SupportActivityDelegate(this);

	private E mBinding;

	protected abstract int getLayout();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mDelegate.onCreate(savedInstanceState);
		bRun = true;
		//消息崩溃处理
		if (JKDebug.debug)
			Thread.setDefaultUncaughtExceptionHandler(new JKException(JKDebug.tReflect));

		if (!this.isTaskRoot()) {//判断是否为根Activity
			Intent mainIntent = getIntent();
			String action = mainIntent.getAction();
			//判断是否为启动页 如果App不会因为其他原因调用该页面，可以不写if语句
			if (mainIntent.hasCategory(Intent.CATEGORY_LAUNCHER)
					&& Intent.ACTION_MAIN.equals(action)) {
				finish();
				return;
			}
		}

		E binding = DataBindingUtil.setContentView(this, getLayout());
		binding.setLifecycleOwner(this);
		mBinding = binding;

		adDialog = InitProgressDialog();
	}

	protected E getBinding() {
		return mBinding;
	}

	/**
	 * 锁屏对话框
	 * @param tMessage 锁屏提示信息
	 */
	public void LockScreen(String tMessage)
	{		
		LockScreen(tMessage, false);
	}

    /**
     * 锁屏对话框
     * @param tMessage 锁屏提示信息
     * @param bCancel 是否取消锁屏
     */
    public void LockScreen(String tMessage,boolean bCancel)
    {
        if (isFinishing())
            return;
        this.bCancel = bCancel;
        if (adDialog.isShowing())
        {
            this.tMessage = tMessage;
            adDialog.setMessage(tMessage);
            adDialog.setCancelable(bCancel);
        }
        else
        {
            if (!bLock)
            {
//                LockView((ViewGroup) getWindow().getDecorView());
                bLock = true;
                adDialog = InitProgressDialog();
                this.tMessage = tMessage;
                adDialog.setMessage(tMessage);
                adDialog.setCancelable(bCancel);
                adDialog.show();
            }
        }
    }

	/**
	 * 解除锁屏对话框
	 */
	public void UnlockScreen()
	{
		bLock = false;
		if (adDialog.isShowing())
        {
            adDialog.dismiss();
        }
	}
	
	/**
	 * Activity是否正常运作中
	 * @return true表示正常运作中
	 */
	public boolean IsRun()
	{
		return bRun;
	}
	
	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 */
	public synchronized void ReplaceActivity(Class<?> cls)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
			super.startActivity(itIntent);
			finish();
		}
	}

	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 */
	public synchronized void StartActivity(Class<?> cls)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
			super.startActivity(itIntent);
		}
	}

	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 * @param itIntent Intent参数对象
	 */
	public synchronized void StartActivity(Class<?> cls,Intent itIntent)
	{
		if (bRun)
		{
			bRun = false;
			itIntent.setClass(this,cls);
			super.startActivity(itIntent);
		}
	}

	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 * @param nEnterResource 进入动画
	 * @param nExitResource 退出动画
	 */
	public synchronized void StartActivity(Class<?> cls,int nEnterResource,int nExitResource)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
//			super.startActivity(itIntent);
//	        overridePendingTransition(nEnterResource, nExitResource);
			ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(this,nEnterResource,nExitResource);
			ActivityCompat.startActivity(this, itIntent, options.toBundle());
		}
	}

	/**
	 * 打开Activity并接受返回
	 * @param itIntent Intent参数对象
	 * @param nRequestCode 返回值索引
	 */
	public synchronized void StartActivityForResult(Intent itIntent,int nRequestCode)
	{
		if (bRun)
		{
			bRun = false;
			super.startActivityForResult(itIntent,nRequestCode);
		}
	}

	/**
	 * 打开Activity并接受返回
	 * @param cls 界面class对象
	 * @param nRequestCode 返回值索引
	 */
	public synchronized void StartActivityForResult(Class<?> cls,int nRequestCode)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
			super.startActivityForResult(itIntent,nRequestCode);
		}
	}

	/**
	 * 打开Activity并接受返回
	 * @param cls 界面class对象
	 * @param itIntent Intent参数对象
	 * @param nRequestCode 返回值索引
	 */
	public synchronized void StartActivityForResult(Class<?> cls,Intent itIntent,int nRequestCode)
	{
		if (bRun)
		{
			bRun = false;
			itIntent.setClass(this,cls);
			super.startActivityForResult(itIntent,nRequestCode);
		}
	}

	@Override
	public void finish()
	{
		if (adDialog != null && adDialog.isShowing())
			adDialog.dismiss();
		super.finish();
	}
	
//	@Override
//	protected void attachBaseContext(Context newBase) {
//		super.attachBaseContext(JKSystem.SetDefaultFontSize(newBase));
//	}

	@Override
	protected void onPause() {
		super.onPause();
		if (bLock)
		{
			adDialog.dismiss();
		}
		bPause = true;
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		bRun = true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (bLock && bPause && !adDialog.isShowing())
		{
			adDialog = InitProgressDialog();
			adDialog.setMessage(tMessage);
            adDialog.setCancelable(bCancel);
			adDialog.show();
		}
		bPause = false;
		bRun = true;
	}

	@Override
	protected void onDestroy()
	{
		mDelegate.onDestroy();
		super.onDestroy();
		adDialog = null;
	}

	/**
	 * 程序是否在后台
	 * @return true表示是
	 */
	public boolean IsBackground()
	{
		return bPause;
	}

	/**
	 * 初始化锁屏对话框对象(需要自定义重写此函数)
	 * @return 对话框对象
	 */
	protected AlertDialog InitProgressDialog()
	{
//		if (adDialog != null)
//			adDialog.setOnDismissListener(null);
        AlertDialog adTmp = new JKProgressDialog(this);
        adTmp.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!bPause)
                {
                    bLock = false;
                }
//                UnlockView();
            }
        });
		return adTmp;
	}

	@Override
	public SupportActivityDelegate getSupportDelegate() {
		return mDelegate;
	}

	/**
	 * Perform some extra transactions.
	 * 额外的事务：自定义Tag，添加SharedElement动画，操作非回退栈Fragment
	 */
	@Override
	public ExtraTransaction extraTransaction() {
		return mDelegate.extraTransaction();
	}

	@Override
	protected void onPostCreate(@Nullable Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDelegate.onPostCreate(savedInstanceState);
	}

	/**
	 * Note： return mDelegate.dispatchTouchEvent(ev) || super.dispatchTouchEvent(ev);
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return mDelegate.dispatchTouchEvent(ev) || super.dispatchTouchEvent(ev);
	}

	/**
	 * 不建议复写该方法,请使用 {@link #onBackPressedSupport} 代替
	 */
	@Override
	final public void onBackPressed() {
		mDelegate.onBackPressed();
	}

	/**
	 * 该方法回调时机为,Activity回退栈内Fragment的数量 小于等于1 时,默认finish Activity
	 * 请尽量复写该方法,避免复写onBackPress(),以保证SupportFragment内的onBackPressedSupport()回退事件正常执行
	 */
	@Override
	public void onBackPressedSupport() {
		mDelegate.onBackPressedSupport();
	}

	/**
	 * 获取设置的全局动画 copy
	 *
	 * @return FragmentAnimator
	 */
	@Override
	public FragmentAnimator getFragmentAnimator() {
		return mDelegate.getFragmentAnimator();
	}

	/**
	 * Set all fragments animation.
	 * 设置Fragment内的全局动画
	 */
	@Override
	public void setFragmentAnimator(FragmentAnimator fragmentAnimator) {
		mDelegate.setFragmentAnimator(fragmentAnimator);
	}

	/**
	 * Set all fragments animation.
	 * 构建Fragment转场动画
	 * <p/>
	 * 如果是在Activity内实现,则构建的是Activity内所有Fragment的转场动画,
	 * 如果是在Fragment内实现,则构建的是该Fragment的转场动画,此时优先级 > Activity的onCreateFragmentAnimator()
	 *
	 * @return FragmentAnimator对象
	 */
	@Override
	public FragmentAnimator onCreateFragmentAnimator() {
		return mDelegate.onCreateFragmentAnimator();
	}

	/**
	 * Causes the Runnable r to be added to the action queue.
	 * <p>
	 * The runnable will be run after all the previous action has been run.
	 * <p>
	 * 前面的事务全部执行后 执行该Action
	 */
	@Override
	public void post(Runnable runnable) {
		mDelegate.post(runnable);
	}

	/****************************************以下为可选方法(Optional methods)******************************************************/

	// 选择性拓展其他方法

	public void loadRootFragment(int containerId, @NonNull ISupportFragment toFragment) {
		mDelegate.loadRootFragment(containerId, toFragment);
	}

	public void start(ISupportFragment toFragment) {
		mDelegate.start(toFragment);
	}

	/**
	 * @param launchMode Same as Activity's LaunchMode.
	 */
	public void start(ISupportFragment toFragment, @ISupportFragment.LaunchMode int launchMode) {
		mDelegate.start(toFragment, launchMode);
	}

	public void startWithPopTo(ISupportFragment toFragment, Class<?> targetFragmentClass, boolean includeTargetFragment) {
		mDelegate.startWithPopTo(toFragment, targetFragmentClass, includeTargetFragment);
	}

	public void replaceFragment(ISupportFragment toFragment, boolean addToBackStack) {
		mDelegate.replaceFragment(toFragment, addToBackStack);
	}
	
	public void showHideFragment(ISupportFragment showFragment, ISupportFragment hideFragment) {
		mDelegate.showHideFragment(showFragment, hideFragment);
	}

	/**
	 * Pop the fragment.
	 */
	public void pop() {
		mDelegate.pop();
	}

	/**
	 * Pop the last fragment transition from the manager's fragment
	 * back stack.
	 */
	public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment) {
		mDelegate.popTo(targetFragmentClass, includeTargetFragment);
	}

	/**
	 * If you want to begin another FragmentTransaction immediately after popTo(), use this method.
	 * 如果你想在出栈后, 立刻进行FragmentTransaction操作，请使用该方法
	 */
	public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment, Runnable afterPopTransactionRunnable) {
		mDelegate.popTo(targetFragmentClass, includeTargetFragment, afterPopTransactionRunnable);
	}

	public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment, Runnable afterPopTransactionRunnable, int popAnim) {
		mDelegate.popTo(targetFragmentClass, includeTargetFragment, afterPopTransactionRunnable, popAnim);
	}

	/**
	 * 得到位于栈顶Fragment
	 */
	public ISupportFragment getTopFragment() {
		return SupportHelper.getTopFragment(getSupportFragmentManager());
	}

	/**
	 * 获取栈内的fragment对象
	 */
	public <T extends ISupportFragment> T findFragment(Class<T> fragmentClass) {
		return SupportHelper.findFragment(getSupportFragmentManager(), fragmentClass);
	}
}