package com.jkframework.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;

import com.cjj.jkframework.R;
import com.cjj.jkframework.databinding.JkframeworkExceptionactivityBindingImpl;
import com.jkframework.config.JKSystem;
import com.jkframework.debug.JKException;

import java.util.Locale;


public class JKExceptionActivity extends JKBaseActivity<JkframeworkExceptionactivityBindingImpl> {

    /**
     * 消息反馈
     */
    private String tReportText;
    /**
     * 反馈类名
     */
    private String tReflectClass;

    @Override
    protected int getLayout() {
        return R.layout.jkframework_exceptionactivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JKSystem.SetStatusBarMode(true);

        //        setContentView(R.layout.jkframework_exceptionactivity);
        tReportText = getIntent().getStringExtra("ReportText");
        tReflectClass = getIntent().getStringExtra("Class");

        InitListener();
        InitData();
    }

    @Override
    public void onBackPressedSupport() {
        JKException.ExitProgram();
    }

    /**
     * 初始化监听事件
     */
    protected void InitListener() {
        getBinding().etExceptionEdit.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    JKException.DoingReflect(JKExceptionActivity.this, tReflectClass, getBinding().etExceptionEdit.getText().toString().toLowerCase(Locale.getDefault()));
                    return true;
                }
                return false;
            }
        });
        getBinding().cbExceptionDebug.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                JKException.DoingReflect(JKExceptionActivity.this, tReflectClass, getBinding().etExceptionEdit.getText().toString().toLowerCase(Locale.getDefault()));
            }
        });
        getBinding().cbExceptionExit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                JKException.ExitProgram();
            }
        });
    }

    /**
     * 初始化数据
     */
    private void InitData() {
        getBinding().jktbToolBar.setSubtitle("程序崩溃");
        getBinding().jktbToolBar.Attach(this);
        getBinding().jktbToolBar.setLogo(JKSystem.GetApplicationIcon());
        getBinding().jktvExceptionInfo.setText(tReportText);
    }
}