package com.jkframework.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import com.jkframework.algorithm.JKConvert;
import com.jkframework.algorithm.JKFile;
import com.jkframework.config.JKPreferences;

import java.util.ArrayList;


public class JKFileActivity extends AppCompatActivity {

    /**
     * 文件选择
     */
    private final int REQUEST_CHOICE_FILE = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent itIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        itIntent.addCategory(Intent.CATEGORY_OPENABLE);
        itIntent.setType("*/*");
        fileChooserLauncher.launch(itIntent);
    }

    private final ActivityResultLauncher<Intent> fileChooserLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                    Uri uri = result.getData().getData();
                    if (uri != null) {
                        ArrayList<Uri> uriList = new ArrayList<>();
                        uriList.add(uri);
                        if (JKFile.mChoiceListener != null) {
                            JKFile.mChoiceListener.FinishChoice(uriList);
                        }
                    }
                } else if (result.getResultCode() == RESULT_CANCELED) {
                    if (JKFile.mChoiceListener != null) {
                        JKFile.mChoiceListener.FinishChoice(new ArrayList<>());
                    }
                }
                finish();
            });

}