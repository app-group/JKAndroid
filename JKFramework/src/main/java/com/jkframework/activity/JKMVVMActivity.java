package com.jkframework.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.jkframework.application.JKApplication;
import com.jkframework.config.JKDataBindingConfig;
import com.jkframework.debug.JKDebug;
import com.jkframework.debug.JKException;

public abstract class JKMVVMActivity<E extends ViewDataBinding> extends AppCompatActivity
{
	/**Activity是否运行中*/
	private  boolean bRun = false;
    /**Activity是否暂停*/
    private boolean bPause = false;

	private ViewModelProvider mActivityProvider;
	private ViewModelProvider mApplicationProvider;
	private E mBinding;

	protected abstract void initViewModel();
	protected abstract JKDataBindingConfig getDataBindingConfig();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		bRun = true;
		//消息崩溃处理
		if (JKDebug.debug)
			Thread.setDefaultUncaughtExceptionHandler(new JKException(JKDebug.tReflect));

		if (!this.isTaskRoot()) {//判断是否为根Activity
			Intent mainIntent = getIntent();
			String action = mainIntent.getAction();
			//判断是否为启动页 如果App不会因为其他原因调用该页面，可以不写if语句
			if (mainIntent.hasCategory(Intent.CATEGORY_LAUNCHER)
					&& Intent.ACTION_MAIN.equals(action)) {
				finish();
				return;
			}
		}

		initViewModel();
		JKDataBindingConfig dataBindingConfig = getDataBindingConfig();

		E binding = DataBindingUtil.setContentView(this, dataBindingConfig.getLayout());
		binding.setLifecycleOwner(this);
		binding.setVariable(dataBindingConfig.getVmVariableId(), dataBindingConfig.getStateViewModel());
		SparseArray bindingParams = dataBindingConfig.getBindingParams();
		for (int i = 0, length = bindingParams.size(); i < length; i++) {
			binding.setVariable(bindingParams.keyAt(i), bindingParams.valueAt(i));
		}
		mBinding = binding;
	}

	protected E getBinding() {
		return mBinding;
	}

	protected <T extends ViewModel> T getActivityViewModel(@NonNull Class<T> modelClass) {
		if (mActivityProvider == null) {
			mActivityProvider = new ViewModelProvider(this);
		}
		return mActivityProvider.get(modelClass);
	}

	protected <T extends ViewModel> T getApplicationScopeViewModel(@NonNull Class<T> modelClass) {
		if (mApplicationProvider == null) {
			mApplicationProvider = new ViewModelProvider((JKApplication) this.getApplicationContext());
		}
		return mApplicationProvider.get(modelClass);
	}

	protected void toggleSoftInput() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/**
	 * Activity是否正常运作中
	 * @return true表示正常运作中
	 */
	public boolean IsRun()
	{
		return bRun;
	}
	
	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 */
	public synchronized void ReplaceActivity(Class<?> cls)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
			super.startActivity(itIntent);
			finish();
		}
	}

	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 */
	public synchronized void StartActivity(Class<?> cls)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
			super.startActivity(itIntent);
		}
	}

	/**
	 * 打开Activity
	 * @param cls 界面class对象
	 * @param itIntent Intent参数对象
	 */
	public synchronized void StartActivity(Class<?> cls,Intent itIntent)
	{
		if (bRun)
		{
			bRun = false;
			itIntent.setClass(this,cls);
			super.startActivity(itIntent);
		}
	}

	/**
	 * 打开Activity并接受返回
	 * @param itIntent Intent参数对象
	 * @param nRequestCode 返回值索引
	 */
	public synchronized void StartActivityForResult(Intent itIntent,int nRequestCode)
	{
		if (bRun)
		{
			bRun = false;
			super.startActivityForResult(itIntent,nRequestCode);
		}
	}

	/**
	 * 打开Activity并接受返回
	 * @param cls 界面class对象
	 * @param nRequestCode 返回值索引
	 */
	public synchronized void StartActivityForResult(Class<?> cls,int nRequestCode)
	{
		if (bRun)
		{
			bRun = false;
			Intent itIntent = new Intent(this,cls);
			super.startActivityForResult(itIntent,nRequestCode);
		}
	}

	/**
	 * 打开Activity并接受返回
	 * @param cls 界面class对象
	 * @param itIntent Intent参数对象
	 * @param nRequestCode 返回值索引
	 */
	public synchronized void StartActivityForResult(Class<?> cls,Intent itIntent,int nRequestCode)
	{
		if (bRun)
		{
			bRun = false;
			itIntent.setClass(this,cls);
			super.startActivityForResult(itIntent,nRequestCode);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		bPause = true;
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		bRun = true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		bPause = false;
		bRun = true;
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	/**
	 * 程序是否在后台
	 * @return true表示是
	 */
	public boolean IsBackground()
	{
		return bPause;
	}
}