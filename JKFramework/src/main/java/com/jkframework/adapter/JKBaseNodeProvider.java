/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jkframework.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.provider.BaseNodeProvider;
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;


public abstract class JKBaseNodeProvider<M extends BaseNode, B extends ViewDataBinding> extends BaseNodeProvider {

    @Override
    @NonNull
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        B binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), getLayoutId(), parent, false);
        return new BaseDataBindingHolder<B>(binding.getRoot());
    }

    public abstract void convert(@NonNull B binding,M item);

    @Override
    @SuppressWarnings("unchecked")
    public void convert(@NotNull BaseViewHolder help, @NotNull BaseNode data) {
        BaseDataBindingHolder<B> holder = (BaseDataBindingHolder<B>) help;
        B binding = holder.getDataBinding();
        if (binding != null) {
            this.convert(binding, (M) data);
            binding.executePendingBindings();
        }
    }

}
