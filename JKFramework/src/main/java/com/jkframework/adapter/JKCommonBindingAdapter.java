/*
 * Copyright 2018-present KunMinX
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jkframework.adapter;

import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.ViewDataBinding;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.viewpager2.widget.ViewPager2;

import com.blankj.utilcode.util.ClickUtils;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseNodeAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.node.BaseNode;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.tabs.TabLayout;
import com.jkframework.control.JKRecyclerView;
import com.jkframework.control.JKViewSwitcher;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.List;

/**
 * Create by KunMinX at 19/9/18
 */
public class JKCommonBindingAdapter {

    @BindingAdapter(value = {"imageUrl", "placeHolder"}, requireAll = false)
    public static void loadUrl(ImageView view, String url, Drawable placeHolder) {
        Glide.with(view.getContext()).load(url).placeholder(placeHolder).into(view);
    }

    @BindingAdapter(value = {"visible"}, requireAll = false)
    public static void visible(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(value = {"showDrawable", "drawableShowed"}, requireAll = false)
    public static void showDrawable(ImageView view, boolean showDrawable, int drawableShowed) {
        view.setImageResource(showDrawable ? drawableShowed : android.R.color.transparent);
    }

    @BindingAdapter(value = {"textColor"}, requireAll = false)
    public static void setTextColor(TextView textView, int textColorRes) {
        textView.setTextColor(textView.getResources().getColor(textColorRes, null));
    }

    @BindingAdapter(value = {"imageRes"}, requireAll = false)
    public static void setImageRes(ImageView imageView, int imageRes) {
        imageView.setImageResource(imageRes);
    }

    @BindingAdapter(value = {"selected"}, requireAll = false)
    public static void selected(View view, boolean select) {
        view.setSelected(select);
    }

    @BindingAdapter(value = {"onClickWithDebouncing"}, requireAll = false)
    public static void onClickWithDebouncing(View view, View.OnClickListener clickListener) {
        ClickUtils.applySingleDebouncing(view, clickListener);
    }

    @BindingAdapter(value = {"adjustWidth"})
    public static void adjustWidth(View view, int adjustWidth) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = adjustWidth;
        view.setLayoutParams(params);
    }

    @BindingAdapter(value = {"adjustHeight"})
    public static void adjustHeight(View view, int adjustHeight) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = adjustHeight;
        view.setLayoutParams(params);
    }

    @BindingAdapter(value = {"toolbarNavigationClick"})
    public static void setNavigationClick(Toolbar toolbar, View.OnClickListener listener) {
        toolbar.setNavigationOnClickListener(listener);
    }

    @BindingAdapter(value = {"toolbarMenuItemClick"})
    public static void setMenuItemClick(Toolbar toolbar, Toolbar.OnMenuItemClickListener listener) {
        toolbar.setOnMenuItemClickListener(listener);
    }

    @BindingAdapter(value = {"keycodeEnter"})
    public static void keycodeEnter(EditText editText, Runnable func) {
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent keyEvent) {
                if (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                    func.run();
                    return true;
                }
                return false;
            }
        });
    }

    /////////////////////////////////RecyclerViewBindingAdapter
    @BindingAdapter(value = {"adapter", "refreshList", "autoScrollToTopWhenInsert", "autoScrollToBottomWhenInsert", "itemClick","itemLongClick"}, requireAll = false)
    public static <T, B extends ViewDataBinding> void bindList(RecyclerView recyclerView, JKBaseDataBindingAdapter<T, B> adapter, List<T> list,
                                    boolean autoScrollToTopWhenInsert, boolean autoScrollToBottomWhenInsert, JKBaseDataBindingAdapter.OnItemClickListener<T> itemClickListener, JKBaseDataBindingAdapter.OnItemLongClickListener<T> itemLongClickListener) {

        if (recyclerView != null && list != null) {
            if (recyclerView.getAdapter() == null) {
                recyclerView.setAdapter(adapter);

                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onItemRangeInserted(int positionStart, int itemCount) {
                        if (autoScrollToTopWhenInsert) {
                            recyclerView.scrollToPosition(0);
                        } else if (autoScrollToBottomWhenInsert) {
                            recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount());
                        }
                    }
                });
            }

            adapter.setOnItemClickListener(itemClickListener);
            adapter.setOnItemLongClickListener(itemLongClickListener);
            adapter.submitList(list);
        }
    }


    @BindingAdapter(value = {"adapter", "refreshList", "autoScrollToTopWhenInsert", "autoScrollToBottomWhenInsert", "itemClick","itemLongClick", "itemChildClick"}, requireAll = false)
    public static <T, B extends BaseViewHolder> void bindList(RecyclerView recyclerView, BaseQuickAdapter<T, B> adapter, List<T> list,
                                boolean autoScrollToTopWhenInsert, boolean autoScrollToBottomWhenInsert, OnItemClickListener itemClickListener, OnItemLongClickListener itemLongClickListener, OnItemChildClickListener itemChildClickListener) {

        if (recyclerView != null && list != null) {
            if (recyclerView.getAdapter() == null) {
                recyclerView.setAdapter(adapter);

                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onItemRangeInserted(int positionStart, int itemCount) {
                        if (autoScrollToTopWhenInsert) {
                            recyclerView.scrollToPosition(0);
                        } else if (autoScrollToBottomWhenInsert) {
                            recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount());
                        }
                    }
                });
            }

            adapter.setOnItemClickListener(itemClickListener);
            adapter.setOnItemLongClickListener(itemLongClickListener);
            adapter.setOnItemChildClickListener(itemChildClickListener);
            adapter.setList(list);
        }
    }

    @BindingAdapter(value = {"adapter", "refreshList", "autoScrollToTopWhenInsert", "autoScrollToBottomWhenInsert", "itemClick","itemLongClick", "itemChildClick"}, requireAll = false)
    public static <T extends BaseNode> void bindList(RecyclerView recyclerView, BaseNodeAdapter adapter, List<T> list,
                                                     boolean autoScrollToTopWhenInsert, boolean autoScrollToBottomWhenInsert, OnItemClickListener itemClickListener, OnItemLongClickListener itemLongClickListener, OnItemChildClickListener itemChildClickListener) {

        if (recyclerView != null && list != null) {
            if (recyclerView.getAdapter() == null) {
                recyclerView.setAdapter(adapter);

                adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onItemRangeInserted(int positionStart, int itemCount) {
                        if (autoScrollToTopWhenInsert) {
                            recyclerView.scrollToPosition(0);
                        } else if (autoScrollToBottomWhenInsert) {
                            recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount());
                        }
                    }
                });
            }

            adapter.setOnItemClickListener(itemClickListener);
            adapter.setOnItemLongClickListener(itemLongClickListener);
            adapter.setOnItemChildClickListener(itemChildClickListener);
            adapter.setList(list);
        }
    }

    @BindingAdapter(value = {"notifyCurrentListChanged"})
    public static void notifyListChanged(RecyclerView recyclerView, boolean notify) {
        if (notify && recyclerView != null && recyclerView.getAdapter() != null) {
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @BindingAdapter(value = {"onRefresh"})
    public static void onRefresh(SmartRefreshLayout refresh, Runnable func) {
        refresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                func.run();
            }
        });
    }

    @BindingAdapter(value = {"autoRefresh"})
    public static void autoRefresh(SmartRefreshLayout refresh, boolean state) {
        if (state) {
            refresh.autoRefresh();
        } else {
            refresh.finishRefresh();
        }
    }

    @BindingAdapter(value = {"onLoadMore"})
    public static void onLoadMore(SmartRefreshLayout refresh, Runnable func) {
        refresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                func.run();
            }
        });
    }

    @BindingAdapter(value = {"autoLoadMore"})
    public static void autoLoadMore(SmartRefreshLayout refresh, boolean state) {
        if (state) {
            refresh.autoLoadMore();
        } else {
            refresh.finishLoadMore();
        }
    }

    @BindingAdapter(value = {"scrollPage"})
    public static void setCurrentIndex(ViewPager2 view, int direct) {
        if (view.getAdapter() != null && view.getAdapter() != null) {
            view.setCurrentItem(view.getCurrentItem() + direct, true);
        }
    }

    @BindingAdapter(value = {"adapter", "submitList"})
    public static void bindList(ViewPager2 viewPager, ListAdapter adapter, List list) {
        if (viewPager != null && list != null) {
            if (viewPager.getAdapter() == null) {
                viewPager.setAdapter(adapter);
            }
            adapter.submitList(list);
        }
    }

    @BindingAdapter(value = {"notifyCurrentListChanged"})
    public static void notifyListChanged(ViewPager2 viewPager, boolean notify) {
        if (notify && viewPager != null && viewPager.getAdapter() != null) {
            viewPager.getAdapter().notifyDataSetChanged();
        }
    }

    @BindingAdapter(value = {"supportsChangeAnimations"})
    public static void supportsChangeAnimations(RecyclerView recyclerView, boolean supports) {
        if (!supports && recyclerView.getItemAnimator() != null) {
            ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
            recyclerView.getItemAnimator().setAddDuration(0);
            recyclerView.getItemAnimator().setChangeDuration(0);
            recyclerView.getItemAnimator().setMoveDuration(0);
            recyclerView.getItemAnimator().setRemoveDuration(0);
        }
    }

    @BindingAdapter(value = {"displayedChild"})
    public static void SetDisplayedChild(JKViewSwitcher view, Integer index) {
        view.SetDisplayedChild(index);
    }

    @InverseBindingAdapter(attribute = "openDrawerLeft", event = "openDrawerLeftChanged")
    public static boolean getDrawerLeft(DrawerLayout view) {
        return view.isDrawerOpen(GravityCompat.START);
    }

    @InverseBindingAdapter(attribute = "openDrawerRight", event = "openDrawerRightChanged")
    public static boolean getDrawerRight(DrawerLayout view) {
        return view.isDrawerOpen(GravityCompat.END);
    }

    @BindingAdapter(value = {"openDrawerLeft", "openDrawerRight"}, requireAll = false)
    public static void openDrawer(DrawerLayout drawerLayout, Boolean stateLeft, Boolean stateRight) {
        try
        {
            if (stateLeft != null) {
                if (stateLeft) {
                    drawerLayout.openDrawer(GravityCompat.START, true);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.START, true);
                }
            }
            if (stateRight != null) {
                if (stateRight) {
                    drawerLayout.openDrawer(GravityCompat.END, true);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.END, true);
                }
            }
        }
        catch (Exception ignored)
        {

        }
    }

    @BindingAdapter(value = {"openDrawerLeftChanged"})
    public static void setDrawerLayoutListener(DrawerLayout view, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener != null) {
            view.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

                }

                @Override
                public void onDrawerOpened(@NonNull View drawerView) {
                    inverseBindingListener.onChange();
                }

                @Override
                public void onDrawerClosed(@NonNull View drawerView) {
                    inverseBindingListener.onChange();
                }

                @Override
                public void onDrawerStateChanged(int newState) {

                }
            });
        }
    }

    @BindingAdapter(value = {"openDrawerRightChanged"})
    public static void setDrawerLayoutListener2(DrawerLayout view, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener != null) {
            view.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

                }

                @Override
                public void onDrawerOpened(@NonNull View drawerView) {
                    inverseBindingListener.onChange();
                }

                @Override
                public void onDrawerClosed(@NonNull View drawerView) {
                    inverseBindingListener.onChange();
                }

                @Override
                public void onDrawerStateChanged(int newState) {

                }
            });
        }
    }

    @BindingAdapter(value = {"dataList"})
    public static void setLineCharData(LineChart chart, List<ILineDataSet> list) {
        if (list != null) {
            chart.setData(new LineData(list));
            chart.invalidate();
        }
    }

    @BindingAdapter(value = {"dataList", "barWidthPercent"}, requireAll = false)
    public static void setBarCharData(BarChart chart, List<IBarDataSet> list, Float widthPercent) {
        if (list != null) {
            var bardata = new BarData(list);
            if (widthPercent != null)
                bardata.setBarWidth(widthPercent);
            chart.setData(bardata);
            chart.invalidate();
        }
    }

    @BindingAdapter(value = {"noDataText"})
    public static void setNoDataText(Chart<?> chart, String noDataText) {
        chart.setNoDataText(noDataText);
    }

    @BindingAdapter(value = {"data"})
    public static void setPieCharData(PieChart chart, PieData dataSet) {
        if (dataSet != null) {
            chart.setData(dataSet);
            chart.invalidate();
        }
    }

    @BindingAdapter(value = {"drawHoleEnabled"})
    public static void drawHoleEnabled(PieChart chart, boolean enabled) {
        chart.setDrawHoleEnabled(enabled);
    }

    @BindingAdapter(value = {"usePercentValues"})
    public static void usePercentValues(PieChart chart, boolean enabled) {
        chart.setUsePercentValues(enabled);
    }

    @BindingAdapter(value = {"drawEntryLabels"})
    public static void drawEntryLabels(PieChart chart, boolean enabled) {
        chart.setDrawEntryLabels(enabled);
    }

    @BindingAdapter(value = {"verticalAlignment"})
    public static void verticalAlignment(Chart<?> chart, Legend.LegendVerticalAlignment alignment) {
        chart.getLegend().setVerticalAlignment(alignment);
    }

    @BindingAdapter(value = {"horizontalAlignment"})
    public static void horizontalAlignment(Chart<?> chart, Legend.LegendHorizontalAlignment alignment) {
        chart.getLegend().setHorizontalAlignment(alignment);
    }

    @BindingAdapter(value = {"orientation"})
    public static void orientation(Chart<?> chart, Legend.LegendOrientation orientation) {
        chart.getLegend().setOrientation(orientation);
    }

    @BindingAdapter(value = {"form"})
    public static void horizontalAlignment(Chart<?> chart, Legend.LegendForm form) {
        chart.getLegend().setForm(form);
    }

    @BindingAdapter(value = {"drawInside"})
    public static void drawInside(Chart<?> chart, boolean enabled) {
        chart.getLegend().setDrawInside(enabled);
    }

    @BindingAdapter(value = {"legendTextColor", "legendTextSize"})
    public static void animate(Chart<?> chart, int textColor, float textSize) {
        chart.getLegend().setTextColor(textColor);
        chart.getLegend().setTextSize(textSize);
    }

    @BindingAdapter(value = {"paddingLeft", "paddingRight","paddingTop","paddingBottom"}, requireAll = false)
    public static void setExtra(PieChart view, Float leftDp, Float rightDp, Float topDp, Float bottomDp) {
        if (topDp != null)
            view.setExtraTopOffset(topDp);
        if (bottomDp != null)
            view.setExtraBottomOffset(bottomDp);
        if (leftDp != null)
            view.setExtraLeftOffset(leftDp);
        if (rightDp != null)
            view.setExtraRightOffset(rightDp);
    }

    @BindingAdapter(value = {"paddingLeft", "paddingRight","paddingTop","paddingBottom"}, requireAll = false)
    public static void setExtra(BarChart view,Float leftDp,Float rightDp,Float topDp,Float bottomDp) {
        if (topDp != null)
            view.setExtraTopOffset(topDp);
        if (bottomDp != null)
            view.setExtraBottomOffset(bottomDp);
        if (leftDp != null)
            view.setExtraLeftOffset(leftDp);
        if (rightDp != null)
            view.setExtraRightOffset(rightDp);
    }

    @BindingAdapter(value = {"paddingLeft", "paddingRight","paddingTop","paddingBottom"}, requireAll = false)
    public static void setExtra(LineChart view,Float leftDp,Float rightDp,Float topDp,Float bottomDp) {
        if (topDp != null)
            view.setExtraTopOffset(topDp);
        if (bottomDp != null)
            view.setExtraBottomOffset(bottomDp);
        if (leftDp != null)
            view.setExtraLeftOffset(leftDp);
        if (rightDp != null)
            view.setExtraRightOffset(rightDp);
    }

    @BindingAdapter(value = {"drawGridBackground"})
    public static void drawGridBackground(BarLineChartBase<?> chart, boolean drawGridBackground) {
        chart.setDrawGridBackground(drawGridBackground);
    }

    @BindingAdapter(value = {"drawBorders"})
    public static void drawBorders(BarLineChartBase<?> chart, boolean drawBorders) {
        chart.setDrawBorders(drawBorders);   //禁止绘制图表边框的线
    }

    @BindingAdapter(value = {"touchEnabled"})
    public static void touchEnabled(BarLineChartBase<?> chart, boolean touchEnabled) {
        chart.setTouchEnabled(touchEnabled);
    }

    @BindingAdapter(value = {"dragEnabled"})
    public static void dragEnabled(BarLineChartBase<?> chart, boolean dragEnabled) {
        chart.setDragEnabled(dragEnabled);
    }

    @BindingAdapter(value = {"scaleEnabled"})
    public static void scaleEnabled(BarLineChartBase<?> chart, boolean scaleEnabled) {
        chart.setScaleEnabled(scaleEnabled);
    }

    @BindingAdapter(value = {"descripEnabled"})
    public static void descripEnabled(Chart<?> chart, boolean enabled) {
        chart.getDescription().setEnabled(enabled);
    }

    @BindingAdapter(value = {"legendEnabled"})
    public static void legendEnabled(Chart<?> chart, boolean enabled) {
        chart.getLegend().setEnabled(enabled);
    }

    @BindingAdapter(value = {"xAxisDrawGridLines"})
    public static void xAxisDrawGridLines(Chart<?> chart, boolean enabled) {
        chart.getXAxis().setDrawGridLines(enabled);
    }

    @BindingAdapter(value = {"yAxisDrawGridLines"})
    public static void yAxisDrawGridLines(BarLineChartBase<?> chart, boolean enabled) {
        chart.getAxisLeft().setDrawGridLines(enabled);
        chart.getAxisRight().setDrawGridLines(enabled);
    }

    @BindingAdapter(value = {"xAxisGranularity"})
    public static void xAxisGranularity(Chart<?> chart, float granularity) {
        chart.getXAxis().setGranularity(granularity);
    }

    @BindingAdapter(value = {"yAxisGranularity"})
    public static void yAxisGranularity(BarLineChartBase<?> chart, float granularity) {
        chart.getAxisLeft().setGranularity(granularity);
        chart.getAxisRight().setGranularity(granularity);
    }

    @BindingAdapter(value = {"xAxisAnimate", "yAxisAnimate"}, requireAll = false)
    public static void animate(Chart<?> chart, Integer xMillis, Integer yMillis) {
        if (xMillis != null)
            chart.animateX(xMillis);
        if (yMillis != null)
            chart.animateY(yMillis);
    }

    @BindingAdapter(value = {"xAxisValueFormatter"})
    public static void xAxisValueFormatter(Chart<?> chart, ValueFormatter f) {
        var xAxis = chart.getXAxis();
        xAxis.setValueFormatter(f);
    }

    @BindingAdapter(value = {"yAxisValueFormatter"})
    public static void yAxisValueFormatter(BarLineChartBase<?> chart, ValueFormatter f) {
        chart.getAxisLeft().setValueFormatter(f);
        chart.getAxisRight().setValueFormatter(f);
    }

    @BindingAdapter(value = {"xAxisPosition"})
    public static void xAxisPosition(Chart<?> chart, XAxis.XAxisPosition position) {
        var xAxis = chart.getXAxis();
        xAxis.setPosition(position);
    }

    @BindingAdapter(value = {"xAxisDrawAxisLine"})
    public static void xAxisDrawAxisLine(Chart<?> chart, boolean drawAxisLine) {
        var xAxis = chart.getXAxis();
        xAxis.setDrawAxisLine(drawAxisLine);
    }

    @BindingAdapter("xAxisLabelRotationAngle")
    public static void xAxisLabelRotationAngle(Chart<?> chart, float angle) {
        var xAxis = chart.getXAxis();
        xAxis.setLabelRotationAngle(angle);
    }

    @BindingAdapter(value = {"xAxisLabelCount", "xAxisLabelForce", "xAxisLabelMinValue", "xAxisLabelMaxValue"}, requireAll = false)
    public static void xAxisLabelCount(Chart<?> chart, Integer count, boolean force, Integer minValue, Integer maxValue) {
        var xAxis = chart.getXAxis();
        if (count != null)
            xAxis.setLabelCount(count, force); //force为false会智能选择count,minValue,maxValue
        if (minValue != null)
            xAxis.setAxisMinimum(minValue);
        if (maxValue != null)
            xAxis.setAxisMaximum(maxValue);
    }

    @BindingAdapter(value = {"yAxisLabelCount", "yAxisLabelForce", "yAxisLabelMinValue", "yAxisLabelMaxValue"}, requireAll = false)
    public static void yAxisLabelCount(BarLineChartBase<?> chart, Integer count, boolean force, Integer minValue, Integer maxValue) {
        var leftAxis = chart.getAxisLeft();
        var rightAxis = chart.getAxisRight();
        if (count != null) {
            leftAxis.setLabelCount(count, force);
            rightAxis.setLabelCount(count, force);
        }
        if (minValue != null) {
            leftAxis.setAxisMinimum(minValue);
            rightAxis.setAxisMinimum(minValue);
        }
        if (maxValue != null) {
            leftAxis.setAxisMaximum(maxValue);
            rightAxis.setAxisMaximum(maxValue);
        }
    }

    @BindingAdapter(value = {"xAxisTextColor"})
    public static void xAxisTextColor(Chart<?> chart, int textColor) {
        var xAxis = chart.getXAxis();
        xAxis.setTextColor(textColor);
    }

    @BindingAdapter(value = {"yAxisTextColor"})
    public static void yAxisTextColor(BarLineChartBase<?> chart, int textColor) {
        var leftAxis = chart.getAxisLeft();
        var rightAxis = chart.getAxisRight();
        leftAxis.setTextColor(textColor);
        rightAxis.setTextColor(textColor);
    }

    @BindingAdapter(value = {"onTabLayoutSelected"})
    public static void setTabLayoutData(TabLayout tabLayout, TabLayout.OnTabSelectedListener listener) {
        tabLayout.addOnTabSelectedListener(listener);
    }

    @BindingAdapter(value = {"onSeekbarChange"})
    public static void onSeekbarChange(SeekBar seekBar, SeekBar.OnSeekBarChangeListener listener) {
        seekBar.setOnSeekBarChangeListener(listener);
    }
}
