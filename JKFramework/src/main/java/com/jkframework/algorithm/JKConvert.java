package com.jkframework.algorithm;


import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;

import com.blankj.utilcode.util.ConvertUtils;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Shorts;
import com.jkframework.application.JKApplication;
import com.jkframework.debug.JKLog;

import org.nlpcn.commons.lang.jianfan.JianFan;
import org.nlpcn.commons.lang.pinyin.Pinyin;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import droidninja.filepicker.utils.ContentUriUtils;


public class JKConvert {

    /**
     * 将字符串转换为整型
     *
     * @param tText 转换的字符串
     * @return 转换后的整型, 若失败则转换为0
     */
    public static int toInt(String tText) {
        try {
            return Integer.parseInt(tText);
        } catch (Exception e) {
            try {
                double dTmp = Double.parseDouble(tText);
                return (int) dTmp;
            } catch (Exception e1) {
                JKLog.ErrorLog("无法将字符串\"" + tText + "\"转成整型.原因为" + e1.getMessage());
                return 0;
            }
        }
    }

    /**
     * 将boolean变量转为整型
     *
     * @param bValue boolean变量
     * @return 真返回1, 否则返回0
     */
    public static int toInt(boolean bValue) {
        return bValue ? 1 : 0;
    }

    /**
     * 将字符串转换为整型
     *
     * @param tText 转换的字符串
     * @return 转换后的整型, 若失败则转换为0
     */
    public static long toLong(String tText) {
        try {
            return Long.parseLong(tText);
        } catch (Exception e) {
            try {
                double dTmp = Double.parseDouble(tText);
                return (long) dTmp;
            } catch (Exception e1) {
                JKLog.ErrorLog("无法将字符串\"" + tText + "\"转成长整型.原因为" + e1.getMessage());
                return 0;
            }
        }
    }

    public static short toShort(byte[] bytes, int startIndex) {
        return toShort(bytes,startIndex,false);
    }

    public static short toShort(byte[] bytes, int startIndex, boolean highFlag) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes, startIndex, 2);
        byteBuffer.order(highFlag ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        return byteBuffer.getShort();
    }

    public static int toInt(byte[] bytes, int startIndex) {
        return toInt(bytes,startIndex,false);
    }

    public static int toInt(byte[] bytes, int startIndex, boolean highFlag) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes, startIndex, 4);
        byteBuffer.order(highFlag ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        return byteBuffer.getInt();
    }

    /**
     * 将字符串转换为单精度浮点型
     *
     * @param tText 转换的字符串
     * @return 转换后的单精度浮点型, 若失败则转换为0.0f
     */
    public static float toFloat(String tText) {
        try {
            return Float.parseFloat(tText);
        } catch (Exception e) {
            JKLog.ErrorLog("无法将字符串\"" + tText + "\"转成浮点型.原因为" + e.getMessage());
            return 0.0f;
        }
    }

    /**
     * 将字符串转换为双精度浮点型
     *
     * @param tText 转换的字符串
     * @return 转换后的单精度浮点型, 若失败则转换为0.0d
     */
    public static double toDouble(String tText) {
        try {
            return Double.parseDouble(tText);
        } catch (Exception e) {
            JKLog.ErrorLog("无法将字符串\"" + tText + "\"转成双精度浮点型.原因为" + e.getMessage());
            return 0.0d;
        }
    }

    /**
     * 将浮点型转换为字符串
     *
     * @param fNum 转换的浮点型
     * @return 转换后的字符串
     */
    public static String toString(float fNum) {
        if ((int) fNum == fNum) return Integer.toString((int) fNum);
        else return Float.toString(fNum);
    }

    /**
     * 将双精度浮点数转换为字符串
     *
     * @param dNum 转换的浮点数
     * @return 转换后的字符串
     */
    public static String toString(double dNum) {
        if ((int) dNum == dNum) return Integer.toString((int) dNum);
        else return Double.toString(dNum);
    }

    /**
     * 将整形转换为字符串
     *
     * @param nNum 转换的整形
     * @return 转换后的字符串
     */
    public static String toString(int nNum) {
        return "" + nNum;
    }

    /**
     * 将长整形转换为字符串
     *
     * @param lNum 转换的长整形
     * @return 转换后的字符串
     */
    public static String toString(long lNum) {
        return "" + lNum;
    }

    /**
     * 将字节数组转换为字符串(默认UTF-8)
     *
     * @param a_byList 字节数组
     * @return 转换后的字符串
     */
    public static String toString(byte[] a_byList) {
        return toString(a_byList, StandardCharsets.UTF_8);
    }

    /**
     * 将字节数组转换为字符串
     *
     * @param a_byList  字节数组
     * @param tEncoding 字节编码(UTF-8,ISO-8859-1,GBK)
     * @return 转换后的字符串
     */
    public static String toString(byte[] a_byList, Charset tEncoding) {
        if (a_byList == null) return "";
        return new String(a_byList, tEncoding);
    }

    /**
     * 取Resource里的string
     *
     * @param nResourceID 资源ID
     * @return 取出的字符串
     */
    public static String toResourceString(int nResourceID) {
        return JKApplication.getInstance().getApplicationContext().getResources().getString
                (nResourceID);
    }

    /**
     * 将字符串转换为字节数组(默认UTF-8)
     *
     * @param tText 字符串
     * @return 转换后的字节数组
     */
    public static byte[] toByteArray(String tText) {
        return tText.getBytes(StandardCharsets.UTF_8);
    }

    public static byte[] toByteArray(short value) {
        return toByteArray(value, false);
    }

    public static byte[] toByteArray(short value, boolean highFlag) {
        byte[] targets = new byte[2];
        if (highFlag) {
            targets[0] = (byte) (value >> 8);
            targets[1] = (byte) value;
        } else {
            targets[0] = (byte) value;
            targets[1] = (byte) (value >> 8);
        }
        return targets;
    }

    public static byte[] toByteArray(int value) {
        return toByteArray(value, false);
    }

    public static byte[] toByteArray(int value, boolean highFlag) {
        byte[] targets = new byte[4];
        if (highFlag) {
            targets[0] = (byte) ((value >> 24) & 0xFF);
            targets[1] = (byte) ((value >> 16) & 0xFF);
            targets[2] = (byte) ((value >> 8) & 0xFF);
            targets[3] = (byte) (value & 0xFF);
        } else {
            targets[0] = (byte) (value & 0xFF);
            targets[1] = (byte) ((value >> 8) & 0xFF);
            targets[2] = (byte) ((value >> 16) & 0xFF);
            targets[3] = (byte) ((value >> 24) & 0xFF);
        }
        return targets;
    }

    /**
     * 将字符串转换为字节数组
     *
     * @param tText     字符串
     * @param tEncoding 字节编码(UTF-8,ISO-8859-1,GBK)
     * @return 转换后的字节数组
     */
    public static byte[] toByteArray(String tText, Charset tEncoding) {
        return tText.getBytes(tEncoding);
    }

    /**
     * 将输入流转换为字节数组
     *
     * @param is 字符串
     * @return 转换后的字节数组
     */
    public static byte[] toByteArray(InputStream is) {
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        byte[] buff = new byte[1024 * 16];
        int rc;
        try {
            while ((rc = is.read(buff, 0, 1024 * 16)) > 0) {
                swapStream.write(buff, 0, rc);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return swapStream.toByteArray();
    }


    /**
     * 将图片转为字节数组
     *
     * @param bmp 图片对象
     * @return 返回图片字节数组
     */
    public static byte[] toByteArray(Bitmap bmp) {
        return toByteArray(bmp, 60);
    }

    /**
     * 将图片转为字节数组
     *
     * @param bmp      图片对象
     * @param nQuality 图片品质(jpg为0~100,小于0为png)
     * @return 返回图片字节数组
     */
    public static byte[] toByteArray(Bitmap bmp, int nQuality) {
        if (bmp == null) return null;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        if (nQuality < 0) bmp.compress(CompressFormat.PNG, 100, output);
        else bmp.compress(CompressFormat.JPEG, nQuality, output);
        return output.toByteArray();
    }

    /**
     * 将InputStream转换成String
     *
     * @param is 输入流
     * @return 转换后的字符串
     */
    public static String toString(InputStream is) {
        return toString(is, StandardCharsets.UTF_8);
    }

    /**
     * 将InputStream转换成String
     *
     * @param is        输入流
     * @param tEncoding 字节编码(UTF-8,ISO-8859-1,GBK)
     * @return 转换后的字符串
     */
    public static String toString(InputStream is, Charset tEncoding) {
        StringBuilder sb = new StringBuilder();
        String readline;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, tEncoding));
            while (br.ready()) {
                readline = br.readLine();
                sb.append(readline);
            }
            br.close();
        } catch (IOException ie) {
            JKLog.ErrorLog("无法将InputStream转成字符串.原因为" + ie.getMessage());
            ie.printStackTrace();
        }
        return sb.toString();
    }

    /**
     * 将图片转为Drawable资源
     *
     * @param btMap 图片
     * @return 转换后的图片资源
     */
    public static Drawable toDrawable(Bitmap btMap) {
        return new BitmapDrawable(JKApplication.getInstance().getApplicationContext()
                .getResources(), btMap);
    }

    /**
     * 将LinkedHashSet模式的字符串数组转成普通数组
     *
     * @param a_tString 字符串数组
     * @return 转换后的普通数组
     */
    public static ArrayList<String> toArrayString(LinkedHashSet<String> a_tString) {
        return new ArrayList<>(a_tString);
    }

    /**
     * 将普通数组转成LinkedHashSet模式的字符串数组
     *
     * @param a_tString 普通数组
     * @return LinkedHashSet模式的字符串数组
     */
    public static LinkedHashSet<String> toLinkedHashSetString(ArrayList<String> a_tString) {
        return new LinkedHashSet<>(a_tString);
    }

    /**
     * 简体转繁体
     *
     * @param tText 简体字符串
     * @return 转换后的繁体字符串
     */
    public static String CnToTw(String tText) {
        return JianFan.j2f(tText);
    }

    /**
     * 繁转换成简体
     *
     * @param tText 繁体字符串
     * @return 转换后的简体字符串
     */
    public static String TwToCn(String tText) {
        return JianFan.f2j(tText);
    }

    public static short GetShort(byte argB1, byte argB2) {
        return Shorts.fromBytes(argB1, argB2);
    }

    public static int GetInt(short argS1, short argS2) {
        return (argS2 & 0xFFFF) | (argS1 << 16);
    }

    public static int GetInt(byte argB1, byte argB2, byte argB3, byte argB4) {
        return Ints.fromBytes(argB1, argB2, argB3, argB4);
    }

    public static double toDouble(byte[] bytes, int startIndex) {
        return toDouble(bytes, startIndex, false);
    }

    public static double toDouble(byte[] bytes, int startIndex, boolean highFlag) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes, startIndex, 8);
        byteBuffer.order(highFlag ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        return byteBuffer.getDouble();
    }

    /**
     * Dip转像素
     *
     * @param fDip dip值
     * @return 像素值
     */
    public static int DipToPx(float fDip) {
        return ConvertUtils.dp2px(fDip);
    }

    /**
     * sp转像素
     *
     * @param fSP sp值
     * @return 像素值
     */
    public static int SpToPx(float fSP) {
        return ConvertUtils.sp2px(fSP);
    }

    /**
     * 汉语拼音转换
     *
     * @param tChinese 需要转换的中文
     * @return 转换后的汉语拼音缩写(小写)
     */
    public static List<String> toPinYin(String tChinese) {
        return Pinyin.firstChar(tChinese);
    }

    /**
     * 汉语拼音转换
     *
     * @param tChinese 需要转换的中文
     * @return 转换后的汉语拼音缩写(小写)
     */
    public static List<String> toFullPinYin(String tChinese) {
        return Pinyin.pinyin(tChinese);
    }

    /**
     * 小数四舍五入
     *
     * @param dNum   原小数
     * @param nIndex 保留的位数
     * @return 返回四舍五入的结果
     */
    public static String toFormatDecimal(double dNum, int nIndex) {
        BigDecimal bd = new BigDecimal(dNum).setScale(nIndex, BigDecimal.ROUND_HALF_UP);
        if (bd.floatValue() == 0) {
            if (dNum < 0) {
                return "-" + bd.toString();
            }
        }
        return bd.toString();
    }

    /**
     * 拼接符合文本
     *
     * @param a_tString 需要拼接的符合文本分段字符串
     * @param a_csStyle 对应每一段的文本样式
     * @return 拼接后的符合文本
     */
    public static SpannableStringBuilder MakeSpannableString(ArrayList<String> a_tString,
                                                             ArrayList<CharacterStyle> a_csStyle) {
        SpannableStringBuilder ssbString = new SpannableStringBuilder();
        for (int i = 0; i < a_tString.size(); i++) {
            SpannableString ssTmp = new SpannableString(a_tString.get(i));
            ssTmp.setSpan(a_csStyle.get(i), 0, ssTmp.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssbString.append(ssTmp);
        }
        return ssbString;
    }

    /**
     * 拼接符合文本
     *
     * @param a_tString  需要拼接的符合文本分段字符串
     * @param a_csStyle1 对应每一段的文本样式
     * @param a_csStyle2 对应每一段的文本样式
     * @return 拼接后的符合文本
     */
    public static SpannableStringBuilder MakeSpannableString(ArrayList<String> a_tString,
                                                             ArrayList<CharacterStyle> a_csStyle1,
                                                             ArrayList<CharacterStyle>
                                                                     a_csStyle2) {
        SpannableStringBuilder ssbString = new SpannableStringBuilder();
        for (int i = 0; i < a_tString.size(); i++) {
            SpannableString ssTmp = new SpannableString(a_tString.get(i));
            ssTmp.setSpan(a_csStyle1.get(i), 0, ssTmp.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssTmp.setSpan(a_csStyle2.get(i), 0, ssTmp.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            ssbString.append(ssTmp);
        }
        return ssbString;
    }

    /**
     * 根据uri获取绝对路径
     *
     * @param uri uri对象
     * @return sd卡上路径
     */
    public static String UriToPath(Uri uri) {
        try {
            return ContentUriUtils.INSTANCE.getFilePath(JKApplication.getInstance().getApplicationContext(), uri);
        } catch (URISyntaxException e) {
            return null;
        }
    }
}