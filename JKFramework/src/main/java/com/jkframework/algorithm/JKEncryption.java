package com.jkframework.algorithm;

import android.util.Base64;

import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.StringUtils;
import com.jkframework.debug.JKLog;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class JKEncryption {

    /**
     * 获取32位大写MD5码
     *
     * @param tString 需要加密的字符串
     * @return 返回加密后的字符串
     */
    public static String MD5_32(String tString) {
        if (StringUtils.isEmpty(tString))
            return "";
        return JKAnalysis.toUpper(EncryptUtils.encryptMD5ToString(tString));
    }

    /**
     * 获取16位大写MD5码
     *
     * @param tString 需要加密的字符串
     * @return 返回加密后的字符串
     */
    public static String MD5_16(String tString) {
        if (StringUtils.isEmpty(tString))
            return "";
        return JKAnalysis.toUpper(EncryptUtils.encryptMD5ToString(tString)).substring(8, 24);
    }

    /**
     * 获取16位MD5码二进制
     *
     * @param tString 需要加密的字符串
     * @return 返回加密后的字节数组
     */
    public static byte[] MD5_byte(String tString) {
        return EncryptUtils.encryptMD5(JKConvert.toByteArray(tString));
    }

    /**
     * 获取40位大写SHA1码
     *
     * @param tString 需要加密的字符串
     * @return 返回加密后的字符串
     */
    public static String SHA1_40(String tString) {
        return EncryptUtils.encryptSHA1ToString(tString);
    }


    /**
     * Aes 128位解密
     *
     * @param a_byList  加密文件字节数组
     * @param tPassword 加密钥匙
     * @return 解密后的字节数组
     */
    public static byte[] AesDecryptor(byte[] a_byList, String tPassword) {
        byte[] a_byByte = JKConvert.toByteArray(MD5_16(tPassword));
        if (a_byByte == null)
            return null;
        IvParameterSpec dps = new IvParameterSpec(a_byByte);
        SecretKeySpec secretKey = new SecretKeySpec(a_byByte, "AES");

        try {
            Cipher cpjm = Cipher.getInstance("AES/CBC/NoPadding");
            cpjm.init(Cipher.DECRYPT_MODE, secretKey, dps);
            byte[]  a_byBack = cpjm.doFinal(a_byList);
            int nRealSize = 0;
            for (int i = a_byBack.length - 1; i >= 0; i--) {
                if (a_byBack[i] != '\0') {
                    nRealSize = i + 1;
                    break;
                }
            }
            byte[] a_byRealBack = new byte[nRealSize];
            System.arraycopy(a_byBack, 0, a_byRealBack, 0, nRealSize);
            return a_byRealBack;
        } catch (Exception e) {
            JKLog.ErrorLog("Aes解密失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Aes 128位加密
     *
     * @param a_byList  加密文件字节数组
     * @param tPassword 加密钥匙
     * @return 加密后的字节数组
     */
    public static byte[] AesEncryptor(byte[] a_byList, String tPassword) {
        byte[] a_byByte = JKConvert.toByteArray(MD5_16(tPassword));
        if (a_byByte == null)
            return null;
        IvParameterSpec dps = new IvParameterSpec(a_byByte);
        SecretKeySpec secretKey = new SecretKeySpec(a_byByte, "AES");

        try {
            Cipher cpjm = Cipher.getInstance("AES/CBC/NoPadding");
            int nBlockSize = cpjm.getBlockSize();
            int nFilLength = a_byList.length;
            if (nFilLength % nBlockSize != 0) {
                nFilLength = nFilLength + (nBlockSize - (nFilLength % nBlockSize));
            }
            byte[] a_byFillList = new byte[nFilLength];
            System.arraycopy(a_byList, 0, a_byFillList, 0, a_byList.length);

            cpjm.init(Cipher.ENCRYPT_MODE, secretKey, dps);
            return cpjm.doFinal(a_byFillList);
        } catch (Exception e) {
            JKLog.ErrorLog("Aes加密失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Base64解密
     *
     * @param a_byList 解密前的字节数组
     * @return Base64 解密后的字节数组
     */
    public static byte[] Base64Decryptor(byte[] a_byList) {
        return Base64.decode(a_byList, Base64.NO_WRAP);
    }

    /**
     * Base64加密
     *
     * @param a_byList 加密前的字节数组
     * @return Base64 加密后的字节数组
     */
    public static byte[] Base64Encryptor(byte[] a_byList) {
        return Base64.encode(a_byList, Base64.NO_WRAP);
    }

    /**
     * Base64加密
     *
     * @param a_byList 加密前的字节数组
     * @return Base64 加密后的字符串
     */
    public static String Base64EncryptorString(byte[] a_byList) {
        return JKConvert.toString(Base64.encode(a_byList, Base64.NO_WRAP));
    }

    /**
     * 将字符串转为Url编码
     *
     * @param tString
     *            需要转换的字符串
     * @return url编码
     */
    public static String UrlEncode(String tString) {
        return UrlEncode(tString,"UTF-8");
    }

    /**
     * 将字符串转为Url编码
     *
     * @param tString
     *            需要转换的字符串
     * @param tEncoding
     *            字符编码(UTF-8,GBK,ISO-8859-1)
     * @return url编码
     */
    public static String UrlEncode(String tString, String tEncoding) {
        String tBack = "";
        try {
            tBack = java.net.URLEncoder.encode(tString, tEncoding);
        } catch (UnsupportedEncodingException e) {
            JKLog.ErrorLog("无法将字符串转成Url编码.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return tBack;
    }

    /**
     * 将url编码转成string
     *
     * @param tString
     *            url编码
     * @return 转换编码后的字符串
     */
    public static String UrlDecode(String tString) {
        return UrlDecode(tString,"UTF-8");
    }

    /**
     * 将url编码转成string
     *
     * @param tString
     *            url编码
     * @param tEncoding
     *            字符编码(UTF-8,GBK,ISO-8859-1)
     * @return 转换编码后的字符串
     */
    public static String UrlDecode(String tString, String tEncoding) {
        String tBack = "";
        try {
            tBack = URLDecoder.decode(tString, tEncoding);
        } catch (UnsupportedEncodingException e) {
            JKLog.ErrorLog("无法将Url编码转成字符串.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return tBack;
    }
}
