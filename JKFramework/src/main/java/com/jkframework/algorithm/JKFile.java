package com.jkframework.algorithm;

import android.content.ContentResolver;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.StatFs;
import android.provider.MediaStore;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.FileIOUtils;
import com.blankj.utilcode.util.SDCardUtils;
import com.jkframework.activity.JKFileActivity;
import com.jkframework.application.JKApplication;
import com.jkframework.bean.JKFolderData;
import com.jkframework.bean.JKImageData;
import com.jkframework.config.JKPreferences;
import com.jkframework.config.JKSystem;
import com.jkframework.debug.JKLog;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class JKFile {

    /**
     * 选择文件监听回调
     */
    public static JKChoiceListener mChoiceListener = null;

    /**
     * 判断文件(夹)是否存在
     *
     * @param tFileName 文件路径地址
     * @return 存在返回true, 否则返回false
     */
    public static boolean IsExists(String tFileName) {
        if (tFileName.indexOf("/") != 0)
            tFileName = "/" + tFileName;
        return com.blankj.utilcode.util.FileUtils.isFileExists(tFileName);
    }

    /**
     * 判断是否为文件
     *
     * @param tFileName 文件路径地址
     * @return 是文件返回true, 否则返回false
     */
    public static boolean IsFile(String tFileName) {
        if (tFileName.indexOf("/") != 0)
            tFileName = "/" + tFileName;

        return com.blankj.utilcode.util.FileUtils.isFile(tFileName);
    }

    /**
     * 判断文件是否为文件夹
     *
     * @param tFileName 文件路径地址
     * @return 存在返回true, 否则返回false
     */
    public static boolean IsDirectory(String tFileName) {
        if (tFileName.indexOf("/") != 0)
            tFileName = "/" + tFileName;

        return com.blankj.utilcode.util.FileUtils.isDir(tFileName);
    }

    /**
     * 在指定路径文件上创建所有不存在的根目录   (文件夹末尾加"/")
     *
     * @param dirName 文件路径(匹配所有向上的文件夹)
     */
    public static void CreateDir(String dirName) {
        // 替换所有的反斜杠为正斜杠
        dirName = dirName.replace("\\", "/");

        // 为了保证路径是绝对路径，确保路径以"/"开始
        if (!dirName.startsWith("/")) {
            dirName = "/" + dirName;
        }

        // 如果路径以文件名结尾（即没有以"/"结尾），则移除文件名部分
        if (!dirName.endsWith("/")) {
            dirName = dirName.substring(0, dirName.lastIndexOf("/") + 1);
        }

        // 创建文件对象
        File file = new File(dirName);

        // 如果目录已存在，直接返回
        if (file.exists()) {
            return;
        }

        // 获取父目录路径
        String parentDir = dirName.substring(0, dirName.lastIndexOf("/", dirName.length() - 2) + 1);

        // 递归地创建父目录
        CreateDir(parentDir);

        // 创建目录并设置权限
        if (file.mkdir()) {
            // 设置权限
            boolean isReadableSet = file.setReadable(true);
            boolean isWritableSet = file.setWritable(true);
            if (!isReadableSet || !isWritableSet) {
                JKLog.ErrorLog("设置文件夹权限失败");
            }
        } else {
            JKLog.ErrorLog("创建目录失败");
        }
    }

    /**
     * 以覆盖的方式写入文件
     *
     * @param tPath  文本的路径地址
     * @param buffer 写入的数据数组
     */
    public static void WriteFile(String tPath, byte[] buffer) {
        try {
            FileIOUtils.writeFileFromBytesByStream(tPath,buffer);
        } catch (Exception e) {
            JKLog.ErrorLog("写入SD卡数据数组失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 写入SD卡数据流
     *
     * @param tPath 写入的地址
     * @param is    数据流
     */
    public static void WriteFile(String tPath, InputStream is) {
        try {
            FileIOUtils.writeFileFromIS(tPath,is);
        } catch (Exception e) {
            JKLog.ErrorLog("写入SD卡数据流失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 以覆盖的方式写入文件(默认UTF-8)
     *
     * @param tPath 文本的路径地址
     * @param tText 写入的字符串
     */
    public static void WriteFile(String tPath, String tText) {
        FileIOUtils.writeFileFromString(tPath,tText);
    }

    /**
     * 指定编码格式覆盖的方式写入文件
     *
     * @param tPath     文本的路径地址
     * @param tText     写入的字符串
     * @param tEncoding 编码格式(GBK,UTF-8)
     */
    public static void WriteFile(String tPath, String tText, Charset tEncoding) {
        try {
            FileUtils.writeStringToFile(new File(tPath), tText, tEncoding);
        } catch (Exception e) {
            JKLog.ErrorLog("写入SD卡字符串失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 以末尾追加的方式写入文件(默认UTF-8)
     *
     * @param tPath  文本的路径地址
     * @param buffer 写入的字符串
     */
    public static void AppendFile(String tPath, byte[] buffer) {
        try {
            FileIOUtils.writeFileFromBytesByStream(tPath,buffer,true);
        } catch (Exception e) {
            JKLog.ErrorLog("附加写入SD卡数据数组失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 以末尾追加的方式写入文件(默认UTF-8)
     *
     * @param tPath 文本的路径地址
     * @param tText 写入的字符串
     */
    public static void AppendFile(String tPath, String tText) {
        try {
            FileIOUtils.writeFileFromString(tPath,tText,true);
        } catch (Exception e) {
            JKLog.ErrorLog("附加写入SD卡字符串失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 读取文件
     *
     * @param tPath 文件路径地址
     * @return 返回文件字节数组
     */
    public static byte[] ReadBytes(String tPath) {
        try {
            return FileIOUtils.readFile2BytesByStream(tPath);
        } catch (Exception e) {
            JKLog.ErrorLog("读取SD卡数据数组失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * 读取assets文件夹里的文件
     *
     * @param tPath 文件路径
     * @return 文件流
     */
    public static InputStream Read(String tPath) {
        try {
            return new FileInputStream(tPath);
        } catch (IOException e) {
            JKLog.ErrorLog("读取文件失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 读取文件(默认UTF-8)
     *
     * @param tPath 文件路径地址
     * @return 返回文件内容字符串
     */
    public static String ReadFile(String tPath) {
        return ReadFile(tPath, StandardCharsets.UTF_8);
    }

    /**
     * 读取文件
     *
     * @param tPath     文件路径地址
     * @param tEncoding 字符编码(UTF-8,GBK,ISO-8859-1)
     * @return 返回文件内容字符串
     */
    public static String ReadFile(String tPath, Charset tEncoding) {
        try {
            return FileIOUtils.readFile2String(tPath,tEncoding.name());
        } catch (Exception e) {
            JKLog.ErrorLog("读取文件字符串失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 读取Assets文件(默认UTF-8)
     *
     * @param tPath 文件路径地址
     * @return 返回文件字符串
     */
    public static String ReadAssetsFile(String tPath) {
        return ReadAssetsFile(tPath,StandardCharsets.UTF_8);
    }

    /**
     * 读取Assets文件
     *
     * @param tPath     文件路径地址
     * @param tEncoding 字符编码(UTF-8,GBK,ISO-8859-1)
     * @return 返回文件内容字符串
     */
    public static String ReadAssetsFile(String tPath, Charset tEncoding) {
        InputStream isStream = ReadAssets(tPath);
        return JKConvert.toString(isStream, tEncoding);
    }

    /**
     * 获取文件(夹)大小
     *
     * @param tPath SD卡路径
     * @return 文件大小以字节为单位
     */
    public static long GetFileSize(String tPath) {
        File fInput = new File(tPath);
        try {
            return FileUtils.sizeOf(fInput);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 获取SD卡总容量
     *
     * @param tPath SD卡根目录
     * @return SD卡字节大小
     */
    public static long GetSDCardSize(String tPath) {
        StatFs stat = new StatFs(tPath);
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        return blockSize * totalBlocks;
    }

    /**
     * 获取SD卡可用容量
     *
     * @param tPath SD卡根目录
     * @return SD卡字节大小
     */
    public static long GetSDCardAvailableSize(String tPath) {
        StatFs stat = new StatFs(tPath);
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getAvailableBlocksLong();
        return blockSize * totalBlocks;
    }

    /**
     * 遍历文件夹里所有的文件
     *
     * @param tPath        遍历文件夹路径
     * @param bIsIterative 是否进入子文件夹
     * @return 文件列表
     */
    public static ArrayList<String> GetFiles(String tPath, boolean bIsIterative) {
        ArrayList<String> a_tBack = new ArrayList<>();
        /*判断目录是否存在以及是否是目录*/
        if (!IsExists(tPath))
            return a_tBack;
        if (IsFile(tPath)) {
            a_tBack.add(tPath);
            return a_tBack;
        }

        List<File> a_flBack = com.blankj.utilcode.util.FileUtils.listFilesInDir(tPath, bIsIterative);
        for (File file : a_flBack) {
            a_tBack.add(file.getAbsolutePath());
        }
        return a_tBack;
    }

    /**
     * 遍历文件夹里所有的文件夹
     *
     * @param tPath        遍历文件夹路径
     * @param bIsIterative 是否进入子文件夹
     * @return 文件夹列表
     */
    public static ArrayList<String> GetFolders(String tPath, boolean bIsIterative) {
        ArrayList<String> a_tBack = new ArrayList<>();
        /*判断目录是否存在以及是否是目录*/
        if (!IsExists(tPath))
            return a_tBack;
        if (IsFile(tPath))
            return a_tBack;

        if (bIsIterative) {
            /*创建队列*/
            Queue<String> q_tMatchList = new ConcurrentLinkedQueue<>();
            q_tMatchList.offer(tPath);
            while (!q_tMatchList.isEmpty()) {
                File fiFile = new File(Objects.requireNonNull(q_tMatchList.poll()));
                File[] a_flFiles = fiFile.listFiles();
                if (a_flFiles != null) {
                    for (File a_flFile : a_flFiles) {
                        if (JKFile.IsDirectory(a_flFile.getPath())) {
                            a_tBack.add(a_flFile.getPath());
                            q_tMatchList.offer(a_flFile.getPath());
                        }
                    }
                }
            }
        } else {        //当前目录下的文件
            File fiFile = new File(tPath);
            File[] a_flFiles = fiFile.listFiles();
            if (a_flFiles != null) {
                for (File a_flFile : a_flFiles) {
                    if (JKFile.IsDirectory(a_flFile.getPath()))
                        a_tBack.add(a_flFile.getPath());
                }
            }
        }
        return a_tBack;
    }

    /**
     * 获取文件名
     *
     * @param tPath 文件路径地址
     * @return 文件名
     */
    public static String GetFileName(String tPath) {
        return FilenameUtils.getName(tPath);
    }

    /**
     * 获取网络文件名
     *
     * @param tUrl 文件路径地址
     * @return 文件名
     */
    public static String GetUrlFileName(String tUrl) {
        int lastIndex = tUrl.lastIndexOf("/");
        lastIndex = Math.max(lastIndex,tUrl.lastIndexOf("\\"));
        if (lastIndex >= 0)
            return tUrl.substring(lastIndex + 1);
        else
            return tUrl;
    }

    /**
     * Java文件操作 获取文件扩展名
     *
     * @param tFilename 文件路径
     * @return 返回文件扩展名
     */
    public static String GetFileExtension(String tFilename) {
        return FilenameUtils.getExtension(tFilename);
    }

    /**
     * Java文件操作 获取不带扩展名的文件名
     *
     * @param tFilename 文件名
     * @return 不带扩展名的文件名
     */
    public static String GetBaseName(String tFilename) {
        return FilenameUtils.getBaseName(tFilename);
    }

    /**
     * 获取SD卡的根目录
     *
     * @return SD卡根目录路径地址
     */
    public static String GetRootPath() {
        return SDCardUtils.getSDCardPathByEnvironment();
    }

    /**
     * 获取所有SD卡的根目录
     *
     * @return SD卡根目录集合
     */
    public static List<String> GetSDCardDir() {
        ArrayList<String> a_tPath = new ArrayList<>();
        for (SDCardUtils.SDCardInfo info : SDCardUtils.getSDCardInfo()) {
            a_tPath.add(info.getPath());
        }
        return a_tPath;
    }

    /**
     * 获取SD卡的根目录(自动卸载)
     *
     * @return SD卡根目录路径地址
     */
    public static String GetPublicPath() {
        if (JKSystem.IsSDcardEnable()) {
            String tSdcardPath = null;
            try {
                File flFile = JKApplication.getInstance().getApplicationContext().getExternalFilesDir(null);
                if (flFile != null) {
                    tSdcardPath = flFile.getAbsolutePath();
                    if (tSdcardPath.indexOf("/") != 0)
                        tSdcardPath = "/" + tSdcardPath;
                }
            } catch (NullPointerException e) {
                return GetPrivatePath();
            }
            return tSdcardPath;
        } else {
            return GetPrivatePath();
        }
    }

    /**
     * 获取SD卡缓存的根目录(自动卸载)
     *
     * @return SD卡缓存根目录路径地址
     */
    public static String GetPublicCachePath() {
        if (JKSystem.IsSDcardEnable()) {
            String tSdcardPath = null;
            try {
                File flFile = JKApplication.getInstance().getApplicationContext().getExternalCacheDir();
                if (flFile != null) {
                    tSdcardPath = flFile.getAbsolutePath();
                    if (tSdcardPath.indexOf("/") != 0)
                        tSdcardPath = "/" + tSdcardPath;
                }
            } catch (NullPointerException e) {
                return GetPrivateCachePath();
            }
            return tSdcardPath;
        } else {
            return GetPrivateCachePath();
        }
    }

    /**
     * 获取内存缓存的根目录
     *
     * @return 内存缓存根目录路径地址
     */
    public static String GetPrivateCachePath() {
        String tSdcardPath = null;
        try {
            tSdcardPath = JKApplication.getInstance().getApplicationContext().getCacheDir().getAbsolutePath();
            if (tSdcardPath.indexOf("/") != 0)
                tSdcardPath = "/" + tSdcardPath;
        } catch (NullPointerException e) {
            JKLog.ErrorLog("无法获取内存缓存卡目录.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return tSdcardPath;
    }

    /**
     * 获取内存卡的根目录
     *
     * @return 内存卡根目录路径地址
     */
    public static String GetPrivatePath() {
        String tSdcardPath = null;
        try {
            tSdcardPath = JKApplication.getInstance().getApplicationContext().getFilesDir().getAbsolutePath();
            if (tSdcardPath.indexOf("/") != 0)
                tSdcardPath = "/" + tSdcardPath;
        } catch (NullPointerException e) {
            JKLog.ErrorLog("无法获取内存卡目录.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return tSdcardPath;
    }

    /**
     * 删除文件(夹)
     *
     * @param tFile 文件(夹)目录地址
     * @return 操作成功返回true
     */
    public static boolean DeleteFile(String tFile) {
        boolean result;
        if (JKFile.IsDirectory(tFile))
            result = com.blankj.utilcode.util.FileUtils.deleteAllInDir(tFile);
        else
            result = com.blankj.utilcode.util.FileUtils.delete(tFile);
        return result;
    }

    /**
     * 将assets文件复制到指定目录
     *
     * @param tAssets   assets文件名
     * @param tFilePath 目标文件目录(末尾不带"/")
     * @return 操作成功返回true
     */
    public static boolean AssetsToPath(String tAssets, String tFilePath) {
        boolean result = false;
        try {
            InputStream isStrem = JKFile.ReadAssets(tAssets);
            if (isStrem != null)
            {
                FileUtils.delete(new File(tFilePath));
                result = FileIOUtils.writeFileFromIS(new File(tFilePath),isStrem);
            }
        } catch (IOException e) {
            JKLog.ErrorLog("无法将Assets路径的文件拷贝到SD卡上.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 重命名文件(夹)
     *
     * @param tSourceFile 源文件(夹)地址
     * @param tNewName    更改后的新名字
     */
    public static void RenameFile(String tSourceFile, String tNewName) {
        if (!IsExists(tSourceFile))
            return;
        try {
            File flSourceFile = new File(tSourceFile);
            File flTargetFolder = new File(flSourceFile.getParentFile(), tNewName);
            if (flSourceFile.isDirectory()) {
                FileUtils.moveDirectory(flSourceFile, flTargetFolder);
            } else if (flSourceFile.isFile()) {
                FileUtils.moveFile(flSourceFile, flTargetFolder);
            }
        } catch (IOException e) {
            JKLog.ErrorLog("重命名文件(夹)失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 剪切文件(夹)
     *
     * @param tSourceFile   源文件(夹)地址
     * @param tTargetFolder 目标文件夹地址下
     */
    public static void CutFileToDirectory(String tSourceFile, String tTargetFolder) {
        if (!IsExists(tSourceFile))
            return;

        try {
            File flSourceFile = new File(tSourceFile);
            File flTargetFolder = new File(tTargetFolder);
            if (flSourceFile.isDirectory()) {
                FileUtils.moveDirectoryToDirectory(flSourceFile, flTargetFolder, true);
            } else if (flSourceFile.isFile()) {
                FileUtils.moveFileToDirectory(flSourceFile, flTargetFolder, true);
            }
        } catch (IOException e) {
            JKLog.ErrorLog("剪切文件(夹)失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 剪切文件(夹)
     *
     * @param tSourceFile 源文件(夹)地址
     * @param tTargetFile 目标文件(夹)地址下
     */
    public static void CutFile(String tSourceFile, String tTargetFile) {
        if (!IsExists(tSourceFile))
            return;

        try {
            File flSourceFile = new File(tSourceFile);
            File flTargetFolder = new File(tTargetFile);
            if (flSourceFile.isDirectory()) {
                FileUtils.moveDirectory(flSourceFile, flTargetFolder);
            } else if (flSourceFile.isFile()) {
                FileUtils.moveFile(flSourceFile, flTargetFolder);
            }
        } catch (IOException e) {
            JKLog.ErrorLog("剪切文件(夹)失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 复制文件
     *
     * @param tSourceFile   源文件
     * @param tTargetFolder 目标文件夹地址下
     */
    public static void CopyFileToDirectory(String tSourceFile, String tTargetFolder) {
        if (!IsExists(tSourceFile))
            return;

        try {
            File flSourceFile = new File(tSourceFile);
            File flTargetFolder = new File(tTargetFolder);
            if (flSourceFile.isDirectory()) {
                FileUtils.copyDirectoryToDirectory(flSourceFile, flTargetFolder);
            } else if (flSourceFile.isFile()) {
                FileUtils.copyFileToDirectory(flSourceFile, flTargetFolder);
            }
        } catch (IOException e) {
            JKLog.ErrorLog("复制文件(夹)失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 复制文件
     *
     * @param tSourceFile 源文件
     * @param tTargetFile 目标文件
     */
    public static void CopyFile(String tSourceFile, String tTargetFile) {
        if (!IsExists(tSourceFile))
            return;

        try {
            File flSourceFile = new File(tSourceFile);
            File flTargetFolder = new File(tTargetFile);
            if (flSourceFile.isDirectory()) {
                FileUtils.copyDirectory(flSourceFile, flTargetFolder);
            } else if (flSourceFile.isFile()) {
                FileUtils.copyFile(flSourceFile, flTargetFolder);
            }
        } catch (IOException e) {
            JKLog.ErrorLog("复制文件(夹)失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * 读取assets文件夹里的文件
     *
     * @param tPath 文件路径
     * @return 文件流
     */
    public static InputStream ReadAssets(String tPath) {
        try {
            return JKApplication.getInstance().getApplicationContext().getResources().getAssets().open(tPath);
        } catch (IOException e) {
            JKLog.ErrorLog("读取Assets文件失败.原因为" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取文件32位大写MD5码
     *
     * @param tFilePath 文件路径
     * @return 文件MD5码
     */
    public static String toMD5(String tFilePath) {
        return com.blankj.utilcode.util.FileUtils.getFileMD5ToString(tFilePath);
    }

    /**
     * 获取Assets文件32位大写MD5码
     *
     * @param tAssetsPath 文件路径
     * @return 文件MD5码
     */
    public static String toAssetsMD5(String tAssetsPath) {
        InputStream fis = ReadAssets(tAssetsPath);
        byte[] buffer = new byte[1024 * 16];
        int numRead;
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            if (fis != null) {
                while ((numRead = fis.read(buffer)) > 0) {
                    md5.update(buffer, 0, numRead);
                }
                fis.close();
            }
            StringBuilder hexString = new StringBuilder();
            for (byte b : md5.digest()) {
                hexString.append(Integer.toHexString(0xFF & b));
            }
            return hexString.toString().toUpperCase(Locale.getDefault());
        } catch (Exception e) {
            JKLog.ErrorLog("获取文件MD5失败.原因为" + e.getMessage());
            return "";
        }
    }

    /**
     * 获取安装包位置
     *
     * @return 返回安装包位置
     */
    public static String GetApkPath() {
        final ApplicationInfo applicationInfo = JKApplication.getInstance().getApplicationContext().getApplicationInfo();
        return applicationInfo.sourceDir;
    }

    /**
     * 选择SD卡上的文件
     *
     * @param jkclChoiceListener 选择文件的回调监听
     */
    public static void ChoiceFilePath(final JKChoiceListener jkclChoiceListener) {
        mChoiceListener = jkclChoiceListener;
        ActivityUtils.startActivity(JKFileActivity.class);
    }

    /**
     * 获取相册接口列表
     *
     * @return 相册结构列表
     */
    public static ArrayList<JKFolderData> GetPhotoAlbumList() {
        ArrayList<JKFolderData> mResultFolder = new ArrayList<>();
        String[] IMAGE_PROJECTION = {
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.DATE_ADDED,
                MediaStore.Images.Media._ID};

        ContentResolver contentResolver = JKApplication.getInstance().getApplicationContext().getContentResolver();
        Cursor data = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_PROJECTION,
                null, null, IMAGE_PROJECTION[2] + " DESC");
        if (data != null) {
            int count = data.getCount();
            if (count > 0) {
                data.moveToFirst();
                do {
                    String path = data.getString(data.getColumnIndexOrThrow(IMAGE_PROJECTION[0]));
                    if (path == null || path.length() == 0)
                        continue;
                    String name = data.getString(data.getColumnIndexOrThrow(IMAGE_PROJECTION[1]));
                    long dateTime = data.getLong(data.getColumnIndexOrThrow(IMAGE_PROJECTION[2]));
                    JKImageData image = new JKImageData(path, name, dateTime);

                    File imageFile = new File(path);
                    File folderFile = imageFile.getParentFile();
                    JKFolderData folder = new JKFolderData();
                    folder.name = folderFile != null ? folderFile.getName() : null;
                    folder.path = folderFile != null ? folderFile.getAbsolutePath() : null;
                    folder.cover = image;
                    if (!mResultFolder.contains(folder)) {
                        List<JKImageData> imageList = new ArrayList<>();
                        imageList.add(image);
                        folder.images = imageList;
                        mResultFolder.add(folder);
                    } else {
                        // 更新
                        JKFolderData f = mResultFolder.get(mResultFolder.indexOf(folder));
                        f.images.add(image);
                    }
                } while (data.moveToNext());
            }
            data.close();
        }
        return mResultFolder;
    }

    public interface JKChoiceListener {

        /**
         * 选择完毕时回调
         *
         * @param pathList 图片选择完后的地址uri,没有选择返回null
         */
        void FinishChoice(ArrayList<Uri> pathList);
    }
}