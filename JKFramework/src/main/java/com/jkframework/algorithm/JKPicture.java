package com.jkframework.algorithm;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;

import com.blankj.utilcode.util.ConvertUtils;
import com.blankj.utilcode.util.ImageUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.bumptech.glide.Glide;
import com.jkframework.application.JKApplication;
import com.jkframework.manager.JKActivityManager;

import java.util.Objects;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;

import static autodispose2.AutoDispose.autoDisposable;


public class JKPicture {

    /**
     * 同步获取图片
     *
     * @param tPath 图片路径地址
     * @return 图片Bitmap
     */
    public static Bitmap LoadBitmap(String tPath) {
        return ImageUtils.getBitmap(tPath);
    }


    /**
     * 同步获取图片
     *
     * @param a_byData 图片字节
     * @return 图片Bitmap
     */
    public static Bitmap LoadBitmap(byte[] a_byData) {
        return ImageUtils.getBitmap(a_byData, 0);
    }

    /**
     * 异步获取图片
     *
     * @param m_Listener 回调响应
     * @param tPath      图片路径地址
     */
    public static void LoadBitmapAsync(final JKPictureListener m_Listener, final String tPath) {
        Flowable.just(tPath)
                .map(new Function<String, Bitmap>() {
                    @Override
                    public Bitmap apply(@NonNull String s) throws Exception {
                        return Glide.with(JKActivityManager.GetCurrentActivity()).asBitmap().load(tPath).submit().get();
//                        return Picasso.get().load(new File(s)).get();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(Objects.requireNonNull(JKActivityManager.GetCurrentLifecycleActivity()), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Bitmap>() {
                    @Override
                    public void accept(Bitmap bitmap) {
                        if (m_Listener != null)
                            m_Listener.FinishSet(bitmap);
                    }
                });
    }

    /**
     * 异步获取图片
     *
     * @param m_Listener 回调响应
     * @param a_byList   图片字节数组
     */
    public static void LoadBitmapAsync(final JKPictureListener m_Listener, final byte[] a_byList) {
        Flowable.just(a_byList)
                .map(new Function<byte[], Bitmap>() {
                    @Override
                    public Bitmap apply(@NonNull byte[] a_byList) {
                        return LoadBitmap(a_byList);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(Objects.requireNonNull(JKActivityManager.GetCurrentLifecycleActivity()), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Bitmap>() {
                    @Override
                    public void accept(Bitmap bitmap) {
                        if (m_Listener != null)
                            m_Listener.FinishSet(bitmap);
                    }
                });
    }

    /**
     * 同步获取资源图片
     *
     * @param nResID 图片路径地址
     * @return 图片Bitmap
     */
    public static Bitmap LoadResourceBitmap(int nResID) {
        return ImageUtils.getBitmap(nResID);
    }


    /**
     * 同步获取Assets图片
     *
     * @param tPath 图片路径地址
     * @return 图片Bitmap
     */
    public static Bitmap LoadAssetsBitmap(String tPath) {
        return ImageUtils.getBitmap(JKFile.ReadAssets(tPath));
    }

    /**
     * 异步获取Assets图片
     *
     * @param m_Listener 回调响应
     * @param tPath      图片路径地址
     */
    public static void LoadAssetsBitmapAsync(final JKPictureListener m_Listener, final String tPath) {
        Flowable.just(tPath)
                .map(new Function<String, Bitmap>() {
                    @Override
                    public Bitmap apply(@NonNull String s) {
                        return LoadAssetsBitmap(tPath);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .to(autoDisposable(AndroidLifecycleScopeProvider.from(Objects.requireNonNull(JKActivityManager.GetCurrentLifecycleActivity()), Lifecycle.Event.ON_DESTROY)))
                .subscribe(new Consumer<Bitmap>() {
                    @Override
                    public void accept(Bitmap bitmap) {
                        if (m_Listener != null)
                            m_Listener.FinishSet(bitmap);
                    }
                });
    }

    /**
     * 缩放图片
     *
     * @param bmBitmap 需要缩放的图片
     * @param fScaleX  缩放的X轴参数
     * @param fScaleY  缩放的Y轴参数
     * @param bType    true为百分比缩放,false为实际像素缩放
     * @return 返回缩放后的图片
     */
    public static Bitmap Scale(Bitmap bmBitmap, float fScaleX, float fScaleY, boolean bType) {
        if (bType)
            return ImageUtils.scale(bmBitmap, fScaleX, fScaleY);
        else
            return ImageUtils.scale(bmBitmap, (int) fScaleX, (int) fScaleY);
    }

    /**
     * 裁剪图片
     *
     * @param bmBitmap 需要裁剪的图片
     * @param rtSize   裁剪大小(4个参数分别为x,y,width,height)
     * @return 返回裁剪后的图片
     */
    public static Bitmap Cut(Bitmap bmBitmap, Rect rtSize) {
        return ImageUtils.clip(bmBitmap, rtSize.left, rtSize.top, rtSize.right, rtSize.bottom);
    }

    /**
     * 获取布局屏幕截图
     *
     * @param vView 要获取的布局
     * @return 返回Bitmap
     */
    public static Bitmap GetScreenBitmap(View vView) {
        return ConvertUtils.view2Bitmap(vView);
    }

    /**
     * 获取当前屏幕截图
     *
     * @return 当前执行的截图
     */
    public static Bitmap GetScreenBitmap() {
        return ScreenUtils.screenShot(JKActivityManager.GetCurrentActivity());
    }

    /**
     * 设置图片圆角
     *
     * @param bitmap 图片文件
     * @param round  圆角系数
     * @return 处理圆角后的图片
     */
    public static Bitmap SetRoundBitmap(Bitmap bitmap, int round) {
        return ImageUtils.toRoundCorner(bitmap, round);
    }

    /**
     * 图片合成
     *
     * @param src        原始图片
     * @param watermark  合成的水印图片
     * @param nDirection (0为左上,1为右上,2为左下,3为右下)
     * @return 合成后的图片
     */
    public static Bitmap Compose(Bitmap src, Bitmap watermark, int nDirection) {
        switch (nDirection) {
            default:
            case 0:
                return ImageUtils.addImageWatermark(src, watermark, 0, 0, 255);
            case 1:
                return ImageUtils.addImageWatermark(src, watermark, src.getWidth() - watermark.getWidth(), 0, 255);
            case 2:
                return ImageUtils.addImageWatermark(src, watermark, 0, src.getHeight() - watermark.getHeight(), 255);
            case 3:
                return ImageUtils.addImageWatermark(src, watermark, src.getWidth() - watermark.getWidth(), src.getHeight() - watermark.getHeight(), 255);
        }
    }

    /**
     * 图片灰化
     *
     * @param bmpOriginal 原始图片
     * @return 灰化后的图片
     */
    public static Bitmap ToGray(Bitmap bmpOriginal) {
        return ImageUtils.toGray(bmpOriginal);
    }

    /**
     * 图片增加倒影
     *
     * @param mBitmap 原始图片
     * @return 倒影图片
     */
    public static Bitmap ReverseShadow(Bitmap mBitmap) {
        return ImageUtils.addReflection(mBitmap, mBitmap.getHeight());
    }

    /**
     * 不加载图片,仅获得图片宽高
     *
     * @param tPath 图片SD卡路径
     * @return 返回图片宽高, x为宽, y为高
     */
    public static Point GetBitmapSizeOnly(String tPath) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        try {
            BitmapFactory.Options.class.getField("inNativeAlloc").setBoolean(opt, true);
        } catch (Exception ignored) {
        }
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(tPath, opt);

        Point ptSize = new Point();
        ptSize.x = opt.outWidth;
        ptSize.y = opt.outHeight;
        return ptSize;
    }

    /**
     * 将图片地址添加到相机相册里去
     *
     * @param tPath 图片路径
     * @return 返回在相册里的路径地址
     */
    public static String AddToAlbum(String tPath) {
        try {
            if (tPath == null || tPath.length() == 0)
                return tPath;
            Uri uri = Uri.parse(MediaStore.Images.Media.insertImage(JKApplication.getInstance().getApplicationContext().getContentResolver(),
                    tPath, JKFile.GetBaseName(tPath), null));
            return JKConvert.UriToPath(uri);
        } catch (Exception e) {
            return tPath;
        }
    }

    /**
     * 获取bitmap字节大小
     *
     * @param btMap 图像bitmap
     * @return 字节大小
     */
    public static long GetBitmapSize(Bitmap btMap) {
        return btMap.getAllocationByteCount();
    }

    public interface JKPictureListener {

        /**
         * 图片设置完成后的回调函数
         *
         * @param btMap 图片加载完毕后的BitMap
         */
        void FinishSet(Bitmap btMap);
    }
}