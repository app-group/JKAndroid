package com.jkframework.animation;

public interface JKAnimationGroupListener {
	/**
	 * 动作执行完毕的回调函数
	 */
	void FinishAnimation();
}
