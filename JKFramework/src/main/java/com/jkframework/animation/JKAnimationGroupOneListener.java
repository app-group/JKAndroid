package com.jkframework.animation;

interface JKAnimationGroupOneListener {
	/**
	 * 单一动作执行完毕的回调函数
	 * @param nOrder 动作完毕执行编号(从1开始)
	 */
	void FinishOneAnimation(int nOrder);
}
