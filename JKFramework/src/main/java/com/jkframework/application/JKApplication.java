package com.jkframework.application;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;

public class JKApplication extends Application implements ViewModelStoreOwner {

    private ViewModelStore mAppViewModelStore;

    /**全局上下文引用*/
    private static Application application = null;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppViewModelStore = new ViewModelStore();
        application = this;
    }

    @NonNull
    @Override
    public ViewModelStore getViewModelStore() {
        return mAppViewModelStore;
    }

    /**
     * 获取application
     * @return application
     */
    public static Application getInstance()
    {
        return application;
    }
}

