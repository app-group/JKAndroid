package com.jkframework.bean;

import android.media.MediaPlayer;


public class JKMusicData
{
	/**播放器*/
	private MediaPlayer Player;
	/**初始化状态*/
	private boolean Init = false;

	public MediaPlayer getPlayer() {
		return Player;
	}

	public void setPlayer(MediaPlayer player) {
		Player = player;
	}

	public boolean isInit() {
		return Init;
	}

	public void setInit(boolean init) {
		Init = init;
	}
}