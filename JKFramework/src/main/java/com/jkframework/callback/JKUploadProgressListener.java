package com.jkframework.callback;

public interface JKUploadProgressListener {


	/**
	 * 请求数据进度
	 * @param lCurrentSize 当前上传大小
	 * @param lTotalSize 文件总大小
	 */
	void ReceiveProgress(long lCurrentSize, long lTotalSize);
}
