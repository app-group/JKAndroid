package com.jkframework.config;

import android.util.SparseArray;

import androidx.lifecycle.ViewModel;

public class JKDataBindingConfig {

    private final int layout;
    private final int vmVariableId;
    private final ViewModel stateViewModel;
    private final SparseArray bindingParams = new SparseArray();

    public JKDataBindingConfig(int layout, int vmVariableId, ViewModel stateViewModel) {
        this.layout = layout;
        this.vmVariableId = vmVariableId;
        this.stateViewModel = stateViewModel;
    }

    public int getLayout() {
        return layout;
    }

    public int getVmVariableId() {
        return vmVariableId;
    }

    public ViewModel getStateViewModel() {
        return stateViewModel;
    }

    public SparseArray getBindingParams() {
        return bindingParams;
    }

    public JKDataBindingConfig addBindingParam(int variableId, Object object) {
        if (bindingParams.get(variableId) == null) {
            bindingParams.put(variableId, object);
        }
        return this;
    }
}
