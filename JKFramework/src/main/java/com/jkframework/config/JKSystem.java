package com.jkframework.config;

import static android.Manifest.permission.WRITE_SETTINGS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Debug;
import android.os.Looper;
import android.provider.Settings;
import android.util.Size;
import android.view.DisplayCutout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.CleanUtils;
import com.blankj.utilcode.util.ClipboardUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.PhoneUtils;
import com.blankj.utilcode.util.ProcessUtils;
import com.blankj.utilcode.util.RomUtils;
import com.blankj.utilcode.util.SDCardUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.SizeUtils;
import com.blankj.utilcode.util.Utils;
import com.google.common.base.Optional;
import com.jkframework.algorithm.JKAnalysis;
import com.jkframework.algorithm.JKConvert;
import com.jkframework.algorithm.JKEncryption;
import com.jkframework.application.JKApplication;
import com.jkframework.debug.JKLog;
import com.jkframework.hardware.JKCamera;
import com.jkframework.manager.JKActivityManager;
import com.tbruyelle.rxpermissions3.RxPermissions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider;
import io.reactivex.rxjava3.functions.Consumer;

import static autodispose2.AutoDispose.autoDisposable;


public class JKSystem {

    /**
     * 获取屏幕宽高(会包含透明状态栏与透明虚拟按钮)
     *
     * @return 返回x为宽y为高
     */
    public static Size GetScreenSize() {
        return new Size(ScreenUtils.getScreenWidth(), ScreenUtils.getScreenHeight());
    }

    /**
     * 获取显示宽高(包括状态栏不包括虚拟按钮)
     *
     * @return 返回x为宽y为高
     */
    public static Size GetDisplaySize() {
        return new Size(ScreenUtils.getAppScreenWidth(), ScreenUtils.getAppScreenHeight());
    }

    public static int GetNotchHeight() {
        if (RomUtils.isHuawei()) {
            try {
                ClassLoader classLoader = JKActivityManager.GetCurrentActivity().getClassLoader();
                Class HwNotchSizeUtil = classLoader.loadClass("com.huawei.android.util.HwNotchSizeUtil");
                Method getNotchSize = HwNotchSizeUtil.getMethod("getNotchSize");
                int[] size = (int[]) getNotchSize.invoke(HwNotchSizeUtil);
                return size[1];
            } catch (Exception ignored) {
            }
        }

        if (RomUtils.isVivo()) {
            return JKConvert.DipToPx(27);   //固定值
        }

        if (RomUtils.isOppo()) {
            return 80;  //固定值
        }

        if (RomUtils.isXiaomi()) {
            int resourceId = JKActivityManager.GetCurrentActivity().getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                return JKActivityManager.GetCurrentActivity().getResources().getDimensionPixelSize(resourceId);
            }
        }

        //AndroidP
        if (Build.VERSION.SDK_INT >= 28) {
            if (JKActivityManager.GetCurrentActivity().getWindow().getDecorView().getRootWindowInsets() != null) {
                DisplayCutout safeArea = JKActivityManager.GetCurrentActivity().getWindow().getDecorView().getRootWindowInsets().getDisplayCutout();
                if (safeArea != null) {
                    return safeArea.getSafeInsetTop() + safeArea.getSafeInsetBottom();
                }
            }
        }

        return 0;
    }

    /**
     * 获取屏幕密度
     *
     * @return 屏幕密度
     */
    public static float GetDensity() {
        return ScreenUtils.getScreenDensity();
    }

    /**
     * 获取字体缩放比例
     *
     * @return 字体缩放比例
     */
    public static float GetFontSize() {
        return JKActivityManager.GetCurrentActivity().getResources().getConfiguration().fontScale;
    }

    /**
     * 获取手机当前状态栏高度
     *
     * @return > 0 success; <= 0 fail
     */
    public static int GetCurrentStatusBarHeight() {
        if (BarUtils.isStatusBarVisible(JKActivityManager.GetCurrentActivity()))
            return BarUtils.getStatusBarHeight();
        else
            return 0;
    }

    /**
     * 获取手机状态栏高度
     *
     * @return > 0 success; <= 0 fail
     */
    public static int GetStatusBarHeight() {
        return BarUtils.getStatusBarHeight();
    }

    /**
     * 获取标题栏高度
     *
     * @return 标题栏高度
     */
    public static int GetActionBarHeight() {
        return BarUtils.getActionBarHeight();
    }

    /**
     * 获取手机虚拟按钮高度
     *
     * @return > 0 success; <= 0 fail
     */
    public static int GetNavigationBarHeight() {
        return BarUtils.getNavBarHeight();
    }

    /**
     * 获取手机当前状态栏高度
     *
     * @return > 0 success; <= 0 fail
     */
    public static int GetCurrentNavigationBarHeight() {
        if (BarUtils.isNavBarVisible(JKActivityManager.GetCurrentActivity()))
            return BarUtils.getNavBarHeight();
        else
            return 0;
    }

    /**
     * 判断设备是否为平板
     *
     * @return 平板返回true
     */
    public static boolean IsTable() {
        return DeviceUtils.isTablet();
    }

    /**
     * 判断屏幕是否为自动亮度调解
     *
     * @return 当前为自动亮度调解返回true
     */
    public static boolean IsAutomicBrightness() {
        try {
            int mode = Settings.System.getInt(
                    Utils.getApp().getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE
            );
            return mode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        } catch (Settings.SettingNotFoundException e) {
            JKLog.ErrorLog("判断屏幕是否为自动亮度调解失败.原因为" + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 设置屏幕亮度类型
     *
     * @param bAuto true时为自动调解,否则为手工调解
     */
    public static void SetBrightnessType(boolean bAuto) {
        if (!Settings.System.canWrite(Utils.getApp())) {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + Utils.getApp().getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Utils.getApp().startActivity(intent);
        }
        Settings.System.putInt(
                Utils.getApp().getContentResolver(),
                Settings.System.SCREEN_BRIGHTNESS_MODE,
                bAuto ? Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
                        : Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
        );
    }

    /**
     * 设置屏幕亮度
     *
     * @param fPercent 屏幕亮度百分比(0~100)
     */
    public static void SetBrightness(float fPercent) {
        if (IsAutomicBrightness())
            SetBrightnessType(false);
        if (!Settings.System.canWrite(Utils.getApp())) {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + Utils.getApp().getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Utils.getApp().startActivity(intent);
        }
        ContentResolver resolver = Utils.getApp().getContentResolver();
        Settings.System.putInt(resolver, Settings.System.SCREEN_BRIGHTNESS, (int) (fPercent / 100 * 255));
        resolver.notifyChange(Settings.System.getUriFor("screen_brightness"), null);
    }

    /**
     * 获取当前屏幕亮度
     *
     * @return 屏幕当前亮度百分比, 失败为-1
     */
    public static float GetBrightness() {
        try {
            int nBrightness = Settings.System.getInt(
                    Utils.getApp().getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS
            );
            return nBrightness / (float) 255 * 100;
        } catch (Settings.SettingNotFoundException e) {
            JKLog.ErrorLog("获取当前屏幕亮度失败.原因为" + e.getMessage());
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * 获取Wifi状态
     *
     * @return 当前Wifi状态返回true
     */
    public static boolean IsWifi() {
        return NetworkUtils.isWifiConnected();
    }

    /**
     * 获取手机流量上网状态
     *
     * @return 当前流量上网状态返回true
     */
    public static boolean IsCelluar() {
        return NetworkUtils.isMobileData();
    }

    /**
     * 获取4G状态
     *
     * @return 当前4G状态返回true
     */
    public static boolean Is4G() {
        return NetworkUtils.is4G();
    }

    /**
     * 获取当前网络状态
     *
     * @return 当前有网状态返回true
     */
    public static boolean IsNetwork() {
        return NetworkUtils.isConnected();
    }

    /**
     * 获取当前网络是否可以访问
     *
     * @return 可以访问返回true
     */
    public static boolean IsNetworkAvailable() {
        return NetworkUtils.isAvailableByPing();
    }


    /**
     * 获取当前GPS状态
     *
     * @return 当前有GPS状态返回true
     */
    public static boolean IsGPS() {
        LocationManager lm = (LocationManager) Utils.getApp().getSystemService(Context.LOCATION_SERVICE);
        return lm != null
                && (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
        );
    }

    /**
     * 设置GPS状态
     */
    public static void SetGPS() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        Utils.getApp().startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    /**
     * 设置屏幕旋转方向
     *
     * @param nType 0为竖屏,1为横屏
     */
    public static void SetScreenOrientation(int nType) {
        if (nType == 0) {
            ScreenUtils.setPortrait(JKActivityManager.GetCurrentActivity());
        } else {
            ScreenUtils.setLandscape(JKActivityManager.GetCurrentActivity());
        }
    }

    /**
     * 保持屏幕常亮
     */
    public static void KeepScreen() {
        JKActivityManager.GetCurrentActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * 屏幕全屏
     */
    public static void FullScreen() {
        ScreenUtils.setFullScreen(JKActivityManager.GetCurrentActivity());
    }

    /**
     * 设置状态栏状态
     *
     * @param bShow 状态栏是否显示
     */
    public static void SetStatus(boolean bShow) {
        BarUtils.setStatusBarVisibility(JKActivityManager.GetCurrentActivity(), bShow);
    }

    /**
     * 设置状态栏文字色调
     *
     * @param bBlack 深色为true
     */
    public static void SetStatusBarMode(boolean bBlack) {
        //小米
        if (IsMiuiSystem()) {
            Class<? extends Window> clazz = JKActivityManager.GetCurrentActivity().getWindow().getClass();
            try {
                int darkModeFlag;
                @SuppressLint("PrivateApi") Class<?> layoutParams = Class.forName("android.view.MiuiWindowManager$LayoutParams");
                Field field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE");
                darkModeFlag = field.getInt(layoutParams);
                Method extraFlagField = clazz.getMethod("setExtraFlags", int.class, int.class);
                extraFlagField.invoke(JKActivityManager.GetCurrentActivity().getWindow(), bBlack ? darkModeFlag : 0, darkModeFlag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //Flyme
        if (IsMeizuSystem()) {
            try {
                WindowManager.LayoutParams lp = JKActivityManager.GetCurrentActivity().getWindow().getAttributes();
                Field darkFlag = WindowManager.LayoutParams.class
                        .getDeclaredField("MEIZU_FLAG_DARK_STATUS_BAR_ICON");
                Field meizuFlags = WindowManager.LayoutParams.class
                        .getDeclaredField("meizuFlags");
                darkFlag.setAccessible(true);
                meizuFlags.setAccessible(true);
                int bit = darkFlag.getInt(null);
                int value = meizuFlags.getInt(lp);
                if (bBlack) {
                    value |= bit;
                } else {
                    value &= ~bit;
                }
                meizuFlags.setInt(lp, value);
                JKActivityManager.GetCurrentActivity().getWindow().setAttributes(lp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        BarUtils.setStatusBarLightMode(JKActivityManager.GetCurrentActivity(), bBlack);
    }

    /**
     * 设置虚拟按钮颜色
     *
     * @param nColor 虚拟按钮颜色
     */
    public static void SetNavigationBarColor(@NonNull final Activity activity,int nColor) {
        BarUtils.setNavBarColor(activity, nColor);
    }

    /**
     * 设置虚拟按钮颜色
     *
     * @param nColor 虚拟按钮颜色
     */
    public static void SetNavigationBarColor(@NonNull final Window window,int nColor) {
        BarUtils.setNavBarColor(window, nColor);
    }

    /**
     * 获取android设备唯一标示
     * 需添加权限 {@code <uses-permission android:name="android.permission.READ_PHONE_STATE"/>}
     *
     * @return android唯一标示字符串
     */
    public static String GetGUID() {
        String tAndroidImei = GetAndroidImei();
        String tAndroidCode = GetAndroidCode();
        String tAndroidMac = GetMac();
        return JKEncryption.MD5_32(tAndroidImei + tAndroidCode + tAndroidMac);
    }

    /**
     * 获取android 机器码
     *
     * @return 获取失败返回"",否则返回机器码
     */
    public static String GetAndroidCode() {
        return Optional.fromNullable(DeviceUtils.getAndroidID()).or("");
    }

    /**
     * 获取android Imei号
     *
     * @return 获取失败返回"",否则返回Imei号
     */
    public static String GetAndroidImei() {
        if (ActivityCompat.checkSelfPermission(JKApplication.getInstance().getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "";
        }
        return Optional.fromNullable(PhoneUtils.getIMEI()).or("");
    }

    /**
     * 获取Mac地址
     *
     * @return Wlan Mac地址
     */
    public static String GetMac() {
        return JKAnalysis.toUpper(DeviceUtils.getMacAddress());
    }

    /**
     * 设置程序当前语言
     *
     * @param lcLanguage 语言对象
     */
    public static Context SetLanguage(Context context, Locale lcLanguage) {
        Configuration config = context.getResources().getConfiguration();//获得设置对象
        config.setLocale(lcLanguage);
        return context.createConfigurationContext(config);
    }

    /**
     * 设置默认的字体大小缩放设置
     */
    public static Context SetDefaultFontSize(Context context) {
        Configuration config = context.getResources().getConfiguration();//获得设置对象
        config.setToDefaults();
        return context.createConfigurationContext(config);
    }

    /**
     * 判断SD卡是否可以使用
     *
     * @return 可以使用返回true
     */
    public static boolean IsSDcardEnable() {
        return SDCardUtils.isSDCardEnableByEnvironment();
    }

    /**
     * 获取当前应用的内部版本号
     *
     * @return 应用的内部版本号
     */
    public static int GetVersionCode() {
        return AppUtils.getAppVersionCode();
    }

    /**
     * 获取当前应用的外部版本号
     *
     * @return 应用的外部版本号
     */
    public static String GetVersionString() {
        return AppUtils.getAppVersionName();
    }

    /**
     * 获取当前应用的包名
     *
     * @return 应用的内部版本号
     */
    public static String GetPackageName() {
        return AppUtils.getAppPackageName();
    }

    /**
     * 获取当前应用的程序名
     *
     * @return 应用程序名
     */
    public static String GetApplicationName() {
        return AppUtils.getAppName();
    }

    /**
     * 获取当前应用的icon资源
     *
     * @return icon资源
     */
    public static Drawable GetApplicationIcon() {
        return AppUtils.getAppIcon();
    }

    /**
     * 判断程序是否为正常的apk安装包
     *
     * @param tPath 程序SD卡路径地址
     * @return true表示正常
     */
    public static boolean IsPackage(String tPath) {
        PackageManager pmManager = JKApplication.getInstance().getApplicationContext().getPackageManager();
        PackageInfo piInfo = pmManager.getPackageArchiveInfo(tPath, PackageManager.GET_ACTIVITIES);
        return piInfo != null;
    }

    /**
     * 安装apk包
     *
     * @param tPath apk包路径地址
     */
    public static void InstallPackage(String tPath) {
        AppUtils.installApp(tPath);

    }

    /**
     * 卸载apk包
     *
     * @param tPackage apk包名
     */
    public static void UninstallPackage(String tPackage) {
        AppUtils.uninstallApp(tPackage);
    }

    /**
     * 获取手机电量百分比
     *
     * @return 手机电量
     */
    public static int GetBatteryPer() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = JKApplication.getInstance().getApplicationContext().registerReceiver(null, ifilter);
        if (batteryStatus != null)
            return batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        else
            return -1;
    }

    /**
     * 检测手机是否Root
     *
     * @return Root返回true, 否则返回false
     */
    public static boolean IsRoot() {
        return DeviceUtils.isDeviceRooted();
    }

    /**
     * 获取IP地址
     *
     * @return 返回IP地址字符串, 若没有网络返回""
     */
    public static String GetLocalIP() {
        return NetworkUtils.getIPAddress(true);
    }

    /**
     * 打开软键盘
     *
     * @param vView 打开软键盘的视图
     */
    public static void OpenKeyboard(View vView) {
        vView.clearFocus();
        KeyboardUtils.showSoftInput(vView);
    }

    /**
     * 关闭软键盘
     *
     * @param vView 关闭软键盘的视图
     */
    public static void CloseKeyboard(View vView) {
        vView.clearFocus();
        KeyboardUtils.hideSoftInput(vView);
    }

    /**
     * 关闭软键盘
     */
    public static void CloseKeyboard() {
        KeyboardUtils.hideSoftInput(JKActivityManager.GetCurrentActivity());
    }

//    /**
//     * 获取手机号
//     * @return 返回手机号码(没有号码返回空字符串)
//     */
//    public String GetUserNumber() {
//        TelephonyManager telephonyManager = (TelephonyManager) JKApplication.getInstance().getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//        if (ActivityCompat.checkSelfPermission(JKApplication.getInstance().getApplicationContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            return "";
//        }
//        String tNumber = telephonyManager.getLine1Number();
//        return Optional.fromNullable(tNumber).or("");
//	}

    /**
     * 打开手电筒
     */
    public static void OpenFlashLight() {
        JKCamera.GetInstance().OpenFlashLight();
    }

    /**
     * 关闭手电筒
     */
    public static void CloseFlashLight() {
        JKCamera.GetInstance().CloseFlashLight();
    }

    /**
     * 禁止程序触发锁屏
     */
    public static void DissmissLockScreen() {
        JKActivityManager.GetCurrentActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    }

    /**
     * 设置程序休眠时间
     *
     * @param nTime 休眠时间毫秒数
     */
    @RequiresPermission(WRITE_SETTINGS)
    public static void SetSleepTime(Fragment fragment, final int nTime) {
        ScreenUtils.setSleepDuration(nTime);
    }

    /**
     * 获取当前程序进程名
     *
     * @return 如果找不到进程则返回""
     */
    public static String GetProcessName() {
        return ProcessUtils.getCurrentProcessName();
    }

    /**
     * 获取当前SDK版本
     *
     * @return SDK整型版本
     */
    public static int GetSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    /**
     * 获取当前程序是否运行
     *
     * @return true表示运行中
     */
    public static boolean IsRunning() {
        return AppUtils.isAppForeground();
    }

    /**
     * 当前程序是否正在显示
     *
     * @return true表示为栈顶程序
     */
    public static boolean IsTopProgress() {
        return ProcessUtils.getForegroundProcessName().equals(GetPackageName());
    }

    /**
     * 返回桌面
     */
    public static void OnHome() {
        ActivityUtils.startHomeActivity();
    }

    /**
     * 返回应用
     */
    public static void ReturnApp() {
        ActivityManager mActivityManager = (ActivityManager) JKApplication.getInstance().getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);
        try {
            if (mActivityManager != null) {
                mActivityManager.moveTaskToFront(JKActivityManager.GetCurrentActivity().getTaskId(), 0);
            }
        } catch (Exception ignored) {
            AppUtils.relaunchApp();
        }
    }

    /**
     * 用系统浏览器打开网址
     *
     * @param tUrl 网址地址
     */
    public static void OpenBrowser(String tUrl) {
        Uri uri = Uri.parse(tUrl);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        JKActivityManager.GetCurrentActivity().startActivity(intent);
    }

    /**
     * 启动应用
     *
     * @param tPackage 应用包名
     */
    public static void OpenApk(String tPackage) {
        AppUtils.launchApp(tPackage);
    }

    /**
     * 清除应用所有数据
     */
    public static void CleanData() {
        CleanUtils.cleanInternalCache();
        CleanUtils.cleanInternalFiles();
        CleanUtils.cleanInternalDbs();
        CleanUtils.cleanInternalSp();
        CleanUtils.cleanExternalCache();
    }

    /**
     * 清除应用缓存
     */
    public static void CleanCache() {
        CleanUtils.cleanInternalCache();
        CleanUtils.cleanExternalCache();
    }

    /**
     * 获取系统缓存使用大小(单位字节)
     *
     * @return 系统缓存使用大小
     */
    public static long GetCacheSize() {
        String str1 = "/proc/meminfo";// 系统内存信息文件
        String str2;
        String[] arrayOfString;
        long initial_memory;
        try {
            FileReader localFileReader = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(localFileReader, 8192);
            localBufferedReader.readLine();
            localBufferedReader.readLine();
            localBufferedReader.readLine();
            str2 = localBufferedReader.readLine();// 读取meminfo第4行，缓存大小
            arrayOfString = str2.split("\\s+");
            switch (arrayOfString[2].toUpperCase(Locale.getDefault())) {
                case "KB":
                    initial_memory = JKConvert.toLong(arrayOfString[1]) * 1024;
                    break;
                case "MB":
                    initial_memory = JKConvert.toLong(arrayOfString[1]) * 1024 * 1024;
                    break;
                case "GB":
                    initial_memory = JKConvert.toLong(arrayOfString[1]) * 1024 * 1024 * 1024;
                    break;
                case "TB":
                    initial_memory = JKConvert.toLong(arrayOfString[1]) * 1024 * 1024 * 1024 * 1024;
                    break;
                default:
                    initial_memory = JKConvert.toLong(arrayOfString[1]);
                    break;
            }
            localBufferedReader.close();
            return initial_memory;
        } catch (IOException e) {
            return 0;
        }
    }

    /**
     * 获取当前应用内存剩余使用空间(单位字节)
     *
     * @return 获取当前应用的内存空间
     */
    public static long GetRemainMemorySize() {
        ActivityManager mActivityManager = (ActivityManager) JKApplication.getInstance().getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        //获得系统里正在运行的所有进程 
        List<RunningAppProcessInfo> runningAppProcessesList;
        if (mActivityManager != null) {
            runningAppProcessesList = mActivityManager.getRunningAppProcesses();
            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcessesList) {
                String processName = runningAppProcessInfo.processName;
                if (processName.equals(GetPackageName())) {
                    int[] pids = new int[]{runningAppProcessInfo.pid};
                    Debug.MemoryInfo[] memoryInfo = mActivityManager.getProcessMemoryInfo(pids);
                    return memoryInfo[0].dalvikPrivateDirty * 1024L;
                }
            }
        }
        return 0;
    }

//    /**
//     * 是否低内存状态
//     * @return true表示低内存状态
//     */
//    public static boolean IsLowMemory()
//    {
//    	MemoryInfo miInfo = new MemoryInfo();
//		((ActivityManager) JKApplication.getInstance().getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE)).getMemoryInfo(miInfo);
//		return miInfo.lowMemory;
//    }

    /**
     * 查看是否在主线程
     *
     * @return true表示在主线程
     */
    public static boolean IsMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /**
     * 查看是否为小米系统
     *
     * @return true表示为小米设备
     */
    public static boolean IsMiuiSystem() {
        return RomUtils.isXiaomi();
    }

    /**
     * 查看是否为魅族系统
     *
     * @return true表示为魅族设备
     */
    public static boolean IsMeizuSystem() {
        return RomUtils.isMeizu();
    }

    /**
     * 判断Cpu架构
     *
     * @return 0为32位系统, 1为64位系统
     */
    public static int GetCpuType() {
        /*判断系统*/
        try {
            @SuppressLint("PrivateApi") Class<?> clazz = Class.forName("android.os.SystemProperties");
            Method get = clazz.getMethod("get", String.class, String.class);
            String tValue = (String) (get.invoke(clazz, "ro.product.cpu.abilist64", ""));
            if (tValue != null && tValue.length() > 0)
                return 1;
        } catch (Exception ignored) {

        }
        /*判断Cpu信息*/
        File cpuInfo = new File("/proc/cpuinfo");
        if (cpuInfo.exists()) {
            InputStream inputStream = null;
            BufferedReader bufferedReader = null;
            try {
                inputStream = new FileInputStream(cpuInfo);
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 512);
                String line = bufferedReader.readLine();
                if (line != null && line.length() > 0 && line.toLowerCase(Locale.US).contains("arch64")) {
                    return 1;
                }
            } catch (Throwable ignored) {
            } finally {
                try {
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        /*判断lib版本*/
        File libcFile = new File("/system/lib64/libc.so");
        if (libcFile.exists()) {
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(libcFile);
                byte[] tempBuffer = new byte[16];
                int count = inputStream.read(tempBuffer, 0, 16);
                if (count == 16) {
                    if (tempBuffer[4] == 2) {
                        return 1;
                    }
                }
            } catch (Throwable ignored) {
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return 0;
    }

    /**
     * 文本复制到剪切板
     *
     * @param content 复制的文本内容
     */
    public static void Copy(String content) {
        ClipboardUtils.copyText(content);
    }

    /**
     * 文本粘贴剪切板内容
     *
     * @return 粘贴内容
     */
    public static String Pause() {
        return ClipboardUtils.getText().toString();
    }

    /**
     * 将相关文件通知程序扫描
     *
     * @param tPath 文件路径
     */
    public static void AddToScanfile(String tPath) {
        FileUtils.notifySystemToScan(tPath);
    }
}