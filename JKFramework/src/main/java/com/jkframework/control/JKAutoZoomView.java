package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import io.reactivex.rxjava3.annotations.Nullable;

public class JKAutoZoomView extends FrameLayout {
    private float childrenScale = 1.0f;
    public float getChildrenScale(){
        return childrenScale;
    }

    public void setChildrenScale(float scale) {
        if (scale != childrenScale) {
            childrenScale = scale;
            requestLayout();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getChildCount() > 0) {
            int maxWidth = -1;
            int maxHeight = -1;
            //获取内容的最大高度和宽度
            for (int index = 0; index < getChildCount(); ++index) {
                View obj = getChildAt(index);
                obj.measure(widthMeasureSpec,heightMeasureSpec);
                maxWidth = Math.max(obj.getMeasuredWidth(), maxWidth);
                maxHeight = Math.max(obj.getMeasuredHeight(), maxHeight);
            }

            //获取自己的预设宽度，使用宽度比较
            int widthSize = MeasureSpec.getSize(widthMeasureSpec);
            int heightSize = 0;

            //默认使用子节点的大小
            if (widthSize == 0) {
                //如果预设尺寸为0，则使用子节点的尺寸*缩放比，得到自己的尺寸
                widthSize = (int) (maxWidth * childrenScale);
            } else {
                //如果预设尺寸不为0，则使用预设尺寸，计算缩放比，使用预设尺寸作为自己的尺寸
                childrenScale = widthSize / (float)maxWidth;
            }
            heightSize = (int) (maxHeight * childrenScale);

            for (int index = 0; index < getChildCount(); ++index) {
                View obj = getChildAt(index);
                obj.setScaleX(childrenScale);
                obj.setScaleY(childrenScale);
                obj.setPivotX(0);
                obj.setPivotY(0);
            }
            this.setMeasuredDimension(widthSize,heightSize);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public JKAutoZoomView(Context context) {
        super(context);
    }

    public JKAutoZoomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public JKAutoZoomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
