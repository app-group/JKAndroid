package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class JKBottomNavigationView extends BottomNavigationView {

    public JKBottomNavigationView(Context context) {
        super(context);
    }

    public JKBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}