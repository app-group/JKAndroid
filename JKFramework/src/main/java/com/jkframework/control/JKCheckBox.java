package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatCheckBox;

public class JKCheckBox extends AppCompatCheckBox {


	public JKCheckBox(Context context) {
		super(context);
	}

	public JKCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JKCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
}