package com.jkframework.control;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Locale;

public class JKDatePicker extends DatePickerDialog {

    public JKDatePicker() {
        super();
    }


    public void initialize(OnDateTimeSetListener callBack, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute, int second) {
        Calendar cal = Calendar.getInstance(getTimeZone());
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear - 1);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        this.initialize(new OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                        callBack.onDateSet(year, monthOfYear+1, dayOfMonth, hourOfDay, minute, second);
                    }
                }, hourOfDay, minute, second, true);
                timePickerDialog.enableSeconds(true);
                timePickerDialog.show(getParentFragmentManager(), "JKDatePicker");
            }
        }, cal);
    }

    public interface OnDateTimeSetListener {

        void onDateSet(int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute, int second);
    }
}