package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import androidx.drawerlayout.widget.DrawerLayout;

public class JKDrawerLayout extends DrawerLayout {

	public JKDrawerLayout(Context context) {
		super(context);
	}

	public JKDrawerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JKDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}