package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.drawerlayout.widget.DrawerLayout;

public class JKEditText extends AppCompatEditText {

    public JKEditText(Context context) {
        super(context);
    }

    public JKEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public JKEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}