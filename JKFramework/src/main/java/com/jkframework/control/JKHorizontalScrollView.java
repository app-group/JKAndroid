package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

public class JKHorizontalScrollView extends HorizontalScrollView {

	public JKHorizontalScrollView(Context context) {
		super(context);
	}

	public JKHorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JKHorizontalScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
}