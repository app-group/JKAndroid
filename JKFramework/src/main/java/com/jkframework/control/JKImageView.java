package com.jkframework.control;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.jkframework.algorithm.JKPicture;
import com.jkframework.callback.JKImageViewListener;
import com.jkframework.config.JKSystem;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;


public class JKImageView extends AppCompatImageView {

	/**图片适配模块*/
	protected  RequestOptions roOptions;
	/**监听成员*/
	private JKImageViewListener m_UpdateLisnter;
	/**LevelList资源数组*/
    private ArrayList<Integer> a_nLevelList = new ArrayList<>();

	public JKImageView(Context context) {
		this(context,null);
	}

	public JKImageView(Context context, AttributeSet attrs) {
		this(context, attrs,0);
	}

	public JKImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context,attrs,defStyle);
		InitBitmapConfig();
	}

	/**
	 * 设置回调函数
	 * @param l	初始化回调变量
	 */
	public void SetUpdateListener(JKImageViewListener l)
	{
		m_UpdateLisnter = l;
	}


    /**
     * 设置图片组
     * @param a_nResourceId 图片组id
     */
    public void SetImageLevelList(ArrayList<Integer> a_nResourceId){
        a_nLevelList = a_nResourceId;
        if (a_nResourceId.size() > 0)
            SetImageResource(a_nResourceId.get(0));
    }

	public void SetImageResource(int resId){
		Glide.with(this).clear(this);
		super.setImageResource(resId);
        if (m_UpdateLisnter != null)
            m_UpdateLisnter.LoadingProgress(100);
		if (m_UpdateLisnter != null)
			m_UpdateLisnter.FinishLoad(true);
	}

	public void SetImageURI(Uri uri){
		Glide.with(this).clear(this);
		super.setImageURI(uri);
        if (m_UpdateLisnter != null && uri != null)
            m_UpdateLisnter.LoadingProgress(100);
		if (m_UpdateLisnter != null)
			m_UpdateLisnter.FinishLoad(uri != null);
	}

	/**
	 * 设置图片
	 * @param bmBitmap 图片bitmap
	 */
	public void SetBitmap(Bitmap bmBitmap){
		Glide.with(this).clear(this);
		super.setImageBitmap(bmBitmap);
        if (m_UpdateLisnter != null && bmBitmap != null)
            m_UpdateLisnter.LoadingProgress(100);
		if (m_UpdateLisnter != null)
			m_UpdateLisnter.FinishLoad(bmBitmap != null);
	}

	/**
	 * 设置图片路径
	 * @param tPath 图片路径
	 */
	public void SetImagePath(String tPath)
	{
		Glide.with(this).clear(this);
		Bitmap btTmp = JKPicture.LoadBitmap(tPath);
        super.setImageBitmap(btTmp);
        if (m_UpdateLisnter != null && btTmp != null)
            m_UpdateLisnter.LoadingProgress(100);
        if (m_UpdateLisnter != null)
            m_UpdateLisnter.FinishLoad(btTmp != null);
	}

	/**
	 * 设置图片路径
	 * @param tPath 图片路径
	 */
	public void SetAssetsImagePath(String tPath)
	{
		Glide.with(this).clear(this);
		Bitmap btTmp = JKPicture.LoadAssetsBitmap(tPath);
        super.setImageBitmap(btTmp);
        if (m_UpdateLisnter != null && btTmp != null)
            m_UpdateLisnter.LoadingProgress(100);
        if (m_UpdateLisnter != null)
            m_UpdateLisnter.FinishLoad(btTmp != null);
	}

	public void SetImageLevel(int nLevel)
    {
        if (a_nLevelList.size() > nLevel) {
            Glide.with(this).clear(this);
            super.setImageResource(a_nLevelList.get(nLevel));
            if (m_UpdateLisnter != null)
                m_UpdateLisnter.LoadingProgress(100);
            if (m_UpdateLisnter != null)
                m_UpdateLisnter.FinishLoad(true);
        }
    }

	/**
	 * 异步设置图片http
	 * @param tUrl  图片url地址
	 */
	public void SetImageHttp(String tUrl)
	{
        LoaderBitmapAsync(tUrl);
	}

	/**
	 * 异步设置SD卡图片
	 * @param tPath  图片路径地址
	 */
	public void SetImagePathAsync(String tPath)
	{
		final String tFullPath = "file:///" + (tPath.indexOf("/") == 0 ? tPath.substring(1) : tPath);
        LoaderBitmapAsync(tFullPath);
	}

	/**
	 * 异步设置assets图片
	 * @param tPath  图片路径地址
	 */
	public void SetAssetsImagePathAsync(String tPath)
	{
		final String tFullPath = "file:///android_asset/" + tPath;
        LoaderBitmapAsync(tFullPath);
	}

	/**
	 * 异步设置图片资源
	 * @param resId  图片资源ID
	 */
	public void SetImageResourceAsync(int resId){

		final String tFullPath = "android.resource://" + JKSystem.GetPackageName() + "/drawable/" + resId;
	    LoaderBitmapAsync(tFullPath);
	}

	/**
	 * 智能识别设置图片
	 * @param tPath 支持SD卡图片,http图片,assets图片
	 */
	public void SetImageAsync(String tPath)
	{
		if (tPath == null)
			tPath = "null";
		if (tPath.startsWith("http"))
			SetImageHttp(tPath);
		else if (tPath.startsWith("/"))
			SetImagePathAsync(tPath);
		else
			SetAssetsImagePathAsync(tPath);
	}

	/**
	 * 回收处理图片
	 */
	public void Destroy()
	{
		Glide.with(this).clear(this);
		setImageBitmap(null);
	}

	/**
	 * 设置控件锚点X轴
	 * @param fPivotX [0,1]范围
	 */
	public void SetPivotX(float fPivotX)
	{
		setPivotX(fPivotX * getWidth());
	}

	/**
	 * 设置控件锚点Y轴
	 * @param fPivotY [0,1]范围
	 */
	public void SetPivotY(float fPivotY)
	{
		setPivotY(fPivotY * getHeight());
	}

    /**
     * 设置加载中时显示的图片
     * @param nResourceID 图片资源ID
     */
    public void SetLoadingImage(int nResourceID)
    {
		roOptions = roOptions.placeholder(nResourceID);
    }

    /**
     * 设置加载中时显示的图片
     * @param bmp 图片
     */
    public void SetLoadingImage(Bitmap bmp)
    {
		roOptions = roOptions.placeholder(new BitmapDrawable(getContext().getResources(),bmp));
    }

    /**
     * 设置失败时显示的图片
     * @param nResourceID 图片资源ID
     */
    public void SetFailImage(int nResourceID)
    {
		roOptions = roOptions.error(nResourceID).fallback(nResourceID);

    }

    /**
     * 设置失败时显示的图片
     * @param bmp 图片
     */
    public void SetFailImage(Bitmap bmp)
    {
		roOptions = roOptions.error(new BitmapDrawable(getContext().getResources(),bmp)).fallback(new BitmapDrawable(getContext().getResources(),bmp));
    }

    /**
     * 加载图片
     * @param tUrl 图片image-loader地址
     */
    private void LoaderBitmapAsync(String tUrl)
    {
		Glide.with(this).load(tUrl).apply(roOptions).transition(DrawableTransitionOptions.withCrossFade(1)).listener(new RequestListener<Drawable>() {
			@Override
			public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
				if (m_UpdateLisnter != null)
					m_UpdateLisnter.FinishLoad(false);
				return false;
			}

			@Override
			public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                if (m_UpdateLisnter != null) {
                    m_UpdateLisnter.LoadingProgress(100);
                }
				if (m_UpdateLisnter != null)
					m_UpdateLisnter.FinishLoad(true);
				return false;
			}
		}).into(this);
    }

	/**
	 * 初始化图片加载配置
	 */
	private void InitBitmapConfig()
	{
		roOptions = new RequestOptions().skipMemoryCache(false);
	}
}