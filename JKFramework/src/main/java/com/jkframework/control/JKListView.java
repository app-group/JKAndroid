package com.jkframework.control;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

public class JKListView extends ListView {

    /**表头界面*/
    private View vHeaderView = null;

    public JKListView(Context context) {
        this(context,null);
    }

    public JKListView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public JKListView(Context context, AttributeSet attrs, int defStyle) {
        super(context,attrs,defStyle);
        Init();
	}

    /**
     * 添加分割线
     * @param  nColor 分割线颜色
     * @param  nHeight 分割线高度(像素)
     */
    public void AddItemDecoration(int nColor,int nHeight)
    {
        setDivider(new ColorDrawable(nColor));
        setDividerHeight(nHeight);
    }

    /**
     * 添加表头
     * @param  vView 表头界面
     */
    public void SetHeaderView(View vView)
    {
        if (vHeaderView != null)
            removeHeaderView(vHeaderView);
        vHeaderView = vView;
        addHeaderView(vView);
    }


    /**
     * 初始化
     */
	private void Init()
	{
		setDivider(null);
	}
}