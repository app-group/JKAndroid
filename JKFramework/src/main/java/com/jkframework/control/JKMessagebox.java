package com.jkframework.control;

import android.content.Context;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnCancelListener;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;


public class JKMessagebox {
    /**
     * 消息提示框
     *
     * @param hContext 上下文指针
     * @param tTitle   提示框标题
     * @param tMessage 提示框内容
     * @param tOK      提示框确认按钮文字
     * @return 返回对话框对象
     */
    public static BasePopupView Messagebox(Context hContext, String tTitle, String tMessage, String tOK) {
        return new XPopup.Builder(hContext).asConfirm(tTitle, tMessage,
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {

                    }
                })
                .setConfirmText(tOK)
                .show();
    }

    /**
     * 消息提示框
     *
     * @param hContext 上下文指针
     * @param tMessage 提示框内容
     * @param tOK      提示框确认按钮文字
     * @return 返回对话框对象
     */
    public static BasePopupView Messagebox(Context hContext, String tMessage, String tOK) {
        return new XPopup.Builder(hContext).asConfirm(null, tMessage,
                new OnConfirmListener() {
                    @Override
                    public void onConfirm() {

                    }
                })
                .setConfirmText(tOK)
                .show();
    }

    /**
     * 消息提示框
     *
     * @param hContext 上下文指针
     * @param tTitle   提示框标题
     * @param tMessage 提示框内容
     * @param tOK      提示框确认按钮文字
     * @param l        确认后执行的命令
     * @return 返回对话框对象
     */
    public static BasePopupView Messagebox(Context hContext, String tTitle, String tMessage, String tOK, OnConfirmListener l) {
        return new XPopup.Builder(hContext).asConfirm(tTitle, tMessage, l)
                .setConfirmText(tOK)
                .show();
    }

    /**
     * 消息提示框
     *
     * @param hContext 上下文指针
     * @param tTitle   消息提示框标题
     * @param tMessage 提示框内容
     * @param tOK      提示框确认按钮文字
     * @param l        确认后执行的命令
     * @param tCancel  提示框取消的文字
     * @return 返回对话框对象
     */
    public static BasePopupView Messagebox(Context hContext, String tTitle, String tMessage, String tOK, OnConfirmListener l, String tCancel) {
        return new XPopup.Builder(hContext).asConfirm(tTitle, tMessage, l, new OnCancelListener() {
            @Override
            public void onCancel() {

            }
        })
                .setConfirmText(tOK)
                .setCancelText(tCancel)
                .show();
    }


    /**
     * 消息提示框
     *
     * @param hContext 上下文指针
     * @param tTitle   消息提示框标题
     * @param tMessage 提示框内容
     * @param tOK      提示框确认按钮文字
     * @param l        确认后执行的命令
     * @param tCancel  提示框取消的文字
     * @param l2       取消后执行的命令
     * @return 返回对话框对象
     */
    public static BasePopupView Messagebox(Context hContext, String tTitle, String tMessage, String tOK, OnConfirmListener l, String tCancel, OnCancelListener l2) {
        return new XPopup.Builder(hContext).asConfirm(tTitle, tMessage, l, l2)
                .setConfirmText(tOK)
                .setCancelText(tCancel)
                .show();
    }

    /**
     * 消息输入框
     *
     * @param hContext 上下文指针
     * @param tTitle   提示框标题
     * @param tOK      提示框确认按钮文字
     * @param l        确认后执行的命令
     * @return 返回对话框对象
     */
    public static BasePopupView Inputbox(Context hContext, String tTitle, String tOK, OnInputConfirmListener l) {
        return new XPopup.Builder(hContext).asInputConfirm(tTitle, null, l)
                .setConfirmText(tOK)
                .show();
    }

    /**
     * 消息选择框
     *
     * @param hContext 上下文指针
     * @param tTitle   选择框标题
     * @param a_tItems 选择框内容数组
     * @param l        选择框监听事件
     * @return 返回对话框对象
     */
    public static BasePopupView Selectbox(Context hContext, String tTitle, String[] a_tItems, OnSelectListener l) {
        return new XPopup.Builder(hContext)
                //.maxWidth(600)
                .asCenterList(tTitle, a_tItems, l)
                .show();
    }

    /**
     * 消息选择框
     *
     * @param hContext 上下文指针
     * @param a_tItems 选择框内容数组
     * @param l        选择框监听事件
     * @return 返回对话框对象
     */
    public static BasePopupView Selectbox(Context hContext, String[] a_tItems, OnSelectListener l) {
        return new XPopup.Builder(hContext)
                //.maxWidth(600)
                .asCenterList(null, a_tItems, l)
                .show();
    }
}