package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatRadioButton;

public class JKRadioButton extends AppCompatRadioButton {


	public JKRadioButton(Context context) {
		super(context);
	}

	public JKRadioButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JKRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
}