package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.RecyclerView;

public class JKRecyclerView extends RecyclerView {

	public JKRecyclerView(Context context) {
		this(context,null);
	}

	public JKRecyclerView(Context context, AttributeSet attrs) {
		this(context, attrs,0);
	}

	public JKRecyclerView(Context context, AttributeSet attrs, int defStyle) {
		super(context,attrs,defStyle);
        Init();
	}

    private void Init()
    {
    }
}