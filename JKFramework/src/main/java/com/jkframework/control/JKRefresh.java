package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;

public class JKRefresh extends SmartRefreshLayout {

	public JKRefresh(Context context) {
		this(context,null);
	}

	public JKRefresh(Context context, AttributeSet attrs) {
		super(context, attrs);
        Init();
	}

    private void Init()
    {
        setDragRate(1);
        setEnableOverScrollBounce(false);
//        setEnableAutoLoadMore(false);
        setEnableScrollContentWhenLoaded(false);
        setEnableLoadMoreWhenContentNotFull(false);
        setHeaderMaxDragRate(1.5f);
        setDisableContentWhenRefresh(true);
        setReboundDuration(500);
    }
}