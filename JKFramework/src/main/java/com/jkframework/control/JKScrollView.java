package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.widget.NestedScrollView;

public class JKScrollView extends NestedScrollView {

	public JKScrollView(Context context) {
		super(context);
	}

	public JKScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JKScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

//	@Override
//	public void requestChildFocus(View child, View focused) {
//	}

}