package com.jkframework.control;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

public class JKTabLayout extends TabLayout {

    private boolean needFix = true;

    public JKTabLayout(Context context) {
        super(context);
    }

    public JKTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed,l,t,r,b);
        if (changed)
            needFix = true;
        if (getTabCount() > 0)
        {
            int widthTotal = 0;
            for (int i=0; i<getTabCount(); i++)
            {
                widthTotal += Objects.requireNonNull(getTabAt(i)).view.getWidth();
            }
            if (getWidth() > widthTotal)
            {
                setTabMode(MODE_FIXED);
            }
            else {
                if (needFix || getTabMode() == MODE_AUTO)
                {
                    setTabMode(MODE_SCROLLABLE);
                    needFix = false;
                }
            }
        }
    }

    public void FixMode()
    {
        needFix = true;
    }
}