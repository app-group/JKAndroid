package com.jkframework.control;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

import com.cjj.jkframework.R;

public class JKTextView extends AppCompatTextView {

    /**
     * 显示最大行数
     */
    private int nMaxLines = -1;
    /**
     * 文字风格
     */
    private int nTextStyle = 0;

    public JKTextView(Context context) {
        this(context, null);
    }


    public JKTextView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public JKTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray taArray = context.obtainStyledAttributes(attrs, R.styleable.JKTextView);
        this.nTextStyle = taArray.getInt(R.styleable.JKTextView_textStyle, 0);
        this.nMaxLines = taArray.getInt(R.styleable.JKTextView_maxLines, -1);
        taArray.recycle();

        Init();
    }

    /**
     * 初始化属性
     */
    private void Init() {
        if ((nTextStyle & 4) != 0)
            getPaint().setUnderlineText(true);
        nTextStyle = nTextStyle - (nTextStyle & 4);
        setTypeface(getTypeface(), nTextStyle);
        if (nMaxLines >= 0)
            setMaxLines(nMaxLines);
    }

//    @Override
//    public void setSingleLine() {
//        setMaxLines(1);
//    }
}