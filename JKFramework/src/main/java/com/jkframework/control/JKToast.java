package com.jkframework.control;


import com.blankj.utilcode.util.ToastUtils;



public class JKToast
{	
	public static void ShowLongToast(String text) {
		//Toast.makeText(JKApplication.getInstance().getApplicationContext(), text, Toast.LENGTH_LONG).show();
		ToastUtils.showLong(text);
	}

	public static void ShowShortToast(String text) {
		//Toast.makeText(JKApplication.getInstance().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
		ToastUtils.showShort(text);
	}

	public static void ShowLongToast(int stringRes) {
		//ShowLongToast(JKApplication.getInstance().getApplicationContext().getString(stringRes));
		ToastUtils.showShort(stringRes);
	}

	public static void ShowShortToast(int stringRes) {
		//ShowShortToast(JKApplication.getInstance().getApplicationContext().getString(stringRes));
		ToastUtils.showShort(stringRes);
	}
}