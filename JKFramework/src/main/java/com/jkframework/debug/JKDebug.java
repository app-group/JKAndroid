package com.jkframework.debug;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.Utils;
import com.jkframework.application.JKApplication;
import com.jkframework.config.JKPreferences;

public class JKDebug {

    /**
     * Debug版本号(true为调试版)
     */
    public static boolean debug = true;
    /**
     * 反射类
     */
    public static String tReflect = "";

    /**
     * 起始初始化执行
     *
     * @param bDebugStatus debug状态
     * @param tReflect     崩溃反射类
     */
    public static void Init(boolean bDebugStatus,String tReflect) {
        debug = bDebugStatus;
        //全局Context记录
        Utils.init(JKApplication.getInstance());
        //崩溃处理框初始化
        JKDebug.tReflect = tReflect;
        ActivityUtils.finishAllActivities();

//		try
//		{
//			System.loadLibrary("JKFramework");
//            Init(nDebugStatus);
//		}
//        catch (UnsatisfiedLinkError ignored)
//        {
//
//        }
    }

    /**
     * 激活回收状态
     */
    public static void Activation() {
    }

//    /**
//     * 初始化NDK
//     * @param nDebug 0为正式版,1为测试版
//     */
//    public static native void Init(int nDebug);
}
