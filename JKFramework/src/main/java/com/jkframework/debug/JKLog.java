package com.jkframework.debug;

import android.util.Log;

import com.jkframework.algorithm.JKDate;
import com.jkframework.algorithm.JKFile;

public class JKLog {

    /**
     * 错误信息默认地址
     */
    static private final String ERROR_PATH = JKFile.GetPublicPath() + "/JKError/Error.txt";
    /**
     * 默认log标签
     */
    private static final String tDebugtTag = "JKDebug";

    /**
     * debug内容
     *
     * @param tMsg log内容
     */
    public static void d(String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.d(tDebugtTag, tMsg);
        }
    }

    /**
     * debug内容
     *
     * @param tTag debug标签
     * @param tMsg log内容
     */
    public static void d(String tTag, String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.d(tTag, tMsg);
        }
    }

    /**
     * info内容
     *
     * @param tMsg log内容
     */
    public static void i(String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.i(tDebugtTag, tMsg);
        }
    }

    /**
     * info内容
     *
     * @param tTag info标签
     * @param tMsg log内容
     */
    public static void i(String tTag, String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.i(tTag, tMsg);
        }
    }

    /**
     * warning内容
     *
     * @param tMsg log内容
     */
    public static void w(String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.w(tDebugtTag, tMsg);
        }
    }

    /**
     * warning内容
     *
     * @param tTag warning标签
     * @param tMsg log内容
     */
    public static void w(String tTag, String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.w(tTag, tMsg);
        }
    }

    /**
     * error内容
     *
     * @param tMsg log内容
     */
    public static void e(String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.e(tDebugtTag, tMsg);
        }
    }

    /**
     * error内容
     *
     * @param tMsg log内容
     */
    public static void e(String tMsg,Throwable tr) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.e(tDebugtTag, tMsg,tr);
        }
    }

    /**
     * error内容
     *
     * @param tTag error标签
     * @param tMsg log内容
     */
    public static void e(String tTag, String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.e(tTag, tMsg);
        }
    }

    /**
     * verbose内容
     *
     * @param tMsg log内容
     */
    public static void v(String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.v(tDebugtTag, tMsg);
        }
    }

    /**
     * verbose内容
     *
     * @param tTag verbose标签
     * @param tMsg log内容
     */
    public static void v(String tTag, String tMsg) {
        if (JKDebug.debug) {
            if (!tMsg.isEmpty())
                Log.v(tTag, tMsg);
        }
    }

    /**
     * 打印消息错误log
     *
     * @param tMessage 错误内容
     */
    public static synchronized void ErrorLog(String tMessage) {
        if (JKDebug.debug)
        {
            JKFile.CreateDir(ERROR_PATH);
            JKFile.AppendFile(ERROR_PATH, JKDate.GetFullDate(false) + ": " + GetFunctionPos() + "  " + tMessage + "\r\n");
        }
    }

    /**
     * 获取当前除库文件外函数位置
     *
     * @return 包名+类名+行数
     */
    public static String GetFunctionPos() {
        StackTraceElement[] a_steFunction = Thread.currentThread().getStackTrace();
        for (int i = 2; i < a_steFunction.length; ++i) {
            if (a_steFunction[i].getFileName() != null) {
                if (a_steFunction[i].getFileName().indexOf("JK") != 0) {
                    if (a_steFunction[i].getClassName() != null && a_steFunction[i].getMethodName() != null)
                        return a_steFunction[i].getClassName() + "." + a_steFunction[i].getMethodName() + "[" + a_steFunction[i].getLineNumber() + "]";
                    else
                        return "";
                }
            }
        }
        return "";
    }
}
