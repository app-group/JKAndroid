package com.jkframework.expression;

import com.jkframework.expression.callback.JKCommandOperatorCallback;
import com.jkframework.expression.exception.JKExpressionException;
import com.jkframework.expression.operator.JKCommandOperator;
import com.jkframework.expression.protocol.JKCommandProtocol;

import java.util.ArrayList;
import java.util.HashMap;

public class JKCommand<T> implements JKCommandProtocol<T> {

    protected JKExpression<T> expression;
    protected ArrayList<JKCommandOperator<T>> commandList = new ArrayList<>();

    public JKCommand(JKExpression<T> expression) {
        this.expression = expression;
    }

    @Override
    public JKCommandProtocol<T> Register(JKCommandOperator<T> op) {
        for (JKCommandOperator<T> oper : commandList) {
            if (oper.name.equals(op.name)) {
                return this;
            }
        }

        this.commandList.add(op);
        return this;
    }


    public String Calc(String formula,HashMap<String,T> paramDir)  throws JKExpressionException {
        JKCommandSuffix suffix = toSuffix(formula);
        return calcWithSuffix(suffix,paramDir);
    }

    public JKCommandSuffix toSuffix(String formula) throws JKExpressionException {



        boolean start = formula.charAt(0) == '{';
        boolean end = formula.charAt(formula.length() - 1) == '}';

        JKCommandSuffix result = new JKCommandSuffix();
        if (!start && !end) {
            result.defaultValue = formula;
            return result;
        } else if (!start) {
            throw new JKExpressionException("找不到对应的左括号");
        } else if (!end) {
            throw new JKExpressionException("找不到对应的右括号");
        } else {
            String str = formula.substring(1, 1 + formula.length() - 2);
            for (JKCommandOperator<T> obj : commandList) {
                if (str.indexOf(obj.name) == 0) {
                    str = str.substring(obj.name.length());
                    return obj.toSuffix(str);
                }
            }
            throw new JKExpressionException("未知操作符");
        }
    }

    public String calcWithSuffix(JKCommandSuffix suffix, HashMap<String,T> paramDir)  throws JKExpressionException {
        for (JKCommandOperator<T> oper : this.commandList) {
            if (oper.name.equals(suffix.operatorName)) {
                return  oper.calcWithSuffix(suffix, paramDir);
            }
        }
        return  suffix.defaultValue;
    }
}

