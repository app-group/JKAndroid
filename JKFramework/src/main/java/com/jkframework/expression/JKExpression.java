package com.jkframework.expression;

import com.jkframework.debug.JKLog;
import com.jkframework.expression.callback.JKExpressionCallback;
import com.jkframework.expression.exception.JKExpressionException;
import com.jkframework.expression.operator.JKBasicOperator;
import com.jkframework.expression.operator.JKBinaryOperator;
import com.jkframework.expression.operator.JKUnaryOperator;
import com.jkframework.expression.protocol.JKExpressionProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class JKExpression<T> implements JKExpressionProtocol<T> {
    private JKExpressionCallback<T> callback;
    private ArrayList<JKBasicOperator<T>> operatorList = new ArrayList<>();


    public JKExpression(JKExpressionCallback<T> callback) {
        this.callback = callback;
    }

    public JKExpressionProtocol<T> Register(JKBasicOperator<T> op) {
        for (JKBasicOperator<T> oper : operatorList) {
            if (oper.name.equals(op.name)) {
                return this;
            }
        }
        operatorList.add(op);
        return this;
    }

    public T calc(String formula) throws JKExpressionException {
        return calcWithSuffix(toSuffix(formula),null);
    }

    public ArrayList<String> toSuffix(String formula) throws JKExpressionException {
        Stack<JKBasicOperator<T>> stackOperator = new Stack<>();
        ArrayList<String> listResult = new ArrayList<>();

        StringBuilder valueTemp = new StringBuilder();
        int index = 0;

        while (true) {
            if (index == formula.length()) {
                if (!valueTemp.toString().equals("")) {
                    listResult.add(valueTemp.toString());
                }

                while (stackOperator.size() != 0) {
                    JKBasicOperator<T> operLast = stackOperator.pop();
                    listResult.add(operLast.name);
                }

                break;
            }

            if (formula.charAt(index) == ' ') {
                index++;
                continue;
            }

            ArrayList<JKBasicOperator<T>> find = new ArrayList<JKBasicOperator<T>>();
            for (JKBasicOperator<T> op : this.operatorList) {
                if (formula.substring(index).indexOf(op.name) == 0) {
                    find.add(op);
                }
            }

            if (find.size() != 0) {
                JKBasicOperator<T> oper = find.get(0);
                for (JKBasicOperator<T> op : find) {
                    if (op.name.length() > oper.name.length()) {
                        oper = op;
                    }
                }

                if (!valueTemp.toString().equals("")) {
                    listResult.add(valueTemp.toString());
                    valueTemp = new StringBuilder();
                } else {
                    if (oper.name.equals("-")) {
                        valueTemp.append(formula.charAt(index));
                        index += 1;
                        continue;
                    }
                }

                if (oper.name.equals("(")) {
                    stackOperator.push(oper);
                } else {
                    boolean found = true;
                    if (oper.name.equals(")")) {
                        found = false;
                    }
                    while (stackOperator.size() != 0) {
                        //看一眼栈顶
                        JKBasicOperator<T> operLast = stackOperator.peek();
                        if (oper.name.equals(")")) {

                            if (operLast.name.equals("(")) {
                                stackOperator.pop();
                                found = true;
                                break;
                            } else {
                                listResult.add(operLast.name);
                                stackOperator.pop();
                            }
                        } else if (operLast.compareTo(oper) >= 0) {
                            listResult.add(operLast.name);
                            stackOperator.pop();
                        } else {
                            break;
                        }
                    }

                    if (!oper.name.equals(")")) {
                        stackOperator.push(oper);
                    } else {
                        if (!found) {
                            throw new JKExpressionException("找不到对应的左括号");
                        }
                    }
                }

                index += oper.name.length();
            } else {
                valueTemp.append(formula.charAt(index));
                index += 1;
            }
        }

        return listResult;
    }

    public T calcWithSuffix(ArrayList<String> suffix, HashMap<String,T> paramDir) throws JKExpressionException {
        //calc
        ArrayList<String> suffixList = new ArrayList<>(suffix);
        Stack<T> stackCalc = new Stack<T>();
        while (suffixList.size() != 0) {
            String first = suffixList.get(0);
            JKBasicOperator<T> op = this.getOperator(first);

            if (op != null) {
                if (op instanceof JKBinaryOperator) {
                    if (stackCalc.size() < 2) {
                        throw new JKExpressionException("计算参数不符");
                    }

                    T secondValue = stackCalc.pop();
                    T firstValue = stackCalc.pop();

                    T result = ((JKBinaryOperator<T>)op).calc(firstValue, secondValue);

                    if (result != null) {
                        stackCalc.push(result);
                        suffixList.remove(0);
                    } else {
                        throw new JKExpressionException("计算出错");
                    }
                } else if (op instanceof JKUnaryOperator) {
                    if (stackCalc.size() < 1) {
                        throw new JKExpressionException("计算参数不符");
                    }
                    T firstValue = stackCalc.pop();
                    T result = ((JKUnaryOperator<T>)op).calc(firstValue);
                    if (result != null) {
                        stackCalc.push(result);
                        suffixList.remove(0);
                    } else {
                        throw new JKExpressionException("计算出错");
                    }
                } else {
                    throw new JKExpressionException("未知操作符");
                }
            } else {

                if (paramDir != null && paramDir.get(first) != null) {
                    stackCalc.push(paramDir.get(first));
                } else {
                    T result = this.callback.convent(first);
                    if (result == null) {
                        throw new JKExpressionException("计算出错");
                    }
                    stackCalc.push(result);
                }
                suffixList.remove(0);
            }
        }

        if (stackCalc.size() != 1) {
            throw new JKExpressionException("表达式异常");
        }
        return stackCalc.pop();
    }


    private JKBasicOperator<T> getOperator(String str) {

        for (JKBasicOperator<T> oper : operatorList) {
            if (oper.name.equals(str)) {
                return oper;
            }
        }
        return null;
    }
}


