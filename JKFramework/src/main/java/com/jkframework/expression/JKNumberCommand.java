package com.jkframework.expression;

import com.jkframework.algorithm.JKAnalysis;
import com.jkframework.expression.callback.JKCommandConvertCallback;
import com.jkframework.expression.callback.JKCommandOperatorCallback;
import com.jkframework.expression.callback.JKExpressionCallback;
import com.jkframework.expression.exception.JKExpressionException;
import com.jkframework.expression.operator.JKBinaryOperator;
import com.jkframework.expression.operator.JKCommandOperator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import kotlin.text.Regex;

public class JKNumberCommand extends JKCommand<Double> {

    public JKNumberCommand(JKNumberExpression expression) {
        super(expression);
        this.Register(new JKCommandOperator<Double>("I", new JKCommandConvertCallback<Double>() {
            @Override
            public JKCommandSuffix function(String str) throws JKExpressionException {
                if (str.length() > 0 && str.charAt(0) == ' ') {
                    str = str.substring(1);
                }

                if (str.length() > 0 && str.charAt(str.length() - 1) == ';') {
                    str = str.substring(0, str.length() - 1);
                }

                JKCommandSuffix result = new JKCommandSuffix();
                result.operatorName = "I";
                ArrayList<String> list = JKAnalysis.Split(str, "; ");
                if (list.size() == 0) {
                    return result;
                }

                for (int i = 0; i < list.size() - 1; ++i) {
                    ArrayList<String> condition = JKAnalysis.Split(list.get(i), ": ");

                    if (condition.size() == 2) {
                        ArrayList<String> value = expression.toSuffix(condition.get(0));
                        result.judgeList.add(value);
                        result.resultList.add(condition.get(1));
                    } else {
                        throw new JKExpressionException("表达式异常");
                    }
                }

                //末尾排查
                ArrayList<String> endCondition = JKAnalysis.Split(list.get(list.size() - 1), ": ");

                if (endCondition.size() == 2) {
                    ArrayList<String> value = expression.toSuffix(endCondition.get(0));
                    result.judgeList.add(value);
                    result.resultList.add(endCondition.get(1));
                } else {
                    result.defaultValue = endCondition.get(0);
                }
                return result;
            }
        }, new JKCommandOperatorCallback<Double>() {
            @Override
            public String funciton(JKCommandSuffix suffix,HashMap<String,Double> paramDir) throws JKExpressionException {
                for( int index = 0 ; index < suffix.judgeList.size(); ++index) {
                    if (expression.calcWithSuffix(suffix.judgeList.get(index), paramDir).intValue() != 0) {
                        return suffix.resultList.get(index);
                    }
                }
                return  suffix.defaultValue;
            }
        })).Register(new JKCommandOperator<Double>("S", new JKCommandConvertCallback<Double>() {
            @Override
            public JKCommandSuffix function(String str) throws JKExpressionException {
                if (str.length() > 0 && str.charAt(0) == ' ') {
                    str = str.substring(1);
                }

                if (str.length() > 0 && str.charAt(str.length() - 1) == ';') {
                    str = str.substring(0, str.length() - 1);
                }

                JKCommandSuffix result = new JKCommandSuffix();
                result.operatorName = "S";
                ArrayList<String> list = JKAnalysis.Split(str, "; ");
                if (list.size() <= 1) {
                    return result;
                }

                ArrayList<String> firstValue = expression.toSuffix(str);
                result.judgeList.add(firstValue);

                for (int i = 1; i < list.size() - 1; ++i) {
                    ArrayList<String> condition = JKAnalysis.Split(list.get(i), ": ");

                    if (condition.size() == 2) {
                        ArrayList<String> temp = new ArrayList<>();
                        temp.add(condition.get(0));
                        result.judgeList.add(temp);
                        result.resultList.add(condition.get(1));
                    } else {
                        throw new JKExpressionException("表达式异常");
                    }
                }

                //末尾排查
                ArrayList<String> endCondition = JKAnalysis.Split(list.get(list.size() - 1), ": ");

                if (endCondition.size() == 2) {
                    ArrayList<String> temp = new ArrayList<>();
                    temp.add(endCondition.get(0));
                    result.judgeList.add(temp);
                    result.resultList.add(endCondition.get(1));
                } else {
                    result.defaultValue = endCondition.get(0);
                }
                return result;
            }
        }, new JKCommandOperatorCallback<Double>() {
            @Override
            public String funciton(JKCommandSuffix suffix, HashMap<String,Double> paramDir) throws JKExpressionException {
                if (suffix.judgeList.size() > 1) {
                    Double value = expression.calcWithSuffix(suffix.judgeList.get(0), paramDir);
                    for( int index = 1 ; index < suffix.judgeList.size(); ++index) {
                        if (value.toString().equals(suffix.judgeList.get(index).get(0)))
                            return suffix.resultList.get(index - 1);
                    }
                    return  suffix.defaultValue;
                } else {
                    return suffix.defaultValue;
                }
            }
        }));
    } 
//    public JKNumberCommand(JKNumberExpression expression) {
//        super(expression);
//        this.Register(new JKCommandOperator<BigDecimal>("I", new JKCommandConvertCallback<BigDecimal>() {
//            @Override
//            public JKCommandSuffix function(String str) throws JKExpressionException {
//                if (str.length() > 0 && str.charAt(0) == ' ') {
//                    str = str.substring(1);
//                }
//
//                if (str.length() > 0 && str.charAt(str.length() - 1) == ';') {
//                    str = str.substring(0, str.length() - 1);
//                }
//
//                JKCommandSuffix result = new JKCommandSuffix();
//                result.operatorName = "I";
//                ArrayList<String> list = JKAnalysis.Split(str, "; ");
//                if (list.size() == 0) {
//                    return result;
//                }
//
//                for (int i = 0; i < list.size() - 1; ++i) {
//                    ArrayList<String> condition = JKAnalysis.Split(list.get(i), ": ");
//
//                    if (condition.size() == 2) {
//                        ArrayList<String> value = expression.toSuffix(condition.get(0));
//                        result.getJudgeList().add(value);
//                        result.resultList.add(condition.get(1));
//                    } else {
//                        throw new JKExpressionException("表达式异常");
//                    }
//                }
//
//                //末尾排查
//                ArrayList<String> endCondition = JKAnalysis.Split(list.get(list.size() - 1), ": ");
//
//                if (endCondition.size() == 2) {
//                    ArrayList<String> value = expression.toSuffix(endCondition.get(0));
//                    result.getJudgeList().add(value);
//                    result.resultList.add(endCondition.get(1));
//                } else {
//                    result.defaultValue = endCondition.get(0);
//                }
//                return result;
//            }
//        }, new JKCommandOperatorCallback<BigDecimal>() {
//            @Override
//            public String funciton(JKCommandSuffix suffix) throws JKExpressionException {
//                for( int index = 0 ; index < suffix.getJudgeList().size(); ++index) {
//                    if (expression.calcWithSuffix(suffix.getJudgeList().get(index)).intValue() != 0) {
//                        return suffix.resultList.get(index);
//                    }
//                }
//                return  suffix.defaultValue;
//            }
//        })).Register(new JKCommandOperator<BigDecimal>("S", new JKCommandConvertCallback<BigDecimal>() {
//            @Override
//            public JKCommandSuffix function(String str) throws JKExpressionException {
//                if (str.length() > 0 && str.charAt(0) == ' ') {
//                    str = str.substring(1);
//                }
//
//                if (str.length() > 0 && str.charAt(str.length() - 1) == ';') {
//                    str = str.substring(0, str.length() - 1);
//                }
//
//                JKCommandSuffix result = new JKCommandSuffix();
//                result.operatorName = "S";
//                ArrayList<String> list = JKAnalysis.Split(str, "; ");
//                if (list.size() <= 1) {
//                    return result;
//                }
//
//                ArrayList<String> firstValue = expression.toSuffix(str);
//                result.getJudgeList().add(firstValue);
//
//                for (int i = 1; i < list.size() - 1; ++i) {
//                    ArrayList<String> condition = JKAnalysis.Split(list.get(i), ": ");
//
//                    if (condition.size() == 2) {
//                        ArrayList<String> temp = new ArrayList<>();
//                        temp.add(condition.get(0));
//                        result.getJudgeList().add(temp);
//                        result.resultList.add(condition.get(1));
//                    } else {
//                        throw new JKExpressionException("表达式异常");
//                    }
//                }
//
//                //末尾排查
//                ArrayList<String> endCondition = JKAnalysis.Split(list.get(list.size() - 1), ": ");
//
//                if (endCondition.size() == 2) {
//                    ArrayList<String> temp = new ArrayList<>();
//                    temp.add(endCondition.get(0));
//                    result.getJudgeList().add(temp);
//                    result.resultList.add(endCondition.get(1));
//                } else {
//                    result.defaultValue = endCondition.get(0);
//                }
//                return result;
//            }
//        }, new JKCommandOperatorCallback<BigDecimal>() {
//            @Override
//            public String funciton(JKCommandSuffix suffix) throws JKExpressionException {
//                if (suffix.getJudgeList().size() > 1) {
//                    BigDecimal value = expression.calcWithSuffix(suffix.getJudgeList().get(0));
//                    for( int index = 1 ; index < suffix.getJudgeList().size(); ++index) {
//                        if (value.toString().equals(suffix.getJudgeList().get(index).get(0)))
//                            return suffix.resultList.get(index - 1);
//                    }
//                    return  suffix.defaultValue;
//                } else {
//                    return suffix.defaultValue;
//                }
//            }
//        }));
//    }
}

