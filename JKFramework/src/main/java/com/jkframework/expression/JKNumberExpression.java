package com.jkframework.expression;

import com.jkframework.expression.callback.JKBinaryOperatorCallback;
import com.jkframework.expression.callback.JKExpressionCallback;
import com.jkframework.expression.callback.JKUnaryOperatorCallback;
import com.jkframework.expression.exception.JKExpressionException;
import com.jkframework.expression.operator.JKBasicOperator;
import com.jkframework.expression.operator.JKBinaryOperator;
import com.jkframework.expression.operator.JKUnaryOperator;

import java.math.BigDecimal;
import java.math.RoundingMode;



public class JKNumberExpression extends JKExpression<Double> {

    public JKNumberExpression(JKExpressionCallback<Double> callback) {
        super(callback);
        this.Register(new JKBinaryOperator<Double>("+", 10, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1 + v2;
            }
        })).Register(new JKBinaryOperator<Double>("-", 10, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1 - v2;
            }
        })).Register(new JKBinaryOperator<Double>("*", 100, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1 * v2;
            }
        })).Register(new JKBinaryOperator<Double>("/", 100, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) throws JKExpressionException {
                if (v2  == 0) {
                    throw new JKExpressionException("除数为0");
                }
                return v1 / v2;
            }
        })).Register(new JKBinaryOperator<Double>("&&", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) throws JKExpressionException {
                return (v1 != 0 && v2 != 0) ? 1.0 : 0;
            }
        })).Register(new JKBinaryOperator<Double>("||", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) throws JKExpressionException {
                return (v1 != 0 || v2 != 0) ? 1.0 : 0;
            }
        })).Register(new JKUnaryOperator<Double>("!", 200, new JKUnaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v) {
                return v == 0 ? 1.0 :  0;
            }
        })).Register(new JKBinaryOperator<Double>("==", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.equals(v2) ? 1.0 : 0;
            }
        })).Register(new JKBinaryOperator<Double>("!=", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.equals(v2) ? 0 : 1.0;
            }
        })).Register(new JKBinaryOperator<Double>(">", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.compareTo(v2) > 0 ? 1.0 : 0;
            }
        })).Register(new JKBinaryOperator<Double>("<", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.compareTo(v2) < 0 ? 1.0 : 0;
            }
        })).Register(new JKBinaryOperator<Double>(">=", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.compareTo(v2) >= 0 ? 1.0 : 0;
            }
        })).Register(new JKBinaryOperator<Double>("<=", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.compareTo(v2) <= 0 ? 1.0 : 0;
            }
        })).Register(new JKUnaryOperator<Double>("sin", 250, new JKUnaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v) {
                return Math.sin(v * Math.PI /180.0);
            }
        })).Register(new JKUnaryOperator<Double>("cos", 250, new JKUnaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v) {
                return Math.cos(v * Math.PI /180.0);
            }
        })).Register(new JKUnaryOperator<Double>("tan", 250, new JKUnaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v) {
                return Math.tan(v * Math.PI /180.0);
            }
        })).Register(new JKBinaryOperator<Double>("!=", 5, new JKBinaryOperatorCallback<Double>() {
            @Override
            public Double function(Double v1, Double v2) {
                return v1.equals(v2) ? 0 : 1.0;
            }
        })).Register(new JKBasicOperator<Double>("(", 0))
                .Register(new JKBasicOperator<Double>(")", 0));
    }
}

//public class JKNumberExpression extends JKExpression<BigDecimal> {
//
//    public JKNumberExpression(JKExpressionCallback<BigDecimal> callback) {
//        super(callback);
//        this.Register(new JKBinaryOperator<BigDecimal>("+", 10, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.add(v2);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("-", 10, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.subtract(v2);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("*", 100, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.multiply(v2);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("/", 100, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) throws JKExpressionException {
//                if (v2.intValue() == 0) {
//                    throw new JKExpressionException("除数为0");
//                }
//                return v1.divide(v2,16, RoundingMode.DOWN);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("&&", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) throws JKExpressionException {
//                return (v1.intValue() != 0 && v2.intValue() != 0) ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("||", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) throws JKExpressionException {
//                return (v1.intValue() != 0 || v2.intValue() != 0) ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKUnaryOperator<BigDecimal>("!", 200, new JKUnaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v) {
//                return v.intValue() == 0 ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("==", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.equals(v2) ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("!=", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.equals(v2) ? new BigDecimal(0) : new BigDecimal(1);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>(">", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.compareTo(v2) > 0 ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("<", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.compareTo(v2) < 0 ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>(">=", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.compareTo(v2) >= 0 ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKBinaryOperator<BigDecimal>("<=", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.compareTo(v2) <= 0 ? new BigDecimal(1) : new BigDecimal(0);
//            }
//        })).Register(new JKUnaryOperator<BigDecimal>("sin", 250, new JKUnaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v) {
//                return new BigDecimal(Math.sin(v.doubleValue() * Math.PI /180.0));
//            }
//        })).Register(new JKUnaryOperator<BigDecimal>("cos", 250, new JKUnaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v) {
//                return new BigDecimal(Math.cos(v.doubleValue() * Math.PI /180.0));
//            }
//        })).Register(new JKUnaryOperator<BigDecimal>("tan", 250, new JKUnaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v) {
//                return new BigDecimal(Math.tan(v.doubleValue() * Math.PI /180.0));
//            }
//        })).Register(new JKBasicOperator<BigDecimal>(")", 0)).Register(new JKBinaryOperator<BigDecimal>("!=", 5, new JKBinaryOperatorCallback<BigDecimal>() {
//            @Override
//            public BigDecimal function(BigDecimal v1, BigDecimal v2) {
//                return v1.equals(v2) ? new BigDecimal(0) : new BigDecimal(1);
//            }
//        })).Register(new JKBasicOperator<BigDecimal>("(", 0))
//                .Register(new JKBasicOperator<BigDecimal>(")", 0));
//    }
//}

