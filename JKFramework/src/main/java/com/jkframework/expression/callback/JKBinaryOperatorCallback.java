package com.jkframework.expression.callback;

import com.jkframework.expression.exception.JKExpressionException;

public interface JKBinaryOperatorCallback<T> {
    T function(T v1, T v2) throws JKExpressionException;
}
