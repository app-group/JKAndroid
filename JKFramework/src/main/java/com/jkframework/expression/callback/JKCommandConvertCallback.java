package com.jkframework.expression.callback;

import com.jkframework.expression.JKCommandSuffix;
import com.jkframework.expression.exception.JKExpressionException;

public interface JKCommandConvertCallback<T> {

    JKCommandSuffix function(String str) throws JKExpressionException;
}
