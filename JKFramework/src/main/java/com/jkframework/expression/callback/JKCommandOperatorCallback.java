package com.jkframework.expression.callback;

import com.jkframework.expression.JKCommandSuffix;
import com.jkframework.expression.exception.JKExpressionException;

import java.util.HashMap;

public interface JKCommandOperatorCallback<T> {
    String funciton(JKCommandSuffix suffix, HashMap<String, T> paramDir) throws JKExpressionException;
}
