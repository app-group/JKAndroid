package com.jkframework.expression.callback;

import com.jkframework.expression.exception.JKExpressionException;

public interface JKExpressionCallback<T> {

    T convent(String str) throws JKExpressionException;
}
