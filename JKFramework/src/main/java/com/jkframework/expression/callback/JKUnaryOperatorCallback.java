package com.jkframework.expression.callback;

public interface JKUnaryOperatorCallback<T> {
    T function(T v);
}
