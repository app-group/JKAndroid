package com.jkframework.expression.exception;

public class JKExpressionException extends Exception {

    public JKExpressionException(String errortype) {
        super(errortype);
    }
}
