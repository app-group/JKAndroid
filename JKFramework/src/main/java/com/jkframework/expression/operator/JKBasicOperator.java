package com.jkframework.expression.operator;

public class JKBasicOperator<T> implements Comparable<JKBasicOperator<T>>{
    public String name = "";
    public int lever = 0;

    public JKBasicOperator(String name, int lever) {
        this.name = name;
        this.lever = lever;
    }

    @Override
    public int compareTo(JKBasicOperator<T> o) {
        return this.lever - o.lever;
    }

}
