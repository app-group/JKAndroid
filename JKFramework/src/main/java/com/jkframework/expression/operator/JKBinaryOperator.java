package com.jkframework.expression.operator;

import com.jkframework.expression.callback.JKBinaryOperatorCallback;
import com.jkframework.expression.exception.JKExpressionException;


public class JKBinaryOperator<T> extends JKBasicOperator<T> {

    private JKBinaryOperatorCallback<T> callback;

    public JKBinaryOperator(String name, int lever,JKBinaryOperatorCallback<T> callback) {
        super(name,lever);
        this.name = name;
        this.lever = lever;
        this.callback = callback;
    }

    public T calc(T v1, T v2) throws JKExpressionException {
        if (callback != null) {
            return callback.function(v1,v2);
        }
       return null;
    }
}
