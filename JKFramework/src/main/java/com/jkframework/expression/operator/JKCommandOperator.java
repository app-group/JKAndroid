package com.jkframework.expression.operator;

import com.jkframework.expression.JKCommandSuffix;
import com.jkframework.expression.callback.JKCommandConvertCallback;
import com.jkframework.expression.callback.JKCommandOperatorCallback;
import com.jkframework.expression.exception.JKExpressionException;

import java.util.HashMap;

public class JKCommandOperator<T> {

    public String name;
    private JKCommandOperatorCallback<T> function;
    private JKCommandConvertCallback<T> convert;

    public JKCommandOperator(String name ,
                             JKCommandConvertCallback<T> convert,
                             JKCommandOperatorCallback<T> function)  {
        this.convert = convert;
        this.function = function;
        this.name = name;
    }

    public JKCommandSuffix toSuffix(String formula) throws  JKExpressionException {
        return convert.function(formula);
    }

    public String calcWithSuffix(JKCommandSuffix suffix, HashMap<String,T> paramDir) throws JKExpressionException {
        return function.funciton(suffix,paramDir);
    }

}

