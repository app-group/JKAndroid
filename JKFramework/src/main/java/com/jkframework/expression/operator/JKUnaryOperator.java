package com.jkframework.expression.operator;

import com.jkframework.expression.callback.JKUnaryOperatorCallback;

public class JKUnaryOperator<T> extends JKBasicOperator<T> {

    private JKUnaryOperatorCallback<T> callback;

    public JKUnaryOperator(String name, int lever,JKUnaryOperatorCallback<T> callback) {
        super(name,lever);
        this.name = name;
        this.lever = lever;
        this.callback = callback;
    }

    public T calc(T v) {
        if (callback != null) {
            return callback.function(v);
        }
        return null;
    }
}
