package com.jkframework.expression.protocol;


import com.jkframework.expression.operator.JKBasicOperator;
import com.jkframework.expression.operator.JKCommandOperator;

public interface JKCommandProtocol<T> {

    JKCommandProtocol<T> Register(JKCommandOperator<T> op);
}

