package com.jkframework.expression.protocol;


import com.jkframework.expression.operator.JKBasicOperator;

public interface JKExpressionProtocol<T> {

    JKExpressionProtocol<T> Register(JKBasicOperator<T> operator);
}

