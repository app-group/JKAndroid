package com.jkframework.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.jkframework.activity.JKBaseActivity;
import com.jkframework.manager.JKActivityManager;

import org.jetbrains.annotations.NotNull;

import me.yokeyword.fragmentation.ExtraTransaction;
import me.yokeyword.fragmentation.ISupportFragment;
import me.yokeyword.fragmentation.SupportFragmentDelegate;
import me.yokeyword.fragmentation.SupportHelper;
import me.yokeyword.fragmentation.anim.FragmentAnimator;

public abstract class JKBaseFragment<E extends ViewDataBinding> extends Fragment implements ISupportFragment {

    /**
     * Frangment是否运行中
     */
    private boolean bRun = false;
    /**
     * 界面初始化
     */
    protected boolean bInit = false;
    /**
     * 回收初始化
     */
    protected boolean bRecycle = false;

    final SupportFragmentDelegate mDelegate = new SupportFragmentDelegate(this);
    protected AppCompatActivity mActivity;

    private E mBinding;

    protected abstract int getLayout();

    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        E binding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
        binding.setLifecycleOwner(this);
        mBinding = binding;
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        bRun = true;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mDelegate.onAttach();
        mActivity = (AppCompatActivity) context;
    }

    protected E getBinding() {
        return mBinding;
    }

    public Lifecycle GetLifecycle() {
        return getLifecycle();
    }

    /**
     * Activity是否正常运作中
     *
     * @return true表示正常运作中
     */
    public boolean IsRun() {
        return bRun;
    }


    /**
     * 获取Framgment所在Activity
     *
     * @return 所在Activity
     */
    public JKBaseActivity GetActivity() {
        if (super.getActivity() != null)
            return (JKBaseActivity) super.getActivity();
        else {
            return (JKBaseActivity) JKActivityManager.GetCurrentActivity();
        }
    }


    /**
     * 打开Activity
     *
     * @param cls 界面class对象
     */
    public synchronized void ReplaceActivity(Class<?> cls) {
        if (bRun) {
            bRun = false;
            Intent itIntent = new Intent(getActivity(), cls);
            super.startActivity(itIntent);
            finish();
        }
    }

    /**
     * 打开Activity
     *
     * @param cls      界面class对象
     * @param itIntent Intent参数对象
     */
    public synchronized void ReplaceActivity(Class<?> cls, Intent itIntent) {
        if (bRun) {
            bRun = false;
            itIntent.setClass(getActivity(), cls);
            super.startActivity(itIntent);
            finish();
        }
    }


    /**
     * 打开Activity
     *
     * @param cls 界面class对象
     */
    public synchronized void StartActivity(Class<?> cls) {
        if (bRun) {
            bRun = false;
            Intent itIntent = new Intent(getActivity(), cls);
            super.startActivity(itIntent);
        }
    }

    /**
     * 打开Activity
     *
     * @param cls      界面class对象
     * @param itIntent Intent参数对象
     */
    public synchronized void StartActivity(Class<?> cls, Intent itIntent) {
        if (bRun) {
            bRun = false;
            itIntent.setClass(getActivity(), cls);
            super.startActivity(itIntent);
        }
    }

    /**
     * 关闭Activity
     */
    public synchronized void finish() {
        JKBaseActivity CurrentActivity = GetActivity();
        if (CurrentActivity != null)
            CurrentActivity.finish();
    }

    /**
     * 设置Toolbar
     *
     * @param toolbar Toolbar对象
     */
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        JKBaseActivity CurrentActivity = GetActivity();
        if (CurrentActivity != null)
            CurrentActivity.setSupportActionBar(toolbar);
    }

    public boolean IsActive() {
        return isAdded();
    }

    /**
     * 锁屏对话框
     *
     * @param tMessage 锁屏提示信息
     */
    public void LockScreen(String tMessage) {
        JKBaseActivity CurrentActivity = GetActivity();
        if (CurrentActivity != null)
            CurrentActivity.LockScreen(tMessage);
    }

    /**
     * 解除锁屏对话框
     */
    public void UnlockScreen() {
        JKBaseActivity CurrentActivity = GetActivity();
        if (CurrentActivity != null)
            CurrentActivity.UnlockScreen();
    }

    /**
     * 打开Activity并接受返回
     *
     * @param itIntent     Intent参数对象
     * @param nRequestCode 返回值索引
     */
    public synchronized void StartActivityForResult(Intent itIntent, int nRequestCode) {
        if (bRun) {
            bRun = false;
            super.startActivityForResult(itIntent, nRequestCode);
        }
    }

    /**
     * 打开Activity并接受返回
     *
     * @param cls          界面class对象
     * @param nRequestCode 返回值索引
     */
    public synchronized void StartActivityForResult(Class<?> cls, int nRequestCode) {
        if (bRun) {
            bRun = false;
            Intent itIntent = new Intent(getActivity(), cls);
            super.startActivityForResult(itIntent, nRequestCode);
        }
    }

    /**
     * 打开Activity并接受返回
     *
     * @param cls          界面class对象
     * @param itIntent     Intent参数对象
     * @param nRequestCode 返回值索引
     */
    public synchronized void StartActivityForResult(Class<?> cls, Intent itIntent, int nRequestCode) {
        if (bRun) {
            bRun = false;
            itIntent.setClass(getActivity(), cls);
            super.startActivityForResult(itIntent, nRequestCode);
        }
    }

    @Override
    public SupportFragmentDelegate getSupportDelegate() {
        return mDelegate;
    }

    /**
     * Perform some extra transactions.
     * 额外的事务：自定义Tag，添加SharedElement动画，操作非回退栈Fragment
     */
    @Override
    public ExtraTransaction extraTransaction() {
        return mDelegate.extraTransaction();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDelegate.onCreate(savedInstanceState);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return mDelegate.onCreateAnimation(transit, enter, nextAnim);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDelegate.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mDelegate.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        mDelegate.onPause();
    }

    @Override
    public void onDestroyView() {
        mDelegate.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mDelegate.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        mDelegate.onHiddenChanged(hidden);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mDelegate.setUserVisibleHint(isVisibleToUser);
    }

    /**
     * Causes the Runnable r to be added to the action queue.
     * <p>
     * The runnable will be run after all the previous action has been run.
     * <p>
     * 前面的事务全部执行后 执行该Action
     */
    @Override
    public void post(Runnable runnable) {
        mDelegate.post(runnable);
    }

    /**
     * Called when the enter-animation end.
     * 入栈动画 结束时,回调
     */
    @Override
    public void onEnterAnimationEnd(Bundle savedInstanceState) {
        mDelegate.onEnterAnimationEnd(savedInstanceState);
    }


    /**
     * Lazy initial，Called when fragment is first called.
     * <p>
     * 同级下的 懒加载 ＋ ViewPager下的懒加载  的结合回调方法
     */
    @Override
    public void onLazyInitView(@Nullable Bundle savedInstanceState) {
        mDelegate.onLazyInitView(savedInstanceState);
    }

    /**
     * Called when the fragment is visible.
     * 当Fragment对用户可见时回调
     * <p>
     * Is the combination of  [onHiddenChanged() + onResume()/onPause() + setUserVisibleHint()]
     */
    @Override
    public void onSupportVisible() {
        mDelegate.onSupportVisible();
    }

    /**
     * Called when the fragment is invivible.
     * <p>
     * Is the combination of  [onHiddenChanged() + onResume()/onPause() + setUserVisibleHint()]
     */
    @Override
    public void onSupportInvisible() {
        mDelegate.onSupportInvisible();
    }

    /**
     * Return true if the fragment has been supportVisible.
     */
    @Override
    final public boolean isSupportVisible() {
        return mDelegate.isSupportVisible();
    }

    /**
     * Set fragment animation with a higher priority than the ISupportActivity
     * 设定当前Fragmemt动画,优先级比在SupportActivity里高
     */
    @Override
    public FragmentAnimator onCreateFragmentAnimator() {
        return mDelegate.onCreateFragmentAnimator();
    }

    /**
     * 获取设置的全局动画 copy
     *
     * @return FragmentAnimator
     */
    @Override
    public FragmentAnimator getFragmentAnimator() {
        return mDelegate.getFragmentAnimator();
    }

    /**
     * 设置Fragment内的全局动画
     */
    @Override
    public void setFragmentAnimator(FragmentAnimator fragmentAnimator) {
        mDelegate.setFragmentAnimator(fragmentAnimator);
    }

    /**
     * 按返回键触发,前提是SupportActivity的onBackPressed()方法能被调用
     *
     * @return false则继续向上传递, true则消费掉该事件
     */
    @Override
    public boolean onBackPressedSupport() {
        return mDelegate.onBackPressedSupport();
    }

    @Override
    public void setFragmentResult(int resultCode, Bundle bundle) {
        mDelegate.setFragmentResult(resultCode, bundle);
    }

    @Override
    public void onFragmentResult(int requestCode, int resultCode, Bundle data) {
        mDelegate.onFragmentResult(requestCode, resultCode, data);
    }

    @Override
    public void onNewBundle(Bundle args) {
        mDelegate.onNewBundle(args);
    }

    @Override
    public void putNewBundle(Bundle newBundle) {
        mDelegate.putNewBundle(newBundle);
    }

    /****************************************以下为可选方法(Optional methods)******************************************************/
    // 自定制Support时，可移除不必要的方法

    /**
     * 隐藏软键盘
     */
    protected void hideSoftInput() {
        mDelegate.hideSoftInput();
    }

    /**
     * 显示软键盘,调用该方法后,会在onPause时自动隐藏软键盘
     */
    protected void showSoftInput(final View view) {
        mDelegate.showSoftInput(view);
    }

    /**
     * 加载根Fragment, 即Activity内的第一个Fragment 或 Fragment内的第一个子Fragment
     *
     * @param containerId 容器id
     * @param toFragment  目标Fragment
     */
    public void loadRootFragment(int containerId, ISupportFragment toFragment) {
        mDelegate.loadRootFragment(containerId, toFragment);
    }

    public void loadRootFragment(int containerId, ISupportFragment toFragment, boolean addToBackStack, boolean allowAnim) {
        mDelegate.loadRootFragment(containerId, toFragment, addToBackStack, allowAnim);
    }

    public void start(ISupportFragment toFragment) {
        mDelegate.start(toFragment);
    }

    /**
     * @param launchMode Similar to Activity's LaunchMode.
     */
    public void start(final ISupportFragment toFragment, @LaunchMode int launchMode) {
        mDelegate.start(toFragment, launchMode);
    }

    /**
     * Launch an fragment for which you would like a result when it poped.
     */
    public void startForResult(ISupportFragment toFragment, int requestCode) {
        mDelegate.startForResult(toFragment, requestCode);
    }

    /**
     * Start the target Fragment and pop itself
     */
    public void startWithPop(ISupportFragment toFragment) {
        mDelegate.startWithPop(toFragment);
    }

    /**
     * @see #popTo(Class, boolean)
     * +
     * @see #start(ISupportFragment)
     */
    public void startWithPopTo(ISupportFragment toFragment, Class<?> targetFragmentClass, boolean includeTargetFragment) {
        mDelegate.startWithPopTo(toFragment, targetFragmentClass, includeTargetFragment);
    }

    public void replaceFragment(ISupportFragment toFragment, boolean addToBackStack) {
        mDelegate.replaceFragment(toFragment, addToBackStack);
    }

    public void showHideFragment(ISupportFragment showFragment, ISupportFragment hideFragment) {
        mDelegate.showHideFragment(showFragment, hideFragment);
    }

    public void pop() {
        mDelegate.pop();
    }

    /**
     * Pop the last fragment transition from the manager's fragment
     * back stack.
     * <p>
     * 出栈到目标fragment
     *
     * @param targetFragmentClass   目标fragment
     * @param includeTargetFragment 是否包含该fragment
     */
    public void popTo(Class<?> targetFragmentClass, boolean includeTargetFragment) {
        mDelegate.popTo(targetFragmentClass, includeTargetFragment);
    }

    /**
     * 获取栈内的fragment对象
     */
    public <T extends ISupportFragment> T findChildFragment(Class<T> fragmentClass) {
        return SupportHelper.findFragment(getChildFragmentManager(), fragmentClass);
    }
}
