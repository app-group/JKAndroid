package com.jkframework.hardware;

import android.content.Context;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;

import com.jkframework.application.JKApplication;

public class JKCamera {

    /**
     * 相机单例对象
     */
    private static JKCamera jkcCamera = null;

    /**
     * 照相机对象
     */
    private Camera cmCamera = null;
    /**
     * 照相机对象
     */
    private CameraManager cameraManager = null;
    /**
     * 判断是否支持硬件
     */
    private boolean bHardware = true;
    /**
     * 判断是否支持闪光灯
     */
    private boolean bFlashLight = false;

    /**
     * 获取相机单例对象
     *
     * @return 相机对象
     */
    public static JKCamera GetInstance() {
        JKCamera tmp = jkcCamera;
        if (tmp == null) {
            synchronized (JKCamera.class) {
                tmp = jkcCamera;
                if (tmp == null) {
                    jkcCamera = tmp = new JKCamera();
                }
            }
        }
        return tmp;
    }

    public JKCamera() {
        if (!JKApplication.getInstance().getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
            bHardware = false;
        }
        PackageManager packageManager = JKApplication.getInstance().getApplicationContext().getPackageManager();
        FeatureInfo[]  features = packageManager.getSystemAvailableFeatures();
        for(FeatureInfo f : features) {
            if (PackageManager.FEATURE_CAMERA_FLASH.equals(f.name)) {
                bFlashLight = true;
                break;
            }
        }
    }

    /**
     * 打开手电筒
     */
    public void OpenFlashLight() {
        if (!bHardware || !bFlashLight)
            return;
        InitCamera();
        try {
            //获得属性
            if (cameraManager.getCameraIdList().length > 0) {
                cameraManager.setTorchMode(cameraManager.getCameraIdList()[0], true);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭手电筒
     */
    public void CloseFlashLight() {
        if (!bHardware || !bFlashLight)
            return;
        try {
            if (cameraManager.getCameraIdList().length > 0) {
                cameraManager.setTorchMode(cameraManager.getCameraIdList()[0], false);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void InitCamera()
    {
        if (cameraManager == null)
            cameraManager = (CameraManager) JKApplication.getInstance().getApplicationContext().getSystemService(Context.CAMERA_SERVICE);
    }
}