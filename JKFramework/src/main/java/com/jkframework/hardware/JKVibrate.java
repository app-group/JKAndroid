package com.jkframework.hardware;

import android.app.Service;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import com.jkframework.application.JKApplication;




public class JKVibrate
{
	
	/**
	 * 手机振动
	 * @param nTime 振动时间
	 */
	public static void Vibrate(int nTime)
	{
		Vibrator vibrator = (Vibrator) JKApplication.getInstance().getApplicationContext().getSystemService(Service.VIBRATOR_SERVICE);
		if (vibrator != null) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				vibrator.vibrate(VibrationEffect.createOneShot(nTime,127));
			}
			else {
				vibrator.vibrate(nTime);
			}
		}
	}
}