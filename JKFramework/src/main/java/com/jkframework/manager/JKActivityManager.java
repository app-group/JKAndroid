/*
 * Copyright (C) 2007-2014 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package com.jkframework.manager;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.ActivityUtils;

import java.util.List;

public class JKActivityManager {

    public static List<Activity> GetAllActivity() {
        return ActivityUtils.getActivityList();
    }

    /**
     * 退出所有Activity
     */
    public static void Exit() {
        ActivityUtils.finishAllActivities();
    }

    /**
     * 获取当前正在运行的Activity
     * @return 当前运行的Activity,获取失败返回null
     */
    public static Activity GetCurrentActivity()
    {
        //return ActivityUtils.getTopActivity();
        for (Activity activity : GetAllActivity()) {
            return activity;
        }
        return ActivityUtils.getTopActivity();
    }

    public static AppCompatActivity GetCurrentLifecycleActivity()
    {
        Activity current = GetCurrentActivity();
        if (current instanceof AppCompatActivity)
        {
            return (AppCompatActivity) current;
        }
        return null;
    }
}
