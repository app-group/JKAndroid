package com.jkframework.manager;

import android.media.MediaPlayer;

import com.jkframework.bean.JKMusicData;

import java.io.IOException;


public class JKMusicManager
{
	/**通知栏单例对象*/
	private static JKMusicManager jknfMusic = null;


	/**
	 * 获取单例对象
	 * @return  通知栏对象
	 */
	public static JKMusicManager GetInstance()
	{
		JKMusicManager tmp = jknfMusic;
		if (tmp == null) {
			synchronized (JKMusicManager.class) {
				tmp = jknfMusic;
				if (tmp == null) {
					jknfMusic = tmp = new JKMusicManager();
				}
			}
		}
		return tmp;
	}

	public JKMusicManager()
	{

	}

	/**
	 * 加载音频文件
	 * @param tPath SD卡
	 * @return 播放器对象返回
	 */
	public JKMusicData LoadMusic(String tPath)
	{
		JKMusicData data = new JKMusicData();
		MediaPlayer mPlayer = new MediaPlayer();
		mPlayer.reset();
		try {
			mPlayer.setDataSource(tPath); // 设置曲目资源
			mPlayer.prepare();
		} catch (IOException e) {
			e.printStackTrace();
			return data;
		}
		data.setPlayer(mPlayer);
		data.setInit(true);
		return data;
	}

	/**
	 * 播放音乐
	 * @param jkmdPlayer 播放器对象
	 */
	public void StartMusic(JKMusicData jkmdPlayer)
	{
		if (jkmdPlayer.isInit())
			jkmdPlayer.getPlayer().start();
	}

	/**
	 * 暂停音乐
	 * @param jkmdPlayer 播放器对象
	 */
	public void PauseMusic(JKMusicData jkmdPlayer)
	{
		if (jkmdPlayer.isInit())
			jkmdPlayer.getPlayer().pause();
	}

}