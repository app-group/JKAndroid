package com.jkframework.net;


import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.jkframework.application.JKApplication;

import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JKHttpRetrofit
{

    private static JKCommonHeadersInterceptor interceptor = new JKCommonHeadersInterceptor();
    private static OkHttpClient okhcClient;

	/** Http请求类 */
    private static final Retrofit.Builder rftBuilder = new Retrofit.Builder();
	static{
        ClearableCookieJar cookieJar =
                new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(JKApplication.getInstance().getApplicationContext()));

        okhcClient = new OkHttpClient().newBuilder().readTimeout(10, TimeUnit.SECONDS)
				.connectTimeout(10,TimeUnit.SECONDS)
				.writeTimeout(10,TimeUnit.SECONDS)
                .addInterceptor(interceptor) // 在这里添加拦截器
                .cookieJar(cookieJar).build();

        rftBuilder.addConverterFactory(GsonConverterFactory.create());
        rftBuilder.addCallAdapterFactory(RxJava3CallAdapterFactory.create());
	}

    /**
     * 获取RetrofitBuilder
     * @return Builder对象
     */
    public static Retrofit.Builder GetRetrofitBuilder(OkHttpClient customClient)
    {
        interceptor.clearHeaders();
        rftBuilder.client(customClient == null ? okhcClient : customClient);
        return rftBuilder;
    }

    public static void setHeader(String key, String value)
    {
        interceptor.setHeader(key, value);
    }
}