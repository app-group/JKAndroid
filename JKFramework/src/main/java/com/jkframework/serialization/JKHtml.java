package com.jkframework.serialization;

import com.jkframework.algorithm.JKAnalysis;
import com.jkframework.algorithm.JKConvert;
import com.jkframework.algorithm.JKFile;
import com.jkframework.debug.JKLog;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class JKHtml {

	/**html Dom对象*/
	private Document dmDoc = null;

	/**
	 * 从文件里读取序列化内容
	 * @param tPath 文件地址(assets不加"/")
	 * @param tEncoding 编码支持"UTF-8","GBK
	 */
	public void LoadFile(String tPath, Charset tEncoding) {
		try {
			if (tPath.startsWith("/"))	//sdcard路径
				dmDoc = Jsoup.parse(new File(tPath), tEncoding.name());
			else
				dmDoc = Jsoup.parse(JKFile.ReadAssetsFile(tPath, tEncoding));
		} catch (IOException e) {
			JKLog.ErrorLog("加载Html文件失败.原因为" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 从文件里读取序列化内容
	 * @param tPath 文件地址(assets不加"/")
	 */
	public void LoadFile(String tPath)
	{
		LoadFile(tPath, StandardCharsets.UTF_8);
	}

	/**
	 * 设置序列化内容
	 * @param tText 序列化内容
	 */
	public void LoadString(String tText) {
		dmDoc = Jsoup.parse(tText);
	}

	/**
	 * 将序列化内容保存文件
	 * @param tPath 保存的sdcard路径
	 */
	public void SaveFile(String tPath, Charset tEncoding) {
		JKFile.WriteFile(tPath, GetString(), tEncoding);
	}

	/**
	 * 将序列化内容转为字符串
	 * @return 转化后的字符串
	 */
	public String GetString() {
		return dmDoc.toString();
	}

	/**
	 * 询问地址获取节点文本
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/@XXX"(匹配从1开始)
	 * @return 匹配成功返回文本,失败返回空
	 */
	public String GetNodeText(String tQuestion) {
		if (dmDoc != null)
		{	
			Element emRoot = dmDoc.child(0);			
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=1; i<a_tRule.size() - 1; ++i)
			{
				String tRule = a_tRule.get(i);
				String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
				if (tIndex.equals(""))
				{
					Elements nlItems = emRoot.getElementsByTag(a_tRule.get(i)); 
					for (int j=0; j<nlItems.size(); ++j)
					{
						Element emChild = nlItems.get(j);
						if (emChild.parent().equals(emRoot))
						{
							emRoot = emChild;
							break;
						}
					}
				}
				else 
				{
					int nIndex = JKConvert.toInt(tIndex);
					Elements nlItems = emRoot.getElementsByTag(tRule.substring(0,tRule.indexOf("["))); 
					for (int j=0; j<nlItems.size(); ++j)
					{
						Element emChild = nlItems.get(j);
						if (emChild.parent().equals(emRoot))
						{
							if (nIndex == 1)
							{
								emRoot = emChild;
								break;
							}
							else
								nIndex--;
						}
					}
				}
			}
			
			String tRule = a_tRule.get(a_tRule.size() - 1);
			if (tRule.indexOf("@") == 0)	//取属性
			{				
				return emRoot.attr(tRule.substring(1));
			}
			else {
				Elements nlItems = emRoot.getElementsByTag(tRule); 
				for (int j=0; j<nlItems.size(); ++j)
				{
					Element emChild = nlItems.get(j);
					if (emChild.parent().equals(emRoot))
					{
						return emChild.text();
					}
				}
			}
		}
		return "";
	}

	/**
	 * 询问地址获取节点个数
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/@XXX"(匹配从1开始)
	 * @return 节点个数
	 */
	public int GetNodeCount(String tQuestion) {
		if (dmDoc != null)
		{	
			Element emRoot = dmDoc.child(0);			
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=1; i<a_tRule.size() - 1; ++i)
			{
				String tRule = a_tRule.get(i);
				String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
				if (tIndex.equals(""))
				{
					Elements nlItems = emRoot.getElementsByTag(a_tRule.get(i)); 
					for (int j=0; j<nlItems.size(); ++j)
					{
						Element emChild = nlItems.get(j);
						if (emChild.parent().equals(emRoot))
						{
							emRoot = emChild;
							break;
						}
					}
				}
				else 
				{
					int nIndex = JKConvert.toInt(tIndex);
					Elements nlItems = emRoot.getElementsByTag(tRule.substring(0,tRule.indexOf("["))); 
					for (int j=0; j<nlItems.size(); ++j)
					{
						Element emChild = nlItems.get(j);
						if (emChild.parent().equals(emRoot))
						{
							if (nIndex == 1)
							{
								emRoot = emChild;
								break;
							}
							else
								nIndex--;
						}
					}
				}
			}
			
			String tRule = a_tRule.get(a_tRule.size() - 1);
			int nNum = 0;
			Elements nlItems = emRoot.getElementsByTag(tRule); 
			for (int j=0; j<nlItems.size(); ++j)
			{
				Element emChild = nlItems.get(j);
				if (emChild.parent().equals(emRoot))
				{
					nNum++;
				}
			}
			return nNum;
		}
		return 0;
	}

	/**
	 * 询问地址是否存在
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/@XXX"(匹配从1开始)
	 * @return 若该节点存在返回true
	 */
	public boolean IsExist(String tQuestion) {
		if (dmDoc != null)
		{	
			Element emRoot = dmDoc.child(0);			
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=1; i<a_tRule.size() - 1; ++i)
			{
				String tRule = a_tRule.get(i);
				String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
				if (tIndex.equals(""))
				{
					Elements nlItems = emRoot.getElementsByTag(a_tRule.get(i)); 
					for (int j=0; j<nlItems.size(); ++j)
					{
						Element emChild = nlItems.get(j);
						if (emChild.parent().equals(emRoot))
						{
							emRoot = emChild;
							break;
						}
					}
				}
				else 
				{
					int nIndex = JKConvert.toInt(tIndex);
					Elements nlItems = emRoot.getElementsByTag(tRule.substring(0,tRule.indexOf("["))); 
					for (int j=0; j<nlItems.size(); ++j)
					{
						Element emChild = nlItems.get(j);
						if (emChild.parent().equals(emRoot))
						{
							if (nIndex == 1)
							{
								emRoot = emChild;
								break;
							}
							else
								nIndex--;
						}
					}
				}
			}
			
			String tRule = a_tRule.get(a_tRule.size() - 1);
			if (tRule.indexOf("@") == 0)	//取属性
			{				
				Attributes aAttrs = emRoot.attributes();
				return aAttrs.hasKey(tRule.substring(1));
			}
			else {
				Elements nlItems = emRoot.getElementsByTag(tRule); 
				for (int j=0; j<nlItems.size(); ++j)
				{
					Element emChild = nlItems.get(j);
					if (emChild.parent().equals(emRoot))
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
}
