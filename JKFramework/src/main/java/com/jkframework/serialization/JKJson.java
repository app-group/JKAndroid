package com.jkframework.serialization;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.jkframework.algorithm.JKAnalysis;
import com.jkframework.algorithm.JKConvert;
import com.jkframework.algorithm.JKFile;
import com.jkframework.debug.JKLog;

public class JKJson{

	/**Json根对象*/
	private JSONObject jsoJson = new JSONObject();

	/**
	 * 从文件里读取序列化内容
	 * @param tPath 文件地址(assets不加"/")
	 * @param tEncoding 编码支持"UTF-8","GBK
	 */
	public void LoadFile(String tPath, Charset tEncoding) {
		try {
			if (tPath.startsWith("/"))	//sdcard路径
				jsoJson = new JSONObject(JKFile.ReadFile(tPath,tEncoding));
			else
				jsoJson = new JSONObject(JKFile.ReadAssetsFile(tPath,tEncoding));
		} catch (JSONException e) {
			JKLog.ErrorLog("读取json失败.原因为" + e.getMessage());
		}
	}

	/**
	 * 从文件里读取序列化内容
	 * @param tPath 文件地址(assets不加"/")
	 */
	public void LoadFile(String tPath)
	{
		LoadFile(tPath, StandardCharsets.UTF_8);
	}

	/**
	 * 设置序列化内容
	 * @param tText 序列化内容
	 */
	public void LoadString(String tText) {
		try {
			jsoJson = new JSONObject(tText);
		} catch (JSONException e) {
			JKLog.ErrorLog("设置json失败.原因为" + e.getMessage());
		}
	}

	/**
	 * 将序列化内容保存文件
	 * @param tPath 保存的sdcard路径
	 * @param tEncoding 编码支持"UTF-8","GBK
	 */
	public void SaveFile(String tPath, Charset tEncoding) {
		JKFile.WriteFile(tPath, GetString(), tEncoding);
	}

	/**
	 * 将序列化内容转为字符串
	 * @return 转化后的字符串
	 */
	public String GetString() {
		return jsoJson.toString();
	}

	/**
	 * 询问地址是否存在
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @return 若该节点存在返回true
	 */
	public String GetNodeText(String tQuestion) {
		if (jsoJson != null)
		{		
			JSONObject jsoTmp = jsoJson;
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=0; i<a_tRule.size() - 1; ++i)
			{
				try {
					String tRule = a_tRule.get(i);
					String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
					if (tIndex.equals(""))
						jsoTmp = jsoTmp.getJSONObject(a_tRule.get(i));
					else 
					{
						JSONArray jsaArray = jsoTmp.getJSONArray(tRule.substring(0,tRule.indexOf("[")));						
						jsoTmp = jsaArray.getJSONObject(JKConvert.toInt(tIndex) - 1);
					}
				} catch (JSONException e) {
					JKLog.ErrorLog("获取json数据出现错误.原因为" + e.getMessage());
					return "";
				}
			}
			try {
				String tRule = a_tRule.get(a_tRule.size() - 1);
				return jsoTmp.getString(tRule);				
			} catch (JSONException e) {
				JKLog.ErrorLog("获取json数据出现错误.原因为" + e.getMessage());
				return "";
			}
		}
		return "";
	}

	/**
	 * 询问地址获取节点文本
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @return 匹配成功返回文本,失败返回空
	 */
	public int GetNodeCount(String tQuestion) {
		if (jsoJson != null)
		{			
			JSONObject jsoTmp = jsoJson;
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=0; i<a_tRule.size() - 1; ++i)
			{
				try {
					String tRule = a_tRule.get(i);
					String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
					if (tIndex.equals(""))
						jsoTmp = jsoTmp.getJSONObject(a_tRule.get(i));
					else 
					{
						JSONArray jsaArray = jsoTmp.getJSONArray(tRule.substring(0,tRule.indexOf("[")));						
						jsoTmp = jsaArray.getJSONObject(JKConvert.toInt(tIndex) - 1);
					}
				} catch (JSONException e) {
					JKLog.ErrorLog("获取json数据出现错误.原因为" + e.getMessage());
					return 0;
				}
			}
			try {
				String tRule = a_tRule.get(a_tRule.size() - 1);
				if (jsoTmp.isNull(tRule))
				{
					return 0;
				}
				else {
					JSONArray jsaArray = jsoTmp.getJSONArray(tRule);
					return jsaArray.length();
				}
			} catch (JSONException e) {
				JKLog.ErrorLog("获取json数据出现错误.原因为" + e.getMessage());
				return 1;
			}
		}
		return 0;
	}

	/**
	 * 询问地址获取节点个数
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @return 节点个数
	 */
	public boolean IsExist(String tQuestion) {
		if (jsoJson != null)
		{
			JSONObject jsoTmp = jsoJson;
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=0; i<a_tRule.size() - 1; ++i)
			{
				try {
					String tRule = a_tRule.get(i);
					String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
					if (tIndex.equals(""))
						jsoTmp = jsoTmp.getJSONObject(a_tRule.get(i));
					else 
					{
						JSONArray jsaArray = jsoTmp.getJSONArray(tRule.substring(0,tRule.indexOf("[")));
						jsoTmp = jsaArray.getJSONObject(JKConvert.toInt(tIndex) - 1);
					}
				} catch (JSONException e) {
					return false;
				}
			}
			return !jsoTmp.isNull(a_tRule.get(a_tRule.size() - 1));
		}
		return false;
	}

	/**
	 * 询问地址创建节点属性
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @param tText 属性值
	 */
	public void CreateNode(String tQuestion, String tText) {
		if (jsoJson != null)
		{			
			JSONObject jsoTmp = jsoJson;
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=0; i<a_tRule.size() - 1; ++i)
			{
				try {
					String tRule = a_tRule.get(i);
					String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
					if (tIndex.equals(""))
					{
						if (jsoTmp.isNull(a_tRule.get(i)))
						{
							jsoTmp.put(tRule, new JSONObject());
						}
						jsoTmp = jsoTmp.getJSONObject(a_tRule.get(i));
					}
					else 
					{
                        if (jsoTmp.isNull(tRule.substring(0, tRule.indexOf("["))))
                        {
                            jsoTmp.put(tRule.substring(0,tRule.indexOf("[")), new JSONArray());
                        }
						JSONArray jsaArray = jsoTmp.getJSONArray(tRule.substring(0,tRule.indexOf("[")));
                        if (jsaArray.isNull(JKConvert.toInt(tIndex) - 1))
                        {
                            jsaArray.put(JKConvert.toInt(tIndex) - 1,new JSONObject());
                        }
						jsoTmp = jsaArray.getJSONObject(JKConvert.toInt(tIndex) - 1);
					}
				} catch (JSONException e) {
					JKLog.ErrorLog("创建json数据出现错误.原因为" + e.getMessage());
					return;
				}
			}
			try {
				String tRule = a_tRule.get(a_tRule.size() - 1);
				jsoTmp.put(tRule, tText);	
			} catch (JSONException e) {
				JKLog.ErrorLog("创建json数据出现错误.原因为" + e.getMessage());
            }
		}
	}

	/**
	 * 询问地址创建节点属性
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @param nText 属性值
	 */
	public void CreateNode(String tQuestion, int nText) {
		if (jsoJson != null)
		{
			JSONObject jsoTmp = jsoJson;
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=0; i<a_tRule.size() - 1; ++i)
			{
				try {
					String tRule = a_tRule.get(i);
					String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
					if (tIndex.equals(""))
					{
						if (jsoTmp.isNull(a_tRule.get(i)))
						{
							jsoTmp.put(tRule, new JSONObject());
						}
						jsoTmp = jsoTmp.getJSONObject(a_tRule.get(i));
					}
					else
					{
						if (jsoTmp.isNull(tRule.substring(0, tRule.indexOf("["))))
						{
							jsoTmp.put(tRule.substring(0,tRule.indexOf("[")), new JSONArray());
						}
						JSONArray jsaArray = jsoTmp.getJSONArray(tRule.substring(0,tRule.indexOf("[")));
						if (jsaArray.isNull(JKConvert.toInt(tIndex) - 1))
						{
							jsaArray.put(JKConvert.toInt(tIndex) - 1,new JSONObject());
						}
						jsoTmp = jsaArray.getJSONObject(JKConvert.toInt(tIndex) - 1);
					}
				} catch (JSONException e) {
					JKLog.ErrorLog("创建json数据出现错误.原因为" + e.getMessage());
					return;
				}
			}
			try {
				String tRule = a_tRule.get(a_tRule.size() - 1);
				jsoTmp.put(tRule, nText);
			} catch (JSONException e) {
				JKLog.ErrorLog("创建json数据出现错误.原因为" + e.getMessage());
			}
		}
	}

	/**
	 * 询问地址创建节点属性
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @param bBool 属性值
	 */
	public void CreateNode(String tQuestion, boolean bBool) {
		if (jsoJson != null)
		{
			JSONObject jsoTmp = jsoJson;
			ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
			for (int i=0; i<a_tRule.size() - 1; ++i)
			{
				try {
					String tRule = a_tRule.get(i);
					String tIndex = JKAnalysis.GetMiddleString(tRule, "[", "]");
					if (tIndex.equals(""))
					{
						if (jsoTmp.isNull(a_tRule.get(i)))
						{
							jsoTmp.put(tRule, new JSONObject());
						}
						jsoTmp = jsoTmp.getJSONObject(a_tRule.get(i));
					}
					else
					{
						if (jsoTmp.isNull(tRule.substring(0, tRule.indexOf("["))))
						{
							jsoTmp.put(tRule.substring(0,tRule.indexOf("[")), new JSONArray());
						}
						JSONArray jsaArray = jsoTmp.getJSONArray(tRule.substring(0,tRule.indexOf("[")));
						if (jsaArray.isNull(JKConvert.toInt(tIndex) - 1))
						{
							jsaArray.put(JKConvert.toInt(tIndex) - 1,new JSONObject());
						}
						jsoTmp = jsaArray.getJSONObject(JKConvert.toInt(tIndex) - 1);
					}
				} catch (JSONException e) {
					JKLog.ErrorLog("创建json数据出现错误.原因为" + e.getMessage());
					return;
				}
			}
			try {
				String tRule = a_tRule.get(a_tRule.size() - 1);
				jsoTmp.put(tRule, bBool);
			} catch (JSONException e) {
				JKLog.ErrorLog("创建json数据出现错误.原因为" + e.getMessage());
			}
		}
	}
}
