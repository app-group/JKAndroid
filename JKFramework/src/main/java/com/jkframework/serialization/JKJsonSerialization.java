package com.jkframework.serialization;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.jkframework.algorithm.JKFile;
import com.jkframework.debug.JKLog;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

public class JKJsonSerialization {

	private static final Gson gson = new Gson();
	static {
//		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static <T> T LoadFile(String tPath, Charset tEncoding, Class<T> cClass) {
		Gson gson = new Gson();
		try (JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(tPath), tEncoding))) {
			return gson.fromJson(reader, cClass);
		} catch (FileNotFoundException e) {
			return null; // 或者抛出自定义异常
		} catch (IOException e) {
			JKLog.e("An error occurred",e);
			return null; // 或者抛出自定义异常
		}
	}

	public static <T> T LoadFile(String tPath, Charset tEncoding, Type tType) {
		Gson gson = new Gson();
		try (InputStreamReader streamReader = new InputStreamReader(new FileInputStream(tPath), tEncoding)) {
			return gson.fromJson(streamReader, tType);
		} catch (FileNotFoundException e) {
			return null; // 或者抛出自定义异常
		} catch (IOException e) {
			JKLog.e("An error occurred", e);
			return null; // 或者抛出自定义异常
		}
	}

	public static <T> T LoadString(String tText,Class<T> cClass) {
		return gson.fromJson(tText,cClass);
	}

	public static <T> T LoadString(String tText,Type type) {
		return gson.fromJson(tText,type);
	}

	public static void SaveFile(String tPath, Object oObject) {
		if (oObject == null) {
			return;
		}

		Gson gson = new Gson();
		// 使用try-with-resources语句来自动关闭JsonWriter
		try (JsonWriter writer = new JsonWriter(new FileWriter(tPath))) {
			gson.toJson(oObject, oObject.getClass(), writer);
			writer.flush(); // 确保所有内容都写入文件
		} catch (IOException e) {
			JKLog.e("An error occurred", e);
			// 可以选择在这里抛出异常，让调用者处理
			throw new RuntimeException("Failed to save data to file", e);
		}
	}

	public static String GetString(Object oObject) {
        return gson.toJson(oObject);
	}

}
