package com.jkframework.serialization;

import com.jkframework.algorithm.JKAnalysis;
import com.jkframework.algorithm.JKFile;
import com.jkframework.debug.JKLog;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public class JKXml{

	/**xml Dom对象*/
	private Document dmDoc = null;


	/**
	 * 从文件里读取序列化内容
	 * @param tPath 文件地址(assets不加"/")
	 */
	public void LoadFile(String tPath) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;

		try {
			builder = factory.newDocumentBuilder();
			if (tPath.startsWith("/"))	//sdcard路径
				dmDoc = builder.parse(new File(tPath));
			else
			{
				InputStream isStream = JKFile.ReadAssets(tPath);
				if (isStream != null) {
					dmDoc = builder.parse(isStream);
				}
			}
        } catch (Exception e) {
			e.printStackTrace();
			JKLog.ErrorLog("读取xml文件失败.原因为" + e.getMessage());
		} 
	}


	/**
	 * 设置序列化内容
	 * @param tText 序列化内容
	 */
	public void LoadString(String tText) {
		StringReader stRead = new StringReader(tText);
	    InputSource iscSource = new InputSource(stRead);
	    
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			dmDoc = builder.parse(iscSource);
		} catch (Exception e) {
			e.printStackTrace();
			JKLog.ErrorLog("读取xml字符串失败.原因为" + e.getMessage());
		} 
	}

	/**
	 * 将序列化内容保存文件
	 * @param tPath 保存的sdcard路径
	 * @param tEncoding 编码支持"UTF-8","GBK
	 */
	public void SaveFile(String tPath, String tEncoding) {
		Source source = new DOMSource(dmDoc);
		Result result = new StreamResult(new File(tPath));
		
		Transformer xformer;
		try {
			xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.setOutputProperty(OutputKeys.ENCODING, tEncoding);
			xformer.transform(source, result);
		} catch (Exception e) {
			e.printStackTrace();
			JKLog.ErrorLog("保存xml文件失败.原因为" + e.getMessage());
		} 
	}

	/**
	 * 将序列化内容保存文件
	 * @param tPath 保存的sdcard路径
	 */
	public void SaveFile(String tPath)
	{
		SaveFile(tPath,"UTF-8");
	}

	/**
	 * 将序列化内容转为字符串
	 * @return 转化后的字符串
	 */
	public String GetString() {
		Source source = new DOMSource(dmDoc);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		OutputStreamWriter write = new OutputStreamWriter(outStream);
		Result result = new StreamResult(write);
		
		Transformer xformer;
		try {
			xformer = TransformerFactory.newInstance()
					.newTransformer();
			xformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			xformer.transform(source, result);
		} catch (Exception e) {
			e.printStackTrace();
			JKLog.ErrorLog("获取xml字符串失败.原因为" + e.getMessage());
		} 
		
		return outStream.toString();
	}

	/**
	 * 询问地址获取节点个数
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @return 节点个数
	 */
	public boolean IsExist(String tQuestion) {
		if (dmDoc != null)
		{
            XPathFactory xpfactory = XPathFactory.newInstance();
            XPath xpath = xpfactory.newXPath();
            try {
                Object bv = xpath.evaluate("/" + tQuestion,dmDoc,XPathConstants.NODE);
                return bv != null;
            } catch (XPathExpressionException e) {
                JKLog.ErrorLog("Xml节点不合法");
            }
        }
		return false;
	}

	/**
	 * 询问地址是否存在
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/XXX"(匹配从1开始)
	 * @return 若该节点存在返回true
	 */
	public String GetNodeText(String tQuestion) {
		if (dmDoc != null) {
            XPathFactory xpfactory = XPathFactory.newInstance();
            XPath xpath = xpfactory.newXPath();
            try {
                return xpath.evaluate("/" + tQuestion,dmDoc);
            } catch (XPathExpressionException e) {
                JKLog.ErrorLog("Xml节点不合法");
            }
        }
		return "";
	}

	/**
	 * 询问地址获取节点文本
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/@XXX"(匹配从1开始)
	 * @return 匹配成功返回文本,失败返回空
	 */
	public int GetNodeCount(String tQuestion) {
		if (dmDoc != null)
		{
            XPathFactory xpfactory = XPathFactory.newInstance();
            XPath xpath = xpfactory.newXPath();
            try {
				NodeList node = (NodeList) xpath.evaluate("/" + tQuestion,dmDoc,XPathConstants.NODESET);
				return node.getLength();
            } catch (XPathExpressionException e) {
                JKLog.ErrorLog("Xml节点不合法");
            }
            catch (ClassCastException e) {
                return 0;
            }
		}
		return 0;
	}

	/**
	 * 询问地址创建节点属性
	 * @param tQuestion 匹配规则为"XXX/XXX[n]/@XXX"(匹配从1开始)
	 * @param tText 属性值
	 */
	public void CreateNode(String tQuestion, String tText) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		if (tQuestion.contains("/"))	//非根节点
		{
			if (dmDoc != null)
			{
				Element emRoot = dmDoc.getDocumentElement(); 
				ArrayList<String> a_tRule = JKAnalysis.Split(tQuestion, "/");
				for (int i=1; i<a_tRule.size() - 1; ++i)
				{
					NodeList nlItems = emRoot.getElementsByTagName(a_tRule.get(i)); 
					for (int j=0; j<nlItems.getLength(); ++j)
					{
						Element emChild = (Element) nlItems.item(j);
						if (emChild.getParentNode().equals(emRoot))
						{
							emRoot = emChild;
							break;
						}
					}
				}
				
				String tRule = a_tRule.get(a_tRule.size() - 1);
				if (tRule.contains("@"))	//属性值
				{		
					emRoot.setAttribute(tRule.substring(1),tText);
				}
				else {
					Element emChild = dmDoc.createElement(a_tRule.get(a_tRule.size() - 1));
					emChild.setTextContent(tText);
					emRoot.appendChild(emChild);
				}
			}
		}
		else {
			try {
				DocumentBuilder builder = factory.newDocumentBuilder();
				dmDoc = builder.newDocument();
				Element emRoot = dmDoc.createElement(tQuestion);
				dmDoc.appendChild(emRoot);	
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
				JKLog.ErrorLog("创建xml根节点失败.原因为" + e.getMessage());	
			}	
		}
	}

}
