package com.jkframework.thread;


import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class JKThread {

    private Disposable dThread;

    /**
     * 执行短期线程功能
     *
     * @param l 线程功能监听
     */
    public void Start(final JKThreadListener l) {
        dThread = Flowable.defer(() -> {
                    if (l != null) {
                        l.OnThread();
                    }
                    return Flowable.empty();
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> {
                    if (l != null) {
                        l.OnMain();
                    }
                })
                .subscribe();
    }

    /**
     * 取消主线程执行
     */
    public void Cancel() {
        if (dThread != null && !dThread.isDisposed()) {
            dThread.dispose();
        }
    }

    /**
     * 休眠等待一定时间
     *
     * @param nMillis 毫秒数
     */
    public static void Sleep(long nMillis) {
        try {
            Thread.sleep(nMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public interface JKThreadListener {

        /**
         * 线程执行的功能
         */
        void OnThread();

        /**
         * 线程执行完毕回调主线程的功能
         */
        void OnMain();
    }
}
